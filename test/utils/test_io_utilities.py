from __future__ import absolute_import

"""
 This is for basic i/o functions.
"""
# STL
import fnmatch
import io
import logging
# import mimetypes
import os
from os import scandir  # scandir is in os module in python 3.5+
import re
import shutil

# PROJECT
from common import veri_global

# EXTERNALS
# """
try:
    from google.cloud import storage
    from google.oauth2 import service_account

# ======================================================================================================================
    # Instantiates a client
    credential_json_filename = os.path.join(veri_global.VERI_CONFIG,
                                            "credentials/veri-din-ctsc-dev-66d2c014718a7477bf.json")
    # # Old version
    # storage_client = storage.Client.from_service_account_json(credential_json_filename)

    # New version
    # TODO : A TESTER : meilleures performances ?
    CREDENTIALS = service_account.Credentials.from_service_account_file(credential_json_filename)
    SCOPED_CREDENTIALS = CREDENTIALS.with_scopes(['https://www.googleapis.com/auth/cloud-platform'])
    PROJECT_ID = "gbl-imt-veri-ctsc-prod"
    storage_client = storage.Client(project=PROJECT_ID, credentials=SCOPED_CREDENTIALS)
    logging.info("Storage clinet initialized on project " + PROJECT_ID + " with credentials " + credential_json_filename)
# ======================================================================================================================
except Exception as e:
    logging.exception(e)
    storage = None
    storage_client = None
# """
# storage_client = None

####################
# PUBLIC FUNCTIONS #
####################

isdir = os.path.isdir


def makedirs(uri, *args, **kwargs):
    try:
        if not _is_on_storage(uri):
            os.makedirs(uri, *args, **kwargs)
    except:
        pass
# makedirs = os.makedirs


def scandir_recursive(path):
    """Recursively yield DirEntry objects for given directory."""
    for entry in scandir(path):
        if entry.is_dir(follow_symlinks=False):
            for entry in scandir_recursive(entry.path):
                yield entry
        else:
            yield entry
# scandir_recursive


def find_files(directory, pattern, exlusion_pattern=None):
    """ Return list of files matching pattern in base folder (recursive). """
    logging.debug(directory + " is_on_storage: " + str(_is_on_storage(str(directory))))
    if _is_on_storage(directory):
        if storage_client is None:
            logging.warning("No access to GCP with this version")
            names = []
        else:
            bucket_name, prefix = _get_bucket_prefix(directory)
            while True:
                try:
                    bucket = storage_client.get_bucket(bucket_name)
                    break
                except:
                    pass
            blobs = bucket.list_blobs(prefix=prefix)
            names = ["/".join(["gs:/", bucket_name, blob.name]) for blob in blobs]
    else:
        names = scandir_recursive(directory)
        names = [os.path.join(directory, f.path) for f in names]
        names = [n for n in names if os.path.isfile(n)]

    output = fnmatch.filter(names, pattern)
    if exlusion_pattern is not None:
        output = [x for x in output if not re.match(exlusion_pattern, x)]
    return output
# find_files


# def find_files(directory, pattern):
#     """ Return list of files matching pattern in base folder. """
#     logging.debug(directory + " is_on_storage: " + str(_is_on_storage(str(directory))))
#     if _is_on_storage(directory):
#         bucket_name, prefix = _get_bucket_prefix(directory)
#         bucket = storage_client.get_bucket(bucket_name)
#         blobs = bucket.list_blobs(prefix=prefix)
#         names = ["/".join(["gs:/", bucket_name, blob.name]) for blob in blobs]
#     else:
#         names = [n.name for n in scandir(directory)]
#         names = [os.path.join(directory, f) for f in names]
#         names = [n for n in names if os.path.isfile(os.path.join(directory, n))]
#     return fnmatch.filter(names, pattern)
# # find_files


def read_file(uri):
    """
        Read file from disk or Google storage
    """
    try:
        if _is_on_storage(uri):
            return _StorageURIReader(uri)
        else:
            return _LocalURIReader(uri)
    except Exception as e:
        logging.error(e)
        return open(uri, 'r')
# read_file


def write_file(uri):
    """
        Write file on disk (TODO: write on google storage)
    """
    try:
        if _is_on_storage(uri):
            return _StorageURIWriter(uri)
        else:
            return _LocalURIWriter(uri)
    except Exception as e:
        logging.error(e)
        return open(uri, 'r')
    # return open(uri, 'w')


def copyfile(src, dst):
    is_src_on_storage = _is_on_storage(src)
    is_dst_on_storage = _is_on_storage(dst)
    if is_src_on_storage and is_dst_on_storage:
        _gcp_copy(src, dst)
    elif is_src_on_storage and (not is_dst_on_storage):
        _gcp_download(src, dst)
    elif (not is_src_on_storage) and is_dst_on_storage:
        _gcp_upload(src, dst)
    else:
        shutil.copyfile(src, dst)
# copyfile


def isfile(uri):
    """ return True iff file exists"""
    is_on_storage = _is_on_storage(uri)
    if is_on_storage:
        result = _gcp_isfile(uri)
    else:
        result = os.path.isfile(uri)
    return result
# ======================================================================================================================

#####################
# PRIVATE FUNCTIONS #
#####################


def _gcp_isfile(uri):
    """ true if file exist on gcp"""
    bucket_name, blob_name = _get_bucket_prefix(uri)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(blob_name)
    return not(blob is None)


def _gcp_copy(src, dst):
    """ copy file from gcp to gcp"""
    src_bucket_name, src_blob_name = _get_bucket_prefix(src)
    dst_bucket_name, dst_blob_name = _get_bucket_prefix(dst)

    src_bucket = storage_client.get_bucket(src_bucket_name)
    src_blob = src_bucket.get_blob(src_blob_name)
    dst_bucket = storage_client.get_bucket(dst_bucket_name)
    src_bucket.copy_blob(src_blob, dst_bucket, dst_blob_name)
# _gcp_copy


def _gcp_download(src, dst):
    """ download file from gcp"""
    # WARNING TODO NOT TESTED
    bucket_name, blob_name = _get_bucket_prefix(src)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(blob_name)
    with open(dst, 'wb') as f:
        blob.download_to_file(f)
# _gcp_download


def _gcp_upload(src, dst):
    # WARNING TODO NOT TESTED
    bucket_name, blob_name = _get_bucket_prefix(src)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    with open(dst, 'rb') as f:
        blob.upload_from_file(f)
# _gcp_upload

def _get_bucket_prefix(path_name):
    """ A path on storage is written as gs:/<bucket_name>/<blob_name or blob_prefix>"""
    path_name = path_name.replace('\\', '/')
    path_name = path_name.split('gs://')[-1]
    bucket_name = path_name.split('/')[0]
    path_name = path_name.split(bucket_name)[-1]
    path_name = path_name.lstrip('/')
    return bucket_name, path_name
# get_bucket_prefix


def _is_on_storage(path_name):
    """ :return True if path name start with gs://"""
    # return len(path_name) > 5 and path_name[:5] == "gs://"
    return path_name.startswith("gs://")
# is_on_storage


class _StorageURI(object):
    def __init__(self, uri):
        bucket_name, self._blob_name = _get_bucket_prefix(uri)
        self._bucket = storage_client.get_bucket(bucket_name)

        self._buffer = io.BytesIO()

    def __enter__(self):
        self._buffer.seek(0)
        return self._buffer

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
# _StorageURI


class _StorageURIWriter(_StorageURI):
    # https://googleapis.github.io/google-cloud-python/latest/storage/buckets.html#google.cloud.storage.bucket.Bucket
    def __init__(self, uri):
        super(_StorageURIWriter, self).__init__(uri)
        # content_type, _ = mimetypes.guess_type(uri)
        self._blob = self._bucket.blob(self._blob_name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._buffer.seek(0)
        self._blob.upload_from_file(self._buffer)
# _StorageURIWriter


class _StorageURIReader(_StorageURI):
    """ class to read URI on Google storage as a file """
    # https://googleapis.github.io/google-cloud-python/latest/storage/buckets.html#google.cloud.storage.bucket.Bucket
    def __init__(self, uri):
        super(_StorageURIReader, self).__init__(uri)
        blob = self._bucket.get_blob(self._blob_name)
        if blob is not None:
            blob.download_to_file(self._buffer)
        else:
            logging.error("empty blob: {} on bucket: {}".format(self._blob_name, uri))
# _StorageURIReader


class _LocalURIReader(object):
    """ class to read URI on Google storage as a file """
    def __init__(self, uri):
        self._buffer = io.BytesIO(open(uri, 'rb').read())

    def __enter__(self):
        self._buffer.seek(0)
        return self._buffer

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
# _LocalURIReader


class _LocalURIWriter(object):
    """ class to read URI on Google storage as a file """
    def __init__(self, uri):
        self._buffer = io.BytesIO()
        self._uri = uri

    def __enter__(self):
        self._buffer.seek(0)
        return self._buffer

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._buffer.seek(0)
        with open(self._uri, 'w') as f:
            f.write(self._buffer.read())
# _LocalURIWriter
# ======================================================================================================================
