import json
import os

from common.pipeline.worker import Worker
from common.segmentation.dtb_to_tf_records import StevenDTBConverter, StevenDTBConverterToThumbnail


class DtbToTFRecordsWorker(Worker):
    """Convert Steven of Stevie database to tensorflow records

    """

    NAME = "database"
    SUBDIR_NAME = "tf_records"

    def run(self):
        root_dir = self._get_config(["database", "root_dir"], "")
        image_meta_directory = self._get_config(["database", "image_metadata"], "")
        image_status_directory = self._get_config(["database", "image_status"], "")
        object_directory = self._get_config(["database", "objects"], "")
        labels_directory = self._get_config(["database", "labels"], "")

        converter = StevenDTBConverter(
            output_directory=self._output_directory,
            image_meta_directory=os.path.join(root_dir, image_meta_directory),
            image_status_directory=os.path.join(root_dir, image_status_directory),
            object_directory=os.path.join(root_dir, object_directory),
            labels_directory=os.path.join(root_dir, labels_directory),
            use_uri_mapper=self._get_config(["database", "use_uri_mapper"], False),
            uri_mapper=self._get_config(["database", "uri_mapper"], {}),
            keep_empty_tag=self._get_config(["labels", "keep_empty_tag"]),
            categories=self._get_config(["labels", "categories"]),
            single_class=self._get_config(["database", "use_single_class"], False),
        )

        accepted_status = self._get_config(["database", "accepted_statuses"], [])
        converter.filter(*accepted_status)

        converter.build_mapper()

        converter.split(
            train=self._get_config(["database", "split", "train"], 0.8),
            test=self._get_config(["database", "split", "test"], 0.15),
            eval=self._get_config(["database", "split", "eval"], 0.05),
        )
        converter.create_tf_records(
            max_examples=self._get_config(["database", "max_examples"])
        )

        with open(os.path.join(self._output_directory, "cache.json"), "w") as f:
            json.dump({}, f)


class DtbToTFRecordsThumbnailWorker(Worker):
    """Convert Steven of Stevie database to tensorflow records

    """

    NAME = "database"
    SUBDIR_NAME = "tf_records"

    def run(self):
        root_dir = self._get_config(["database", "root_dir"], "")
        image_meta_directory = self._get_config(["database", "image_metadata"], "")
        image_status_directory = self._get_config(["database", "image_status"], "")
        object_directory = self._get_config(["database", "objects"], "")
        labels_directory = self._get_config(["database", "labels"], "")

        converter = StevenDTBConverterToThumbnail(
            output_directory=self._output_directory,
            image_meta_directory=os.path.join(root_dir, image_meta_directory),
            image_status_directory=os.path.join(root_dir, image_status_directory),
            object_directory=os.path.join(root_dir, object_directory),
            labels_directory=os.path.join(root_dir, labels_directory),
            use_uri_mapper=self._get_config(["database", "use_uri_mapper"], False),
            uri_mapper=self._get_config(["database", "uri_mapper"], {}),
            keep_empty_tag=self._get_config(["labels", "keep_empty_tag"]),
            categories=self._get_config(["labels", "categories"]),
            single_class=self._get_config(["database", "use_single_class"], False),
        )

        accepted_status = self._get_config(["database", "accepted_statuses"], [])
        converter.filter(*accepted_status)

        converter.build_mapper()

        converter.split(
            train=self._get_config(["database", "split", "train"], 0.8),
            test=self._get_config(["database", "split", "test"], 0.15),
            eval=self._get_config(["database", "split", "eval"], 0.05),
        )
        converter.create_tf_records(
            max_examples=self._get_config(["database", "max_examples"])
        )

        with open(os.path.join(self._output_directory, "cache.json"), "w") as f:
            json.dump({}, f)
