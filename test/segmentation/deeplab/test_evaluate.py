import tensorflow as tf

SCOPE = "semantic"


def eval(dataset_name, split, num_classes, flags):
    """Evaluate a model on a given dataset

    :param dataset_name: Used by `data_generator.Dataset` internally
    :param split: Name of the split to evaluate
    :param num_classes: Number of classes
    :param flags: Dictionary that contains many informations required
    for evaluation and model building, usually generated from tf.flags.FLAGS

    """

    from deeplab import common, model, train
    from deeplab.datasets import data_generator
    from deeplab.utils import train_utils

    descriptor = data_generator.DatasetDescriptor(
        splits_to_sizes=split, num_classes=num_classes, ignore_label=255
    )
    data_generator._DATASETS_INFORMATION[dataset_name] = descriptor
    dataset = data_generator.Dataset(
        dataset_name=flags["dataset"],
        split_name=flags["eval_split"],
        dataset_dir=flags["dataset_dir"],
        batch_size=flags["eval_batch_size"],
        crop_size=[int(sz) for sz in flags["eval_crop_size"]],
        min_resize_value=flags["min_resize_value"],
        max_resize_value=flags["max_resize_value"],
        resize_factor=flags["resize_factor"],
        model_variant=flags["model_variant"],
        num_readers=1,
        is_training=True,
        should_shuffle=False,
        should_repeat=False,
    )

    tf.gfile.MakeDirs(flags["eval_logdir"])
    tf.logging.info("Evaluating on %s set", flags["eval_split"])

    with tf.Graph().as_default() as g:
        samples = dataset.get_one_shot_iterator().get_next()

        model_options = common.ModelOptions(
            outputs_to_num_classes={common.OUTPUT_TYPE: dataset.num_of_classes},
            crop_size=[int(sz) for sz in flags["eval_crop_size"]],
            atrous_rates=flags["atrous_rates"],
            output_stride=flags["output_stride"],
        )

        outputs_to_scales_to_logits = model.multi_scale_logits(
            samples[common.IMAGE],
            model_options=model_options,
            image_pyramid=flags["image_pyramid"],
        )
        output_type_dict = outputs_to_scales_to_logits[common.OUTPUT_TYPE]
        output_type_dict[model.MERGED_LOGITS_SCOPE] = tf.identity(
            output_type_dict[model.MERGED_LOGITS_SCOPE], name=common.OUTPUT_TYPE
        )

        train_utils.add_softmax_cross_entropy_loss_for_each_scale(
            outputs_to_scales_to_logits[SCOPE],
            samples[common.LABEL],
            num_classes,
            255,
            loss_weight=1.0,
            upsample_logits=flags["upsample_logits"],
            hard_example_mining_step=flags["hard_example_mining_step"],
            top_k_percent_pixels=flags["top_k_percent_pixels"],
            scope=SCOPE,
        )

        input_image = samples[common.IMAGE]
        label = samples[common.LABEL]

        # Scale up summary image pixel values for better visualization.
        pixel_scaling = max(1, 255 // num_classes)
        summary_label = tf.cast(label * pixel_scaling, tf.uint8)

        # Predicted image
        output = output_type_dict[model.MERGED_LOGITS_SCOPE]
        predictions = tf.expand_dims(tf.argmax(output, 3), -1)
        summary_predictions = tf.cast(predictions * pixel_scaling, tf.uint8)

        losses = tf.losses.get_losses(scope=SCOPE)
        for loss in losses:
            if "const" not in loss.op.name.lower():
                tf.summary.scalar(f"Losses/{loss.op.name}", loss)

        tf.summary.image(f"Originals/{common.IMAGE}", input_image, 20)
        tf.summary.image(f"Labels/{common.LABEL}", summary_label, 20)
        tf.summary.image(f"Predictions/{common.OUTPUT_TYPE}", summary_predictions, 20)

        summary_op = tf.summary.merge_all()
        summary_hook = tf.contrib.training.SummaryAtEndHook(
            log_dir=flags["eval_logdir"], summary_op=summary_op
        )
        tf.contrib.training.evaluate_once(
            checkpoint_path=tf.train.latest_checkpoint(flags["checkpoint_dir"]),
            master=flags["master"],
            hooks=[summary_hook],
        )
