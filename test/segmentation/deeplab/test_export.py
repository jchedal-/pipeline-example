import importlib
import json
import os.path
from shutil import copyfile

import tensorflow as tf
from tensorflow.saved_model import signature_constants, tag_constants

import common.detection.configuration
from common.pipeline.worker import Worker


class ModelExporterWorker(Worker):
    """Export and freeze tf checkpoints"""

    NAME = "export"
    SUBDIR_NAME = "tf_export"

    def run(self):
        try:
            with open(os.path.join(self._input_directory, "cache.json"), "r") as f:
                prev_cache = json.load(f)
        except FileNotFoundError:
            prev_cache = {}

        with open(os.path.join(self._output_directory, "cache.json"), "w") as f:
            json.dump(prev_cache, f, indent=4)

        export_model = self._import_module_and_set_main_flags(prev_cache)
        export_model.main(None)

        with tf.gfile.GFile(tf.flags.FLAGS.export_path, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        builder = tf.saved_model.builder.SavedModelBuilder(
            os.path.join(self._output_directory, "saved_model")
        )
        sigs = {}

        with tf.Graph().as_default() as g:
            with tf.Session(graph=g) as sess:
                inp, out = tf.import_graph_def(
                    graph_def,
                    input_map=None,
                    return_elements=[
                        export_model._INPUT_NAME + ":0",
                        export_model._OUTPUT_NAME + ":0",
                    ],
                    name="",
                )

                sigs[
                    signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
                ] = tf.saved_model.signature_def_utils.predict_signature_def(
                    {export_model._INPUT_NAME + ":0": inp},
                    {export_model._OUTPUT_NAME + ":0": out},
                )

                builder.add_meta_graph_and_variables(
                    sess, [tag_constants.SERVING], signature_def_map=sigs
                )

            builder.save()

        for filename in ["labels.pbtxt", "groups.json"]:
            src = os.path.join(self._input_directory, filename)
            dst = os.path.join(self._output_directory, filename)
            copyfile(src, dst)

    def _import_module_and_set_main_flags(self, cache):
        common.detection.configuration.clear_all_tf_flags(tf.flags.FLAGS)
        import deeplab.export_model

        importlib.reload(deeplab.export_model)
        importlib.reload(deeplab.common)

        tf.flags.FLAGS.checkpoint_path = tf.train.latest_checkpoint(
            self._input_directory
        )
        tf.flags.FLAGS.export_path = os.path.join(
            self._output_directory, "frozen_graph.pb"
        )
        tf.flags.FLAGS.save_inference_graph = True

        tf.flags.FLAGS.max_resize_value = self._get_config(["evaluation", "size"]) or 800
        # Hack to force the size to be config["evaluation"]["size"]
        tf.flags.FLAGS.min_resize_value = tf.flags.FLAGS.max_resize_value + 1
        tf.flags.FLAGS.crop_size = [tf.flags.FLAGS.max_resize_value, tf.flags.FLAGS.min_resize_value]

        tf.flags.FLAGS.num_classes = cache["num_classes"]
        tf.flags.FLAGS.atrous_rates = cache["atrous_rates"]
        tf.flags.FLAGS.output_stride = cache["output_stride"]
        tf.flags.FLAGS.model_variant = cache["model_variant"]
        tf.flags.FLAGS.image_pyramid = cache["image_pyramid"]
        tf.flags.FLAGS.add_image_level_feature = cache["add_image_level_feature"]
        tf.flags.FLAGS.aspp_with_batch_norm = cache["aspp_with_batch_norm"]
        tf.flags.FLAGS.aspp_with_separable_conv = cache["aspp_with_separable_conv"]
        tf.flags.FLAGS.multi_grid = cache["multi_grid"]
        tf.flags.FLAGS.decoder_output_stride = cache["decoder_output_stride"]
        tf.flags.FLAGS.decoder_use_separable_conv = cache["decoder_use_separable_conv"]
        tf.flags.FLAGS.merge_method = cache["merge_method"]
        tf.flags.FLAGS.prediction_with_upsampled_logits = cache[
            "prediction_with_upsampled_logits"
        ]

        return deeplab.export_model
