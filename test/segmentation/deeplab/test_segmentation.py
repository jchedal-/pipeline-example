import glob
import json
import logging
import os
from shutil import copyfile

import numpy as np
import tensorflow as tf
from PIL import Image

from common.pipeline.worker import Worker
from common.segmentation.model_runner import SegmentationRunner
from common.utilities import random_colors


class SegmentationWorker(Worker):
    NAME = "evaluation"
    SUBDIR_NAME = "tf_evaluation"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = None

    def run(self):
        """Load the frozen graph, and run the model on the pipeline's tf records"""
        self._model = SegmentationRunner(
            os.path.join(self._input_directory, "frozen_graph.pb")
        )

        with open(os.path.join(self._input_directory, "cache.json"), "r") as f:
            cache = json.load(f)

        path_to_records = cache["path_to_records"]
        color_map = (
            (np.array(random_colors(cache["num_classes"])) * 255)
            .astype(np.uint8)
            .__getitem__
        )

        for record in glob.glob(os.path.join(path_to_records, "*-?????-of-?????")):
            logging.info(f"Evaluating {record}...")
            # get the current file, split on `-` and get the first element
            # /foo/bar/baz-00000-of-00001 => baz
            folder_name, number, _ = (
                record.replace("\\", "/").rsplit("/", 1)[1].split("-", 2)
            )
            number = int(number)
            folder_path = os.path.join(self._output_directory, folder_name).replace(
                "\\", "/"
            )
            os.makedirs(folder_path, exist_ok=True)
            with tf.Session(graph=self._model.graph) as sess:
                for i, data in enumerate(
                    self._model.run_from_record(sess, _decoder, record)
                ):
                    filename = data["filename"].decode().rsplit("/", 1)[1]
                    image = data["image"].squeeze()
                    mask = data["mask"].squeeze()
                    inferred = data["inferred"].squeeze()
                    original_image = Image.fromarray(image)
                    original_mask = Image.fromarray(mask)
                    inferred_mask = Image.fromarray(inferred)
                    original_mask_cmap = Image.fromarray(color_map(mask))
                    inferred_mask_cmap = Image.fromarray(color_map(inferred))

                    def saver(img, name, type_):
                        img.save(
                            os.path.join(
                                folder_path, f"{filename}-{name}-{type_}.png"
                            ).replace("\\", "/")
                        )

                    saver(original_image, "original", "raw")
                    saver(original_mask, "original", "mask")
                    saver(inferred_mask, "inferred", "mask")
                    saver(original_mask_cmap, "original", "cmap")
                    saver(inferred_mask_cmap, "inferred", "cmap")

        for filename in ["labels.pbtxt", "groups.json", "cache.json"]:
            src = os.path.join(self._input_directory, filename)
            dst = os.path.join(self._output_directory, filename)
            copyfile(src, dst)


def _decoder(serialized_example):
    def _decode_image(content, channels):
        return tf.cond(
            tf.image.is_jpeg(content),
            lambda: tf.image.decode_jpeg(content, channels),
            lambda: tf.image.decode_png(content, channels),
        )

    features = {
        "image/encoded": tf.FixedLenFeature((), tf.string, default_value=""),
        "image/filename": tf.FixedLenFeature((), tf.string, default_value=""),
        "image/format": tf.FixedLenFeature((), tf.string, default_value="jpeg"),
        "image/height": tf.FixedLenFeature((), tf.int64, default_value=0),
        "image/width": tf.FixedLenFeature((), tf.int64, default_value=0),
        "image/segmentation/class/encoded": tf.FixedLenFeature(
            (), tf.string, default_value=""
        ),
        "image/segmentation/class/format": tf.FixedLenFeature(
            (), tf.string, default_value="png"
        ),
    }

    parsed_features = tf.parse_single_example(serialized_example, features)
    image = _decode_image(parsed_features["image/encoded"], channels=3)
    image = tf.expand_dims(image, 0)
    mask = _decode_image(
        parsed_features["image/segmentation/class/encoded"], channels=1
    )
    filename = parsed_features["image/filename"]
    return filename, image, mask
