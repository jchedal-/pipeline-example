# STL
import datetime
import json
import logging
import re

# External
import numpy as np
import pytz

# VeRI
from common.database import object_set


class DatabaseDescriptor(object):
    # <IMAGE STATUSES>
    LABEL_DONE = "LabelDone"
    SEG_DONE = "SegDone"
    SEG_NO = "SegNO"
    SEG_STARTED = "SegStarted"
    SEG_CHECKED = "SegChecked"
    # </IMAGE STATUSES>

    NO_PROPOSAL = "NO_PROPOSAL"
    PROPOSAL = "PROPOSAL"
    FOCUS_PROPOSAL = "FOCUS_PROPOSAL"

    def __init__(self, **kwargs):
        self.segmented_objects = None
        self.image_status = None
        self.image_metadata = None

        self._statuses = {}
        self._label_creator = {}
        self._segment_creator = {}
        self._segment_proposal = {}
        self._segment_creator = {}
        self._segment_bounding_box = {}
        self._tags = {}

        self._segment_timestamp = {}
        self._label_timestamp = {}
        self._segment_duration = {}
        self._label_duration = {}

        # <LOAD DTB>
        annotation_directory = kwargs.get("directories", {}).get("objects")
        if annotation_directory:
            self.segmented_objects = object_set.ObjectSet(
                annotation_directory=annotation_directory, all_in_cache=True
            )

        annotation_directory = kwargs.get("directories", {}).get("metadata")
        if annotation_directory:
            self.image_metadata = object_set.ObjectSet(
                annotation_directory=annotation_directory, all_in_cache=True
            )

        annotation_directory = kwargs.get("directories", {}).get("status")
        if annotation_directory:
            self.image_status = object_set.ObjectSet(
                annotation_directory=annotation_directory, all_in_cache=True
            )

        self._load()
        # </LOAD DTB>

        # <CREATOR MAPPER>
        self._creator_mapper = kwargs.get("creator_mapper", {})

    # __init__

    ##################
    # PUBLIC METHODS #
    ##################

    def get_bounding_box(self):
        return self._count(self._segment_bounding_box)

    def get_label_creators(self):
        """ get list of label creators"""
        return self._count(self._label_creator)

    def get_label_duration(self):
        return list(self._label_duration.values())

    def get_segment_creators(self):
        return self._count(self._segment_creator)

    def get_segment_duration(self):
        return list(self._segment_duration.values())

    def get_segment_from_proposition(self):
        return self._count(self._segment_proposal)

    def get_start_date(self):
        start_seg = min(self._segment_timestamp.values(), default=None)
        return str(start_seg)

    def get_end_date(self):
        end_seg = max(self._segment_timestamp.values(), default=None)
        end_label = max(self._label_timestamp.values(), default=None)
        if end_label is None:
            return str(end_seg)
        end_date = max([end_seg, end_label])
        return str(end_date)

    def get_statuses(self):
        return self._count(self._statuses)

    def get_tags(self):
        return self._count(self._tags)

    def get_time_stats(self):
        bbox_name = {True: "bounding_box", False: "mesh"}

        result = {}
        for is_bbox in [True, False]:
            if is_bbox not in self._segment_bounding_box:
                logging.warning(
                    "is_bbox {} not in self._segment_bounding_box".format(is_bbox)
                )
                continue
            durations = self._get_durations(
                self._segment_duration, self._segment_bounding_box[is_bbox]
            )
            res = {"all": {"median": np.median(durations), "count": np.size(durations)}}
            for proposition, val in self._segment_proposal.items():
                durations = self._get_durations(
                    self._segment_duration, self._segment_bounding_box[is_bbox], val
                )
                res[proposition] = {
                    "median": np.median(durations),
                    "count": np.size(durations),
                }
            result[bbox_name[is_bbox]] = res

        if len(self._label_duration) > 0:
            durations = self._get_durations(self._label_duration)
            result["labels"] = {
                "median": np.median(durations),
                "count": np.size(durations),
            }
        return result

    # get_time_stats

    def group_by_image(self, *args):
        if len(args) == 0:
            args = [self.LABEL_DONE, self.SEG_DONE, self.SEG_CHECKED]
        result = {}
        for label in args:
            if label not in self._statuses:
                continue
            for key in self._statuses[label]:
                val = self.segmented_objects.get(key)
                feedback = self.image_status.get(key, default_value={}).get(
                    "feedback", ""
                )
                # remove html anchors
                feedback = """ "{}" """.format(re.sub("<[^<>]*>", "", feedback))
                result[key] = {
                    "duration": [
                        self._segment_duration.get(object_key, "")
                        for object_key in val.keys()
                    ],
                    "original": val,
                    "nb_objects": len(val),
                    "feedback": feedback,
                }
                result[key]["median_duration"] = np.median(
                    np.array(result[key]["duration"])
                )
        return result

    # group_by_image

    def parse_statuses(self):
        """ group images per statuses """
        self._statuses = {}
        for key, val in self.image_status.items():
            if "status" not in val:
                continue
            self._add_key(self._statuses, val["status"], key)

    # parse_statuses

    def parse_segmentation(self, *args):
        if len(args) == 0:
            args = [self.LABEL_DONE, self.SEG_DONE, self.SEG_CHECKED]
        self._segment_creator = {}
        self._segment_proposal = {}
        self._segment_bounding_box = {}
        # for label in [self.LABEL_DONE, self.SEG_DONE, ]:
        for label in args:
            if label not in self._statuses:
                continue
            for key in self._statuses[label]:
                val = self.segmented_objects.get(key)
                for object_key, object_val in val.items():
                    bounding_box_only = object_val.get("bounding_box_only")
                    self._add_key(
                        self._segment_bounding_box, bounding_box_only, object_key
                    )

                    proposed = object_val.get("from_proposal")
                    self._add_key(self._segment_proposal, proposed, object_key)

                    timestamp = object_val.get("timestamp")
                    if timestamp:
                        timestamp = object_set.str_to_date(timestamp)
                        self._segment_timestamp[object_key] = timestamp

                    creator_id = object_val.get("object_creator_id", None)
                    creator = self._creator_name(creator_id)
                    self._add_key(self._segment_creator, creator, object_key)
        self._compute_segment_duration()

    # parse_segmentation

    def parse_labelling(self):
        self._tags = {}
        self._label_creator = {}
        if self.LABEL_DONE not in self._statuses:
            return
        for key in self._statuses[self.LABEL_DONE]:
            val = self.segmented_objects.get(key)
            for object_key, object_val in val.items():
                tags = json.dumps(object_val.get("tags"), sort_keys=True)
                self._add_key(self._tags, tags, object_key)

                tags_metadata = object_val.get("tags_metadata", {})
                creator_id = tags_metadata.get("creator_id", None)
                creator = self._creator_name(creator_id)
                self._add_key(self._label_creator, creator, object_key)

                timestamp = tags_metadata.get("timestamp")
                if timestamp:
                    timestamp = object_set.str_to_date(timestamp)
                    self._label_timestamp[object_key] = timestamp

        self._compute_label_duration()

    # parse_labelling

    ###################
    # PRIVATE METHODS #
    ###################

    def _compute_label_duration(self):
        # group by creators
        self._label_duration = {}
        self._compute_durations(
            self._label_duration, self._label_creator, self._label_timestamp
        )

    # _compute_label_duration

    def _compute_segment_duration(self):
        # group by creators
        self._segment_duration = {}
        self._compute_durations(
            self._segment_duration, self._segment_creator, self._segment_timestamp
        )

    # _compute_segment_duration

    def _creator_name(self, creator_id):
        """ return a user name """
        if creator_id is None:
            return None
        name = creator_id.split("_")[0]
        name = self._creator_mapper.get(name, name)
        return name

    # _creator_name

    def _get_durations(self, durations, *args):
        keys = set(durations.keys())
        for arg in args:
            keys &= set(arg)
        return [durations[k] for k in keys]

    # _get_durations

    def _load(self):
        """ load annotations """
        self.segmented_objects.load_annotations()
        self.image_metadata.load_annotations()
        self.image_status.load_annotations()

    # _load

    def _compute_durations(self, durations, creators, timestamps):
        for creator, val in creators.items():
            keys = list(val)
            ordered_keys = [(timestamps[k], k) for k in keys if k in timestamps]
            ordered_keys = sorted(ordered_keys)
            previous = datetime.datetime(datetime.MINYEAR, 1, 1, tzinfo=pytz.UTC)

            for timestamp, key in ordered_keys:
                duration = timestamp - previous
                # if duration.total_seconds() == 0:
                #     print(timestamp, previous)
                previous = timestamp
                durations[key] = duration.total_seconds()

    # _compute_durations

    ##########################
    # PRIVATE STATIC METHODS #
    ##########################

    @staticmethod
    def _add_key(container, field, key):
        if field not in container:
            container[field] = set()
        container[field].add(key)

    # _add_key

    @staticmethod
    def _count(container):
        res = {}
        for key, val in container.items():
            res[key] = len(val)
        return res
