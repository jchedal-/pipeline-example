from abc import abstractmethod

from common.utilities import recursive_get


class Worker(object):
    NAME = None         # user friendly process name
    SUBDIR_NAME = None  # pipeline step output subdirectory

    def __init__(self):
        self._module_directory = None
        self._input_directory = None
        self._output_directory = None
        self._configuration = None

    ####################
    # PUBLIC METHOD(S) #
    ####################

    def set(self, module_directory, input_directory, output_directory, **configuration):
        self._module_directory = module_directory
        self._input_directory = input_directory
        self._output_directory = output_directory
        self._configuration = configuration

    def force_computation(self, configuration=None):
        """ return true or false whether the computation """
        if configuration is None:
            configuration = self._configuration
        force_computation = recursive_get(configuration,
                                          ["force_computation", self.name()],
                                          False)
        return force_computation
    # force_computation

    def name(self):
        """ :return user friendly process name"""
        if not self.NAME:
            raise NotImplementedError('NAME should be defined in {}'.format(self.__class__))
        return self.NAME
    # name

    def output_subdir_name(self):
        """ return pipeline step output subdirectory"""
        if not self.SUBDIR_NAME:
            raise NotImplementedError('SUBDIR_NAME should be defined in {}'.format(self.__class__))
        return self.SUBDIR_NAME
    # _output_subdir_name

    @abstractmethod
    def run(self):
        """ perform the process """
        raise NotImplementedError('DetectionWorker is an abstract class. run should be implemented')
    # run

    #####################
    # PRIVATE METHOD(S) #
    #####################

    def _get_config(self, fields, default=None):
        """ get configuration parameters"""
        return recursive_get(self._configuration, fields, default)
    # _get_config
