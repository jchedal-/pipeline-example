import datetime
import fnmatch
import inspect
import logging
import os.path
import re
import shutil
from pathlib import Path

import git

import common.detection.configuration
import common.veri_global
from common.pipeline.worker import Worker
from common.utilities import recursive_dictionary_update


class Pipeline(object):
    COMPLETED_FILE = ".over"
    CONFIG_DIRECTORY = "configuration"
    CONFIG_EXT = ["*.config", "*.json"]
    GIT_COMMIT_SHA = "git_commit_sha.txt"
    PREVIOUS_DIR_FILENAME = "previous_directory.txt"

    def __init__(self, module_directory, **configuration_overrides):
        self._module_directory = module_directory
        self._module_name = os.path.split(module_directory)[-1]
        self._configuration = common.detection.configuration.load(module_directory)
        self._configuration = recursive_dictionary_update(
            self._configuration, configuration_overrides
        )

        # current worker's state
        self._output_directory = None
        self._previous_dir = ""
        self._worker = None

    def run(self, *args):
        for worker in args:
            self._run_worker(worker)

    def _run_worker(self, worker):
        if not inspect.isclass(worker):
            raise ValueError(
                "worker is {} and should be a class derived from DetectionWorker ".format(
                    type(worker)
                )
            )

        self._worker = worker()

        if not isinstance(self._worker, Worker):
            raise ValueError(
                "worker is {} and should be a class derived from DetectionWorker ".format(
                    type(worker)
                )
            )

        # get output directory
        subdir_name = self._worker.output_subdir_name()
        force = self._worker.force_computation(configuration=self._configuration)

        self._get_latest_directory(subdir_name, force)
        if not self._output_directory:
            self._get_new_directory(subdir_name)
            _make_directories(self._output_directory)

        _is_complete = self.is_complete(self._output_directory)

        if not _is_complete:
            self._worker.set(
                self._module_directory,
                self._previous_dir,
                self._output_directory,
                **self._configuration
            )
            self._worker.run()
            self._mark_as_complete()
        else:
            logging.info(
                "Computation of {} already done: {}.".format(
                    self._module_name, self._output_directory
                )
            )

        self._previous_dir = self._output_directory

    @staticmethod
    def is_complete(output_directory):
        """Return True is COMPLETED_FILE is present in output_directory
        """
        filename = os.path.join(output_directory, Pipeline.COMPLETED_FILE)
        return _is_file(filename)

    @staticmethod
    def mark_as_complete(directory):
        """Mark directory as complete by writing to COMPLETE_FILE
        """
        _make_directories(directory)
        filename = _join_path(directory, Pipeline.COMPLETED_FILE)
        _touch(filename)

    def _get_latest_directory(self, subdir_name, force):
        root_dir = _join_path(common.veri_global.VERI_OUT_DIR, self._module_name)
        self._output_directory = None
        if not force and _is_dir(root_dir):
            names = [n.name for n in _scandir(root_dir)]
            date_pattern = re.sub(
                "[0-9]", "[0-9]", re.sub("[: .]", "_", str(datetime.datetime.now()))
            )

            pattern = "{}_{}".format(subdir_name, date_pattern)
            directories = fnmatch.filter(names, pattern)
            directories = [os.path.join(root_dir, f) for f in directories]
            directories = [f for f in directories if _is_dir(f)]
            directories = sorted(directories)
            if len(directories) > 0:
                # choose most recent directory
                self._output_directory = directories[-1]

    def _get_new_directory(self, subdir_name):
        """Get new directory based on timestamps"""
        root_dir = _join_path(common.veri_global.VERI_OUT_DIR, self._module_name)
        if self._output_directory is None:
            date_string = re.sub("[: .]", "_", str(datetime.datetime.now()))
            subdir_name_with_date = "{}_{}".format(subdir_name, date_string)
            self._output_directory = _join_path(root_dir, subdir_name_with_date)

    def _mark_as_complete(self):
        """ mark a directory as complete by writing an empty file and copy configuration"""
        self.mark_as_complete(self._output_directory)

        config_directory = _join_path(
            self._output_directory, Pipeline.CONFIG_DIRECTORY
        )

        _make_directories(config_directory)

        prev_dir_file_path = _join_path(config_directory, self.PREVIOUS_DIR_FILENAME)
        _write_text_to_file(prev_dir_file_path, str(self._previous_dir))

        if self._module_directory:
            files = []
            for ext in self.CONFIG_EXT:
                files += common.io_utilities.find_files(self._module_directory, ext)
            for f in files:
                shutil.copy(f, config_directory)

        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        status = repo.git.status()
        diff = repo.git.diff()
        _write_text_to_file(
            _join_path(config_directory, Pipeline.GIT_COMMIT_SHA),
            str(sha),
            status,
            "\n",
            diff,
        )


_copy_file = shutil.copy


_is_dir = os.path.isdir


_is_file = os.path.isfile


def _join_path(*args):
    return os.path.join(*args).replace("\\", "/")


def _make_directories(directory):
    if not _is_dir(directory):
        os.makedirs(directory)


_scandir = os.scandir


def _touch(filename):
    Path(filename).touch()


def _write_text_to_file(filename, *args):
    with open(filename, "w") as f:
        for x in args:
            f.write(x)
