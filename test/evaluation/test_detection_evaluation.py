# STL
from collections.abc import Iterable
import copy
from operator import itemgetter
import random
import uuid
import warnings

# External
import numpy as np

# Project
from common.evaluation.confidence import BinomialConfidenceInterval

# ======================================================================================================================
DEFAULT_LABEL = "null"  # default label to be compatible with tensorflow


class BoundingBox(object):
    def __init__(
        self,
        ymin=None,
        xmin=None,
        ymax=None,
        xmax=None,
        height=None,
        width=None,
        absolute=True,
        image_shape=None,
    ):
        """

        :param ymin:
        :param xmin:
        :param ymax:
        :param xmax:
        :param height:
        :param width:
        :param absolute:
        :param image_shape:
        """
        ymin, ymax, height = BoundingBox._fill_none(ymin, ymax, height)
        xmin, xmax, width = BoundingBox._fill_none(xmin, xmax, width)

        self.ymin = ymin
        self.xmin = xmin
        self.ymax = ymax
        self.xmax = xmax
        self.height = height
        self.width = width
        self.image_shape = image_shape  # warning shallow copy
        if not absolute:
            if image_shape is None:
                raise ValueError("relative value with unknown image shape")
            self.__imul__(self.image_shape[:2])

    # __init__

    ###############
    # OPERATOR(S) #
    ###############

    def __mul__(self, image_factor):
        """
        :param image_factor: height_factor, width_factor
        :return: new bounding box
        """
        image_height_factor = float(image_factor[0])
        image_width_factor = float(image_factor[1])
        return BoundingBox(
            ymin=self.ymin * image_height_factor,
            xmin=self.xmin * image_width_factor,
            ymax=self.ymax * image_height_factor,
            xmax=self.xmax * image_width_factor,
            height=self.height * image_height_factor,
            width=self.width * image_width_factor,
        )

    # __mul__

    def __imul__(self, image_factor):
        """
        :param image_factor: height_factor, width_factor
        :return: new bounding box
        """
        image_height_factor = float(image_factor[0])
        image_width_factor = float(image_factor[1])

        self.ymin *= image_height_factor
        self.xmin *= image_width_factor
        self.ymax *= image_height_factor
        self.xmax *= image_width_factor
        self.height *= image_height_factor
        self.width *= image_width_factor
        return self

    # __mul__

    ####################
    # PUBLIC METHOD(S) #
    ####################

    def area(self):
        """ Computes area of box."""
        return self.width * self.height

    # area

    def relative(self, image_shape=None):
        """ compute relative bounding box"""
        if image_shape is None:
            image_shape = self.image_shape
        if image_shape is None:
            raise ValueError("unknown image shape")
        image_height_factor = 1.0 / float(self.image_shape[0])
        image_width_factor = 1.0 / float(self.image_shape[1])
        return self * (image_height_factor, image_width_factor)

    # relative

    ##################
    # PRIVATE STATIC #
    ##################
    @staticmethod
    def _fill_none(*args):
        """
            fill None values
        :param args: _min, _max, _diff
        :return:
        """
        count = args.count(None)
        if count == 0:
            return args
        elif count > 1:
            raise ValueError("Too many unknown values for bounding box")

        _min, _max, _diff = args

        if _min is None:
            _min = _max - _diff

        elif _max is None:
            _max = _min + _diff
        else:
            _diff = _max - _min

        return _min, _max, _diff

    # _fill_none


# BoundingBox


class DetectionMetric(object):
    def __init__(self, confidence_threshold=0.5, **miscellaneous):
        self.confidence = []
        self._inc_fp = []
        self._inc_tp = []
        self._image_index = []

        self.tp = None
        self.fp = None
        self.fn = None
        self.fn_value = None
        self.fp_value = None
        self.tp_value = None

        self.miscellaneous = miscellaneous

        self._confidence_threshold = confidence_threshold
        # self._nb_gt = nb_gt
        # self._tp = 0
        # self._fp = 0

        self._image_uid_to_index = dict()
        self._image_index_to_uid = []
        self._image_nb_gt = np.array([], dtype=np.float64)
        self._image_weight = np.array([], dtype=np.float64)
        # TODO IMAGE KEY SHOULD BE REPLACED WITH INDEXES

    # __init__

    def _bootstrap_copy(self):
        """ partly deep copy """
        # <SHALLOW>
        other = DetectionMetric(self._confidence_threshold, **self.miscellaneous)

        other.confidence = self.confidence
        other._inc_fp = self._inc_fp
        other._inc_tp = self._inc_tp
        other._image_index = self._image_index

        other._image_index_to_uid = self._image_uid_to_index
        other._image_index_to_uid = self._image_index_to_uid
        other._image_nb_gt = self._image_nb_gt
        # </SHALLOW>

        other._image_weight = np.zeros(
            self._image_weight.shape, dtype=self._image_weight.dtype
        )
        return other

    # _bootstrap_copy

    def add_image(self, uid, nb_gt=0, weight=1.0):
        self._partial_reset()
        self._image_uid_to_index[uid] = len(self._image_index_to_uid)
        self._image_index_to_uid.append(uid)
        self._image_nb_gt = np.append(self._image_nb_gt, nb_gt)
        self._image_weight = np.append(self._image_weight, weight)

    # add_image

    def average_precision(self):
        """ compute average precision (area under precision/recall curve)"""
        recalls = self.recalls()
        precision = self.precisions()
        return np.trapz(precision, x=recalls)

    # average_precision

    def average_precision_old_pascal_voc(self):
        recall = self.recalls()
        precision = self.non_increasing_precision()
        # tf code for integration
        # indices = np.where(recall[1:] != recall[:-1])[0] + 1
        # average_precision = np.sum(
        #     (recall[indices] - recall[indices - 1]) * precision[indices])
        # v2 = average_precision

        return np.trapz(precision, x=recall)

    # average_precision_old_pascal_voc

    def bootstrap_sample(self, k=None):
        """
        :param k: number of samples (default number of images)
        :return:
        """

        # <DRAW IMAGE KEYS WITH REPLACEMENT>
        if k is None:
            k = self._image_weight.size
        keys = np.random.choice(self._image_weight.size, k, replace=True)
        keys, counts = np.unique(np.array(keys), return_counts=True)
        # </DRAW IMAGE KEYS WITH REPLACEMENT>

        # <BUILD NEW DetectionMetric OBJECT>
        sample = self._bootstrap_copy()
        sample._image_weight[keys] = counts
        sample.update(force=True)
        # </BUILD NEW DetectionMetric OBJECT>
        return sample

    # bootstrap_sample

    def bootstrap(self, nb_draws=1000, k=None, **statistic):
        """

        :param nb_draws:
        :param k:
        :param statistic:
        :return:
        """
        results = {}
        for name in statistic:
            results[name] = []
        import time

        sample_time = 0
        function_time = 0
        for _ in range(nb_draws):
            t = time.time()
            sample = self.bootstrap_sample(k=k)
            elapsed = time.time() - t
            t = time.time()
            sample_time += elapsed
            for name in statistic:
                result = statistic[name][0](sample, *statistic[name][1:])
                results[name].append(result)
            elapsed = time.time() - t
            function_time += elapsed

        print("sample time {}, function time {}".format(sample_time, function_time))
        return results

    # bootstrap

    def inc(self, tp=0, fp=0, confidence=1.0, image_uid=None):
        """ increment value (assumed sorted by decreasing confidence)"""
        if image_uid is not None and image_uid not in self._image_uid_to_index:
            raise ValueError(
                "image uid {} has not been added to image set".format(image_uid)
            )
        self._partial_reset()
        self._inc_fp.append(fp)
        self._inc_tp.append(tp)
        self.confidence.append(confidence)
        self._image_index.append(self._image_uid_to_index[image_uid])

    # _inc

    def non_increasing_precision(self):
        precision = self.precisions()
        for i in range(len(precision) - 2, -1, -1):
            precision[i] = np.maximum(precision[i], precision[i + 1])
        return precision

    # non_increasing_precision

    def precision(self):
        return self._ratio(*self._precision())

    # precision

    def precision_ci(self, alpha, *ci):
        return self._ci(alpha, ci, *self._precision())

    # recall_ci

    def precisions(self):
        tp = np.array(self.tp, dtype=np.float32)
        fp = np.array(self.fp, dtype=np.float32)
        res = np.zeros((tp.size,), dtype=np.float32)
        d = tp + fp
        res[d > 0] = tp[d > 0] / d[d > 0]
        res = np.append(res, 0.0)
        return res

    # precisions

    def recall(self):
        return self._ratio(*self._recall())

    # recall

    def recall_ci(self, alpha, *ci):
        return self._ci(alpha, ci, *self._recall())

    # recall_ci

    def recalls(self):
        tp = np.array(self.tp, dtype=np.float32)
        fn = np.array(self.fn, dtype=np.float32)
        res = np.zeros((tp.size,), dtype=np.float32)
        d = tp + fn
        res[d > 0] = tp[d > 0] / d[d > 0]
        res = np.append(res, 1.0)
        return res

    # recalls

    def update(self, force=False):
        """ list of all tp, fp, fn with varying degree of confidence """
        if self.tp is None or force:
            inc_tp = np.array(self._inc_tp, dtype=np.float64)
            inc_fp = np.array(self._inc_fp, dtype=np.float64)

            # self._image_uid_to_index[uid] = len(self._image_index_to_uid)
            # self._image_index_to_uid.append(uid)
            # self._image_nb_gt = np.append(self._image_nb_gt, nb_gt)
            # self._image_weight = np.append(self._image_weight, weight)

            weights = self._image_weight[self._image_index]

            nb_gt = np.sum(self._image_weight * self._image_nb_gt)

            self.tp = np.cumsum(inc_tp * weights)
            self.fp = np.cumsum(inc_fp * weights)
            self.fn = nb_gt - self.tp

            index = len(self.confidence) - np.searchsorted(
                self.confidence[::-1], self._confidence_threshold
            )
            index -= 1
            self.tp_value = self.tp[index]
            self.fp_value = self.fp[index]
            self.fn_value = self.fn[index]
            return self

    # update

    #####################
    # PRIVATE METHOD(S) #
    #####################

    def _precision(self):
        return float(self.tp_value), float(self.tp_value + self.fp_value)

    # _precision

    def _recall(self):
        return float(self.tp_value), float(self.tp_value + self.fn_value)

    # _recall

    def _partial_reset(self):
        self.tp = None
        self.fp = None
        self.fn = None
        self.fn_value = None
        self.fp_value = None
        self.tp_value = None
        return self

    # _partial_reset

    ##################
    # PRIVATE STATIC #
    ##################
    @staticmethod
    def _ci(alphas, cis, nb_successes, nb_trials):
        """

        :param alphas:
        :param cis: list of confidence interval estimator cf common.evaluation.
        :param nb_successes:
        :param nb_trials:
        :return:
        """
        if not isinstance(alphas, Iterable):
            alphas = [alphas]
        estimated = DetectionMetric._ratio(nb_successes, nb_trials)
        res = {}
        for ci in cis:
            if not isinstance(ci, BinomialConfidenceInterval):
                raise ValueError(
                    "ci: {} should be a BinomialConfidenceInterval".format(ci)
                )
            name = ci.name()
            res_ci = {}
            res[name] = res_ci
            for alpha in alphas:
                lower, upper, adjusted = ci(nb_trials, nb_successes, alpha)
                res_ci[alpha] = {
                    "lower": float(lower),
                    "upper": float(upper),
                    "adjusted": float(adjusted),
                    "alpha": float(alpha),
                    "name": name,
                    "estimated": float(estimated),
                }
        return res

    # _ci

    @staticmethod
    def _ratio(numerator, denominator):
        return numerator / denominator if denominator > 0 else 0

    # _ratio


# DetectionMetric


class DetectionEvaluation(object):
    def __init__(self, **kwargs):
        """

        :param kwargs:
            + confidence_threshold: confidence threshold (default 0.5)
            + iou_threshold: Intersection over Union (i.e. Jaccard index) to consider that a detection if correct
                             (default 0.5)
        """
        # <PARAMETERS>
        self.confidence_threshold = kwargs.get("confidence_threshold", 0.5)
        self.matching_iou_threshold = kwargs.get("matching_iou_threshold", 0.5)
        # </PARAMETERS>

        # <OTHER MEMBERS>
        self._labels = set()
        self._images = set()
        self._detections = _DetectionList(self._images)
        self._ground_truth = (
            {}
        )  # dict(key: image_uid, value: dict(key: label, value: _DetectionList)
        # </OTHER MEMBERS>

        self.metrics = {}  # key: string (label), value:DetectionMetric
        self.nb_gt_boxes = {}
        self.count_comparisons = {}

        # self._has_run = False

    # __init__

    def __bool__(self):
        return True

    # __bool__

    ##################
    # PUBLIC METHODS #
    ##################

    def add_image(self, image_uid=None):
        """
            add image uid and create one if empty
        :param image_uid:
        :return: image_ui
        """
        if image_uid is None:
            uid = uuid.uuid4()
            while uid in self._images:
                uid = uuid.uuid4()
            image_uid = uid

        if image_uid in self._images:
            warnings.warn("Image id already in detection evaluation list", UserWarning)
        self._images.add(image_uid)
        if image_uid not in self._ground_truth:
            self._ground_truth[image_uid] = {}

        return image_uid

    # add_image

    def add_bounding_box(
        self,
        bounding_box,
        image_uid=None,
        ground_truth=False,
        label=None,
        confidence=1.0,
    ):
        """
            detected item
        :param image_uid:
        :param ground_truth:
        :param bounding_box:
        :param label:
        :param confidence:
        :return:
        """
        if label is None:
            label = DEFAULT_LABEL
        self._labels.add(label)
        detection = _SingleDetection(
            image_uid=image_uid,
            bounding_box=bounding_box,
            label=label,
            confidence=confidence,
        )
        if ground_truth:
            if label not in self.nb_gt_boxes:
                self.nb_gt_boxes[label] = 0
            self.nb_gt_boxes[label] += 1
            if label not in self._ground_truth[image_uid]:

                self._ground_truth[image_uid][label] = _DetectionList({image_uid})

            self._ground_truth[image_uid][label].append(detection)
        else:
            self._detections.append(detection)

    # add_detection

    def _compare_detection_counts(self):
        """
            this does not change the evaluator state
            compare number of ground-truths with the number of detections
        :return:
        """
        counts = {}
        image_count = {}
        all_labels = "all_labels"
        if all_labels in self._labels:
            all_labels = "/".join(self._labels)
        for bounding_box, confidence, label, image_uid in self._detections:
            if confidence > self.confidence_threshold:
                if image_uid not in image_count:
                    image_count[image_uid] = {}
                if label not in image_count[image_uid]:
                    image_count[image_uid][label] = 0
                image_count[image_uid][label] += 1
                if all_labels not in image_count[image_uid]:
                    image_count[image_uid][all_labels] = 0
                image_count[image_uid][all_labels] += 1

        for image_uid, count in image_count.items():
            sum_nb = 0
            sum_gt = 0
            for label in self._labels:
                nb = count.get(label, 0)
                sum_nb += nb
                if label not in counts:
                    counts[label] = {}
                nb_gt = len(self._ground_truth[image_uid].get(label, {}))
                sum_gt += nb_gt
                if np.abs(nb - nb_gt) > 10:
                    print(image_uid, label, nb_gt, nb)
                if (nb_gt, nb) not in counts[label]:
                    counts[label][(nb_gt, nb)] = 0
                counts[label][(nb_gt, nb)] += 1

            if all_labels not in counts:
                counts[all_labels] = {}
            if (sum_gt, sum_nb) not in counts[all_labels]:
                counts[all_labels][(sum_gt, sum_nb)] = 0
            counts[all_labels][(sum_gt, sum_nb)] += 1
        self.count_comparisons = {}
        for label, count in counts.items():
            self.count_comparisons[label] = [c + (n,) for c, n in count.items()]

    # _compare_detection_counts

    def run(self):
        """
            perform greedy match between detections and ground-truths
            warning this change the evaluator state
        :return: None
        """
        # warning this change the evaluator state
        # FIXME : it should be called only once
        # if self._has_run:
        #     raise RuntimeError("{}.run() should be called only once".format(self))
        # self._has_run = True

        # <RESET VALUES>
        self.metrics = {}
        for label in self._labels:
            self.metrics[label] = DetectionMetric(
                self.confidence_threshold, nb_images=len(self._images)
            )

            for image_uid in self._images:
                ground_truths = self._ground_truth.get(image_uid, {}).get(
                    label, _DetectionList()
                )
                self.metrics[label].add_image(image_uid, nb_gt=len(ground_truths))

        self._free_ground_truths()
        # </RESET VALUES>

        self._compare_detection_counts()

        # sort detected bounding boxes by decreasing confidence
        self._detections.sort()

        for bounding_box, confidence, label, image_uid in self._detections:

            ground_truths = self._ground_truth.get(image_uid, {}).get(
                label, _DetectionList()
            )
            iou = ground_truths.bounding_boxes.iou(bounding_box)
            tp = 0
            fp = 0
            try:
                match = np.argmax(iou)
                if (
                    ground_truths.is_free(match)
                    and iou[match] >= self.matching_iou_threshold
                ):
                    ground_truths.set_free(match, False)
                    tp = 1
                else:
                    fp = 1
            except ValueError:
                fp = 1
            self.metrics[label].inc(
                tp=tp, fp=fp, confidence=confidence, image_uid=image_uid
            )

        # COMPUTE TP, FN, FP
        for label in self._labels:
            self.metrics[label].update()

    # run

    ###################
    # PRIVATE METHODS #
    ###################
    def _free_ground_truths(self):
        """ mark all ground truths as not matched"""
        for image_gt in self._ground_truth.values():
            for label_gt in image_gt.values():
                label_gt.free()

    # _free_ground_truths


# DetectionEvaluation


# ======================================================================================================================
# PRIVATE CLASSES
# ======================================================================================================================


class _BoundingBoxes(object):
    def __init__(self):
        self.ymin = []
        self.xmin = []
        self.ymax = []
        self.xmax = []
        self.height = []
        self.width = []
        self.image_shape = []

    ###############
    # OPERATOR(S) #
    ###############

    def __add__(self, other):
        if self.__class__.__name__ != other.self.__class__.__name__:
            raise ValueError("other should be {}".format(other.self.__class__.__name__))
        result = _BoundingBoxes()
        result.ymin = self.ymin + other.ymin
        result.xmin = self.xmin + other.xmin
        result.ymax = self.ymax + other.ymax
        result.xmax = self.xmax + other.xmax
        result.height = self.height + other.height
        result.width = self.width + other.width
        result.image_shape = self.image_shape + other.image_shape
        return result

    # __add__

    def __iadd__(self, other):
        if self.__class__.__name__ != other.self.__class__.__name__:
            raise ValueError("other should be {}".format(other.self.__class__.__name__))

        self.ymin += other.ymin
        self.xmin += other.xmin
        self.ymax += other.ymax
        self.xmax += other.xmax
        self.height += other.height
        self.width += other.width
        self.image_shape += other.image_shape
        return self

    # __iadd__

    ###############
    # ACCESSOR(S) #
    ###############

    def __iter__(self):
        return self.zip().__iter__()

    # __iter__

    ####################
    # PUBLIC METHOD(S) #
    ####################

    def area(self):
        """ compute area of boxes"""
        return np.array(self.height) * np.array(self.width)

    def append(self, item):
        self.ymin.append(item.ymin)
        self.xmin.append(item.xmin)
        self.ymax.append(item.ymax)
        self.xmax.append(item.xmax)
        self.height.append(item.height)
        self.width.append(item.width)
        self.image_shape.append(item.image_shape)

    # append

    def intersection(self, box):
        """Compute pairwise intersection areas between boxes.

        Args:
          box: _SimpleBoundingBox

        Returns:
          a numpy array representing pairwise intersection area
        """
        # box = _SimpleBoundingBox()

        all_pairs_min_ymax = np.minimum(self.ymax, box.ymax)
        all_pairs_max_ymin = np.maximum(self.ymin, box.ymin)

        intersect_heights = np.maximum(all_pairs_min_ymax - all_pairs_max_ymin, 0)

        all_pairs_min_xmax = np.minimum(self.xmax, box.xmax)
        all_pairs_max_xmin = np.maximum(self.xmin, box.xmin)

        intersect_widths = np.maximum(all_pairs_min_xmax - all_pairs_max_xmin, 0)
        return intersect_heights * intersect_widths

        # intersection

    def iou(self, box):
        """Computes pairwise intersection-over-union between box collections.

        Args:
          box: _SimpleBoundingBox

        Returns:
          a numpy array with shape [N, M] representing pairwise iou scores.
        """
        intersect = self.intersection(box)
        area1 = self.area()
        area2 = box.area()
        union = area1 + area2 - intersect
        return intersect / union

    # iou

    def len(self):
        return len(self.ymin)

    # len

    def sub(self, item_getter):
        """
            return sub list of bounding boxes using item getter
        :param item_getter:
        :return:  _SimpleBoundingBoxes
        """
        result = _BoundingBoxes()
        result.ymin = item_getter(self.ymin)
        result.xmin = item_getter(self.xmin)
        result.ymax = item_getter(self.ymax)
        result.xmax = item_getter(self.xmax)
        result.height = item_getter(self.height)
        result.width = item_getter(self.width)
        result.image_shape = item_getter(self.image_shape)
        return result

    # sub

    def zip(self):
        return zip(
            self.ymin,
            self.xmin,
            self.ymax,
            self.xmax,
            self.height,
            self.width,
            self.image_shape,
        )

    # zip


# BoundingBoxes


class _SingleDetection(object):
    def __init__(self, bounding_box, image_uid=None, label=None, confidence=1.0):
        self.image_uid = image_uid
        self.bounding_box = bounding_box
        self.label = label
        self.confidence = confidence

    # __init__


# _SingleDetection


class _DetectionList(object):
    def __init__(self, images=None):
        self._images = set() if images is None else images
        self.label_set = set()

        self.bounding_boxes = _BoundingBoxes()
        self.bounding_box_list = []  # duplicate information
        self.confidences = []
        self.image_uid = []
        self.labels = []
        self._free = []

    # __init__

    def __iter__(self):
        return self.zip().__iter__()

    # __iter__

    def __len__(self):
        return self.labels.__len__()

    # __len__

    def append(self, item):
        """

        :param item: _SingleDetection item
        :return:
        """
        if not isinstance(item, _SingleDetection):
            raise ValueError("item is not a _SingleDetection")

        if item.image_uid not in self._images:
            warnings.warn("Image id not in image detection list", UserWarning)

        self.bounding_boxes.append(item.bounding_box)
        self.bounding_box_list.append(item.bounding_box)
        self.confidences.append(item.confidence)
        self.image_uid.append(item.image_uid)
        self.label_set.add(item.label)
        self.labels.append(item.label)
        self._free.append(True)

    # append

    def free(self):
        """ mark all bounding boxes as free (i.e. not matched)"""
        self._free = [True] * len(self._free)

    # _free

    def is_free(self, index):
        """ return True iff the index box is not matched yet"""
        return self._free[index]

    # is_free

    def set_free(self, index, value):
        self._free[index] = bool(value)

    # set_free

    def sort(self):
        """
            sort detection results by decreasing confidence score
        :return:
        """
        if len(self.confidences) == 0:
            return

        _, indexes = zip(
            *sorted(((e, i) for i, e in enumerate(self.confidences)), reverse=True)
        )

        getter = itemgetter(*indexes)

        def _getter(x):
            return list(getter(x))

        self.image_uid = _getter(self.image_uid)
        self.labels = _getter(self.labels)
        self.confidences = _getter(self.confidences)
        self._free = _getter(self._free)
        self.bounding_box_list = _getter(self.bounding_box_list)
        self.bounding_boxes = self.bounding_boxes.sub(_getter)

    # sort

    def sub(self, getter):

        """ """
        result = copy.deepcopy(self)
        result.image_uid = getter(result.image_uid)
        result.labels = getter(result.labels)
        result.confidences = getter(result.confidences)
        result._free = getter(result._free)
        result.bounding_box_list = getter(result.bounding_box_list)
        result.bounding_boxes = result.bounding_boxes.sub(getter)

        result.label_set = set(result.labels)
        return result

    # sub

    def zip(self):
        return zip(
            self.bounding_box_list, self.confidences, self.labels, self.image_uid
        )

    # zip


# _DetectionList
