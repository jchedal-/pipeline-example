"""
Created on Wed May 29 13:12:18 2019

@author: remi
"""
import logging
import warnings

# external
import numpy as np
import scipy.stats
import scipy.optimize
from scipy.stats import beta


class BinomialConfidenceInterval(object):
    AGRESTI_COULL = "Agresti Coull"
    CLOPPER_PEARSON = "Clopper Pearson Exact"
    JEFFREY = "Jeffrey's non-informative prior"
    HOEFFDING = "Hoeffding"
    WALD = "Wald"
    WILSON_SCORE = "Wilson Score"
    WILSON_SCORE_CC = "Wilson Score with Continuity Correction"

    def __init__(self, method=WILSON_SCORE_CC):
        self._method = method
        methods = {
            self.AGRESTI_COULL: self._agresti_coull,
            self.CLOPPER_PEARSON: self._clopper_pearson,
            self.JEFFREY: self._jeffrey,
            self.HOEFFDING: self._hoeffding,
            self.WALD: self._wald,
            self.WILSON_SCORE: self._wilson_score_interval,
            self.WILSON_SCORE_CC: self._wilson_score_interval_cc,
        }

        self._ci = methods[self._method]

    # __init__

    def __call__(self, nb_trials, nb_successes, alpha):
        nb_trials = np.array(nb_trials, dtype=np.float32)
        nb_successes = np.array(nb_successes, dtype=np.float32)
        return self._ci(nb_trials, nb_successes, alpha)

    # __call

    def name(self):
        return self._method

    def inverse(self, nb_trials, lower, alpha):
        """

        :param nb_trials:
        :param lower:
        :param alpha:
        :return:
        """
        nb_trials = np.array(nb_trials, dtype=np.float32)
        lower = np.array(lower, dtype=np.float32)
        return self._numerical_inversion(nb_trials, lower, alpha, self._ci)

    # inverse

    @staticmethod
    def _agresti_coull(nb_trials, nb_successes, alpha, **kwargs):
        z = kwargs.get("z", BinomialConfidenceInterval._z(alpha))
        " inverse cumulative gaussian function"
        n2 = nb_trials + z * z
        p2 = (nb_successes + z * z / 2) / n2
        return BinomialConfidenceInterval._wald(n2, p2 * n2, alpha)

    # _agresti_coull

    @staticmethod
    def _clopper_pearson(nb_trials, nb_successes, alpha, **kwargs):
        n = nb_trials
        p = nb_successes / nb_trials
        k = nb_successes

        lower = beta.ppf(alpha / 2, k, n - k + 1)
        upper = beta.ppf(1 - alpha / 2, k + 1, n - k)
        upper = np.minimum(1, upper)
        lower = np.maximum(0, lower)
        return lower, upper, p

    # _clopper_pearson

    @staticmethod
    def _hoeffding(nb_trials, nb_successes, alpha, **kwargs):
        n = nb_trials
        p = nb_successes / nb_trials
        epsilon = np.sqrt(np.log(2 / alpha) / (2 * n))
        upper_ep = np.minimum(1, p + epsilon)
        lower_ep = np.maximum(0, p - epsilon)
        return lower_ep, upper_ep, p

    # _hoeffding

    @staticmethod
    def _jeffrey(nb_trials, nb_successes, alpha, **kwargs):
        """
            credible interval with Jeffrey's non-informative prior
            https://scholar.google.fr/scholar?cluster=1724854656192528190&hl=en&as_sdt=0,5&as_vis=1
            https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval#Jeffreys_interval
        """

        lambda_prior = 0.5  # Jeffrey's prior
        nb_failures = nb_trials - nb_successes + lambda_prior
        nb_successes = nb_successes + lambda_prior

        lower = scipy.stats.beta.ppf(alpha / 2, nb_successes, nb_failures)
        upper = scipy.stats.beta.ppf(1 - alpha / 2, nb_successes, nb_failures)
        adjusted = nb_successes / (nb_successes + nb_failures)
        return lower, upper, adjusted

    # _jeffrey

    @staticmethod
    def _numerical_inversion(nb_trials, lower, alpha, direct_method):
        """
        :return:
        """
        nb_trials = np.array(nb_trials, dtype=np.float32).tolist()
        z = BinomialConfidenceInterval._z(alpha)

        n_list = []
        p_list = []

        for n in nb_trials:

            def to_inverse(p):
                """ """
                if not 0 <= p <= 1:
                    warnings.warn("wrong value p={} not in [0, 1]".format(p))
                lower_bound, _, _ = direct_method(n, p * n, alpha, z=z)
                print(p, lower_bound)
                return lower_bound - lower

            res = scipy.optimize.root(to_inverse, lower)
            logging.debug(res.message)
            if res.success:
                n_list.append(n)
                p_list.append(float(res.x))

        return np.array(n_list, dtype=np.float32), np.array(p_list, dtype=np.float32)

    # _numerical_inversion

    @staticmethod
    def _wald(nb_trials, nb_successes, alpha, **kwargs):
        n = np.array(nb_successes, dtype=np.float32)
        p = n / np.array(nb_trials, dtype=np.float32)
        z = kwargs.get("z", BinomialConfidenceInterval._z(alpha))
        " inverse cumulative gaussian function"
        epsilon = z * np.sqrt(p * (1 - p) / np.array(nb_successes, dtype=np.float32))
        upper = np.minimum(1, p + epsilon)
        lower = np.maximum(0, p - epsilon)
        return lower, upper, p

    # _wald

    @staticmethod
    def _wilson_score_interval(nb_trials, nb_successes, alpha, **kwargs):
        """
            Wilson score interval without continuity correction
            https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval#Wilson_score_interval
        :return: lower_bound, upper_bound
        """
        z = kwargs.get("z", BinomialConfidenceInterval._z(alpha))

        n = nb_trials
        p = nb_successes / nb_trials

        z2 = z * z
        adjusted = (p + z2 / (2 * n)) / (1.0 + z * z / n)
        logging.debug("adjusted: {} -> {}".format(p, adjusted))

        epsilon = z * np.sqrt(p * (1 - p) / n + z2 / (4.0 * n * n)) / (1.0 + z * z / n)

        lower = adjusted - epsilon
        upper = adjusted + epsilon
        lower = np.minimum(np.maximum(lower, 0.0), 1.0)
        upper = np.minimum(np.maximum(upper, 0.0), 1.0)
        adjusted = np.minimum(np.maximum(adjusted, 0.0), 1.0)
        return lower, upper, adjusted

    # wilson_score_interval

    @staticmethod
    def _wilson_score_interval_cc(nb_trials, nb_successes, alpha, **kwargs):
        """
            Wilson score interval with continuity correction
            https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval#Wilson_score_interval_with_continuity_correction
        :return: lower_bound, upper_bound
        """
        p = nb_successes / nb_trials
        n = nb_trials
        z = kwargs.get("z", BinomialConfidenceInterval._z(alpha))
        z2 = z * z

        x = 2 * n * p + z2
        d = 2 * (n + z2)
        y = z * np.sqrt(z2 - 1.0 / n + 4 * n * p * (1 - p) - (4 * p - 2)) + 1

        lower = (x - y) / d
        upper = (x + y) / d

        lower = np.minimum(np.maximum(lower, 0.0), 1.0)
        upper = np.minimum(np.maximum(upper, 0.0), 1.0)
        adjusted = x / d

        logging.debug("adjusted: {} -> {}".format(p, adjusted))
        return lower, upper, adjusted

    # _wilson_score_interval_cc

    @staticmethod
    def _z(alpha):
        """
        :return: the 1-alpha/2 quantile of a standard normal distribution (i.e., the probit) corresponding to the target
         error rate alpha.
        """
        return scipy.stats.norm.ppf(1 - alpha / 2)

    # _z


# BinomialConfidenceInterval
