import json
import os
from itertools import zip_longest

from common.pipeline.worker import Worker

METRICS_JSON = "metrics.json"
SUMMARY_JSON = "summary.json"


class BenchmarkWorker(Worker):
    NAME = "tf_benchmarks"
    SUBDIR_NAME = "tf_benchmarks"

    def run(self):
        metrics_filename = os.path.join(self._input_directory, METRICS_JSON)
        summary_filename = os.path.join(self._input_directory, SUMMARY_JSON)
        with open(metrics_filename) as metrics_file, open(
            summary_filename
        ) as summary_file:
            # both metrics and summary file are passed to the validator to allow flexibility in the function definition
            self._validator(
                summary=json.load(summary_file), metrics=json.load(metrics_file)
            )

    def _validator(self, **kwargs):
        """Validator used by the benchmarking worker, raises AssertionError if the tests do not pass"""
        benchmark_config = self._get_config(["benchmark"])
        for metrics_type, file_content in kwargs.items():
            config = benchmark_config.get(metrics_type, {})
            config_labels = config.get("labels", [])
            config_metrics_names = config.get("metrics_names", [])
            config_metrics = config.get("metrics", [])
            benchmark_dict = {
                label: dict(zip_longest(config_metrics_names, metric, fillvalue=-1))
                for label, metric in zip(config_labels, config_metrics)
            }
            # train because we want to fit as much as possible
            file_content = file_content.get("train", [])

            for image_metrics in file_content:
                # metrics.json has an additional depth which we need to retrieve
                if metrics_type == "metrics":
                    image_metrics = image_metrics.get("metrics", {})
                for label, metrics_dict in image_metrics.items():
                    for metric_name, metric_value in metrics_dict.items():
                        if (
                            label in config_labels
                            and label in benchmark_dict
                            and metric_name in config_metrics_names
                            and metric_value < benchmark_dict[label][metric_name]
                        ):
                            raise AssertionError(
                                f"Label {label}, {metric_name} {metric_value}"
                                f" should be > than {benchmark_dict[label][metric_name]}"
                            )
