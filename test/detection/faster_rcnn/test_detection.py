# STL
import copy
import json
import logging
import os.path
import shutil

# External
import numpy as np
import scipy.misc
from PIL import Image

# VeRI
import common.detection.configuration
import common.detection.detector
import common.utilities
import common.veri_global
from common import io_utilities
from common.database import database_descriptor
from common.database.dtb_to_tf_records import StevenLabelMapper
from common.pipeline.worker import Worker
from common.utilities import recursive_get

# add paths
from utils.visualization_utils import visualize_boxes_and_labels_on_image_array

# ======================================================================================================================
FINE_TUNE_CHECKPOINT_DIR = "detection_fine_tune_checkpoints"
LABEL_FILENAME = "image_tensor/labels.pbtxt"
MODEL_FILENAME = "image_tensor/frozen_inference_graph.pb"
GROUP_FILENAME = "image_tensor/groups.json"
LABEL_JSON = "labels.json"
# ======================================================================================================================


class DetectionWorker(Worker):
    """
        export and freeze tf checkpoints
    """

    NAME = "tf_evaluate"
    SUBDIR_NAME = "tf_evaluation"

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

        self._descriptors = None  # input dtb information
        self._detector = None  # Tensorflow detector
        self._display = None  # Display information
        self._group_keys = []  # keys of self._groups (order matter
        self._groups = None  # Image groups (e.g. test/train/evaluation)

        # <LABELS>
        self._tag_to_class_index = {}
        self._label_mapper = None
        # </LABELS>

    # __init__

    def run(self):
        self._load_detector()
        self._load_groups()
        self._load_ground_truths()
        self._run_detections()

        # FIXME IO operations should not be here
        with open(os.path.join(self._output_directory, LABEL_JSON), "w") as f:
            json.dump(self._detector.category_index, f)

    # __call__

    def _detection_class_from_tag(self, obj):
        """ return detection class id from object with tag information """
        label = self._label_mapper.object_data_to_label(obj)
        if label not in self._tag_to_class_index:
            for val in self._detector.category_index.values():
                if val.get("name") == label:
                    self._tag_to_class_index[label] = val.get("id")
        return self._tag_to_class_index[label]

    # _detection_class_from_tag

    def _image_of_detections(self, image, detection_dict):
        """ :return image with detected boxes in overlay"""
        out_image = visualize_boxes_and_labels_on_image_array(
            np.copy(image),
            detection_dict["detection_boxes"],
            detection_dict["detection_classes"],
            detection_dict["detection_scores"],
            self._detector.category_index,
            instance_masks=detection_dict.get("detection_masks"),
            use_normalized_coordinates=True,
            max_boxes_to_draw=None,
            line_thickness=recursive_get(
                self._configuration, ["eval", "display", "line_thickness"], 8
            ),
            min_score_thresh=recursive_get(
                self._configuration, ["eval", "display", "min_score_thresh"], 0.5
            ),
        )
        return out_image

    # _image_of_detections

    def _image_of_groundtruth(self, image, image_key):
        segmented_objects = self._descriptor.segmented_objects.get(image_key)

        detection_boxes = []
        detection_classes = []
        detection_scores = []

        for val in segmented_objects.values():
            # <SCORE>
            detection_scores.append(1)
            # </SCORE>

            # <BBOX>
            bbox = val.get("bounding_box", None)
            if bbox is None:
                continue
            xmin, ymin, xmax, ymax = bbox
            detection_boxes.append((ymin, xmin, ymax, xmax))
            # </BBOX>

            # <CLASS>
            detection_class = self._detection_class_from_tag(val)
            detection_classes.append(detection_class)
            # </CLASS>
        # <IMAGE>

        detection_boxes = np.array(detection_boxes)
        detection_classes = np.array(detection_classes).astype(np.uint8)
        detection_scores = np.array(detection_scores).astype(np.uint8)

        out_image = visualize_boxes_and_labels_on_image_array(
            np.copy(image),
            detection_boxes,
            detection_classes,
            detection_scores,
            self._detector.category_index,
            max_boxes_to_draw=None,
            instance_masks=None,
            use_normalized_coordinates=False,
            line_thickness=recursive_get(
                self._configuration, ["eval", "display", "line_thickness"], 8
            ),
            min_score_thresh=recursive_get(
                self._configuration, ["eval", "display", "min_score_thresh"], 0.5
            ),
        )
        # </IMAGE>
        return out_image

    def _load_detector(self):
        """ load tensorflow object detector """
        # <LABEL MAPPER>
        self._label_mapper = StevenLabelMapper(
            categories=self._get_config(["labels", "categories"])
        )
        # </LABEL MAPPER>

        # <FULL PATHS>
        exported_model_directory = self._input_directory
        path_to_frozen_graph = os.path.join(exported_model_directory, MODEL_FILENAME)
        path_to_labels = os.path.join(exported_model_directory, LABEL_FILENAME)
        path_to_groups = os.path.join(exported_model_directory, GROUP_FILENAME)
        # </FULL PATHS>

        # <COPY SOME CONFIG>
        shutil.copy(path_to_labels, self._output_directory)
        shutil.copy(path_to_groups, self._output_directory)
        # </COPY SOME CONFIG>

        # <LOAD DETECTOR>
        self._detector = common.detection.detector.Detector(
            path_to_frozen_graph, path_to_labels
        )
        # </LOAD DETECTOR>

    # _load_detector

    def _load_ground_truths(self):
        """ load information from input database """
        if not self._groups:
            logging.warning("Groups should be loaded before the database")

        root_dir = recursive_get(self._configuration, ["database", "root_dir"], "")

        image_meta_directory = recursive_get(
            self._configuration, ["database", "image_metadata"], ""
        )
        image_meta_directory = os.path.join(root_dir, image_meta_directory).replace(
            "\\", "/"
        )

        image_status_directory = recursive_get(
            self._configuration, ["database", "image_status"], ""
        )
        image_status_directory = os.path.join(root_dir, image_status_directory).replace(
            "\\", "/"
        )

        object_directory = recursive_get(
            self._configuration, ["database", "objects"], ""
        )
        object_directory = os.path.join(root_dir, object_directory).replace("\\", "/")

        config = {
            "directories": {
                "objects": object_directory,
                "status": image_status_directory,
                "metadata": image_meta_directory,
            }
        }
        self._descriptor = database_descriptor.DatabaseDescriptor(**config)

        self._group_keys = sorted(self._groups.keys())

        self._display = {}
        for key in self._group_keys:
            self._display[key] = True

        # <ADD A GROUP "ALL">
        run_on_all = recursive_get(self._configuration, ["eval", "run_on_all"], False)
        if run_on_all:
            self._group_keys.append("all")
            self._display["all"] = recursive_get(
                self._configuration, ["eval", "display_for_all"], False
            )
            self._groups["all"] = list(
                copy.deepcopy(self._descriptor.image_metadata.keys())
            )
        # </ADD A GROUP "ALL">

    # _load_ground_truths()

    def _load_groups(self):
        """ load groups (e.g. train / test / evaluation) """
        group_filename = os.path.join(self._input_directory, GROUP_FILENAME)
        with open(group_filename, "r") as f:
            self._groups = json.load(f)

    # _load_groups

    def _run_detections(self):
        if not self._detector or not self._groups:
            return

        max_example = self._get_config(["database", "max_examples"], None)
        if not max_example:
            max_example = np.inf
        for group_name in self._group_keys:

            group = self._groups[group_name]
            out_dir = os.path.join(self._output_directory, group_name)
            if not os.path.isdir(out_dir):
                os.makedirs(out_dir)
            for index, image_key in enumerate(group):
                if index >= max_example:
                    break
                self._run_single_detection(group_name, image_key, out_dir)

    # _run_detections

    def _run_single_detection(self, group_name, image_key, out_dir):
        """ run object detection on a single image """
        metadata = self._descriptor.image_metadata.get(image_key)
        image_path = metadata.get("uri")
        if not image_path:
            logging.warning(
                "group {}; image_path for {} does not exists".format(
                    group_name, image_key
                )
            )

        logging.info("group {}; image path: {}".format(group_name, image_path))
        timer = common.utilities.MultiTimer()
        timer.start()
        timer.tick("start")

        # FIXME: io operations should not be here
        with io_utilities.read_file(image_path) as f:
            image = Image.open(f)

        # remove alpha channel if any
        image_np = np.array(image, dtype=np.uint8)[:, :, :3]

        timer.tick("read image")
        output_dict = self._detector(image_np)
        timer.tick("detect")

        # Visualization of the results of a detection.
        out_image = None
        gt_image = None
        if self._display.get(group_name):
            out_image = self._image_of_detections(image_np, output_dict)
            gt_image = self._image_of_groundtruth(image_np, image_key)

        for key, val in output_dict.items():
            try:
                if hasattr(val, "tolist"):
                    val = val.tolist()
            except Exception as e:
                logging.exception(e)
            output_dict[key] = val

        result = {
            "predicted": output_dict,
            "ground_truth": self._descriptor.segmented_objects.get(image_key),
            "image_shape": list(image_np.shape),
        }
        filename = "{}.json".format(image_key).replace(":", "_")
        filename = os.path.join(out_dir, filename)
        with open(filename, "w") as fid:
            json.dump(result, fid, indent=2)
            json.dumps(result)

        # FIXME: io operations should not be here
        if out_image is not None:
            filename_detected = "{}_detected.png".format(image_key).replace(":", "_")
            filename_gt = "{}_gt.png".format(image_key).replace(":", "_")
            filename_combined = "{}_combined_gt_right.png".format(image_key).replace(
                ":", "_"
            )
            scipy.misc.imsave(os.path.join(out_dir, filename_detected), out_image)
            scipy.misc.imsave(os.path.join(out_dir, filename_gt), gt_image)
            scipy.misc.imsave(
                os.path.join(out_dir, filename_combined),
                np.concatenate((out_image, gt_image), axis=1),
            )

    # _run_single_detection


# _Evaluator


# ======================================================================================================================


# def display(metrics):
#     import matplotlib.pyplot as plt
#     plt.ioff()
#     for key, val in metrics.items():
#         print("label {}, precision {}, recall {}, tp {}, fp{}, fn{}".format(key,
#                                                                             val.precisions(),
#                                                                             val.recalls(),
#                                                                             val.tp,
#                                                                             val.fp,
#                                                                             val.fn
#                                                                             ))
#         print("Precision: {}, Recall {} ".format(val.precision(), val.recall()))
#         print("AP: {}".format(val.average_precision()))
#         plt.figure(key)
#         plt.plot(val.recalls(), val.precisions())
#         plt.grid()
#         plt.xlabel("recall (sensitivity)")
#         plt.ylabel("precision")
#     plt.show()
