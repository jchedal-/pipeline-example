# STL
import datetime
import fnmatch
import json
import logging
import os.path
import re
import shutil
import warnings
from pathlib import Path

# External
import git

import common.io_utilities
import common.utilities

# VeRI
import common.veri_global

# pip install gitpython
# env variable:
# GIT_PYTHON_GIT_EXECUTABLE = C:/Program Files/Git/mingw64/bin/git.exe

# ======================================================================================================================

COMPLETED_FILE = ".over"
GIT_COMMIT_SHA = "git_commit_sha.txt"
CONFIG_DIRECTORY = "configuration"
# ======================================================================================================================


def clear_all_tf_flags(flags):
    """ clear all tensorflow flag"""
    flags_dict = flags._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        flags.__delattr__(keys)


# clear_all_tf_flags


def find_configuration_file(directory):
    """
    :param directory:
    :return:
    """
    configuration_files = common.io_utilities.find_files(directory, "*.config")
    configuration_files = sorted(configuration_files)
    if len(configuration_files) == 0:
        raise ValueError("Missing configuration file in {}".format(directory))
    elif len(configuration_files) > 1:
        warnings.warn(
            "Multiple configuration file, pick last: {}".format(configuration_files),
            UserWarning,
        )
    configuration_file = configuration_files[-1]
    return configuration_file


# find_configuration_file


def load(
    module_directory,
    configuration=None,
    default_configuration="default/detection_configuration.json",
):
    """
        load configuration file for object detection training etc
    :param module_directory: path of the module directory
    :param configuration: (default None)
    :param default_configuration: (default default/default_configuration.json)
    :return:
    """

    if configuration is None:
        configuration = {}

    # <LOAD DEFAULT CONFIGURATION>
    default_configuration = os.path.join(
        common.veri_global.VERI_CONFIG, default_configuration
    )
    with open(default_configuration, "r") as fid:
        config = json.load(fid)
    common.utilities.recursive_dictionary_update(configuration, config)
    # </LOAD DEFAULT CONFIGURATION>

    # <FIND CONFIGURATION FILENAME(S)>
    configuration_files = common.io_utilities.find_files(module_directory, "*.json")
    if len(configuration_files) == 0:
        ValueError("Missing configuration file in {}".format(module_directory))
    elif len(configuration_files) > 1:
        warnings.warn(
            "Multiple configuration files in {}. Merging all".format(module_directory),
            UserWarning,
        )
        configuration_files = sorted(configuration_files)
    # </FIND CONFIGURATION FILENAME(S)>

    # <LOAD CONFIGURATION>
    for configuration_file in configuration_files:
        try:
            with open(configuration_file, "r") as fid:
                config = json.load(fid)
            common.utilities.recursive_dictionary_update(configuration, config)
        except Exception as e:
            logging.exception(e)
    # </LOAD CONFIGURATION>

    return configuration


# load


def is_complete(output_directory):
    """ return True is COMPLETED_FILE is present in directory"""
    return os.path.isfile(os.path.join(output_directory, COMPLETED_FILE))


# is_complete


def mark_as_complete(directory):
    """ mark a directory as complete by writing an empty file"""
    if not common.io_utilities.isdir(directory):
        common.io_utilities.makedirs(directory)

    Path(os.path.join(directory, COMPLETED_FILE)).touch()


# mark_as_complete


class OutputDirectory(object):
    PREVIOUS_DIR_FILENAME = "previous_directory.txt"
    CONFIG_EXT = ["*.config", "*.json"]

    def __init__(self, module_directory, subdir_name, previous_dir="", force=False):

        """
            find existing output directory and create one is non exists
        :param module_name:
        :param subdir_name:
        :param force: if force is True does not look for existing directory
        :return: output directory
        """
        self._module_directory = module_directory
        self._module_name = os.path.split(module_directory)[-1]
        self._output_directory = None
        self._previous_dir = previous_dir

        self._get_latest_directory(subdir_name, force)

        self._create_new_directory(subdir_name)

    # __init__

    def __enter__(self):
        _is_complete = is_complete(self._output_directory)
        if not _is_complete:
            prev_dir_file_path = os.path.join(
                self._output_directory, self.PREVIOUS_DIR_FILENAME
            )
            if os.path.isfile(prev_dir_file_path):
                with open(prev_dir_file_path, "w") as f:
                    f.write(self._previous_dir)
        else:
            logging.info(
                "Computation of {} already done: {}.".format(
                    self._module_name, self._output_directory
                )
            )
        return self._output_directory, _is_complete

    # __enter__

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._mark_as_complete()

    # __exit__

    def _create_new_directory(self, subdir_name):
        """ create new directory and """
        root_dir = os.path.join(common.veri_global.VERI_OUT_DIR, self._module_name)
        if self._output_directory is None:
            date_string = re.sub("[: .]", "_", str(datetime.datetime.now()))
            subdir_name_with_date = "{}_{}".format(subdir_name, date_string)
            self._output_directory = os.path.join(root_dir, subdir_name_with_date)

        if not os.path.isdir(self._output_directory):
            os.makedirs(self._output_directory)

    # _create_new_directory

    def _is_complete(self):
        """ return True is COMPLETED_FILE is present in directory"""
        return os.path.isfile(os.path.join(self._output_directory, COMPLETED_FILE))

    # _is_complete

    def _get_latest_directory(self, subdir_name, force):
        root_dir = os.path.join(common.veri_global.VERI_OUT_DIR, self._module_name)
        self._output_directory = None
        if not force and os.path.isdir(root_dir):
            names = [n.name for n in os.scandir(root_dir)]
            date_pattern = re.sub(
                "[0-9]", "[0-9]", re.sub("[: .]", "_", str(datetime.datetime.now()))
            )

            pattern = "{}_{}".format(subdir_name, date_pattern)
            directories = fnmatch.filter(names, pattern)
            directories = [os.path.join(root_dir, f) for f in directories]
            directories = [f for f in directories if os.path.isdir(f)]
            directories = sorted(directories)
            if len(directories) > 0:
                # chose most recent directory
                self._output_directory = directories[-1]

    # _get_latest_directory

    def _mark_as_complete(self):
        """ mark a directory as complete by writing an empty file"""
        if self._is_complete:
            return

        mark_as_complete(self._output_directory)

        # <COPY CONFIGURATION>

        config_directory = common.io_utilities.join(
            self._output_directory, CONFIG_DIRECTORY
        )
        if not common.io_utilities.isdir():
            common.io_utilities.makedirs(config_directory)

        if self._module_directory:
            for ext in self.CONFIG_EXT:
                shutil.copy(os.path.join(self._module_directory, ext), config_directory)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        status = repo.git.status()
        diff = repo.git.diff()
        with open(os.path.join(self._output_directory, GIT_COMMIT_SHA), "w") as f:
            f.write(str(sha))
            f.write(status)
            f.write("\n")
            f.write(diff)
        # </COPY CONFIGURATION>

    # mark_as_complete
