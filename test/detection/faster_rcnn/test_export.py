# STL
import logging
import os.path
import re
from shutil import copyfile, rmtree

# TF
import tensorflow as tf
from tensorflow.python.platform import flags

# VeRI
import common.detection.configuration
import common.io_utilities
from common.pipeline.worker import Worker


class ModelExporterWorker(Worker):
    """
        export and freeze tf checkpoints
    """

    NAME = "export"
    SUBDIR_NAME = "tf_export"

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def run(self):
        # <IMPORT REQUIRED MODULE>
        common.detection.configuration.clear_all_tf_flags(flags.FLAGS)
        from object_detection import export_inference_graph

        # </IMPORT REQUIRED MODULE>

        # <FIND LATEST MODEL>
        tf_configuration_file = common.detection.configuration.find_configuration_file(
            self._input_directory
        )
        logging.info("tf configuration file: {}".format(tf_configuration_file))
        export_inference_graph.FLAGS.pipeline_config_path = tf_configuration_file.replace(
            "\\", "/"
        )

        checkpoint = self._find_trained_checkpoint_prefix()
        logging.info("tf checkpoint file: {}".format(checkpoint))
        export_inference_graph.FLAGS.trained_checkpoint_prefix = checkpoint
        # TODO add target range for number of iterations
        # </FIND LATEST MODEL>

        # <EXPORT>
        for input_type in ["encoded_image_string_tensor", "image_tensor"]:

            tf.reset_default_graph()

            export_inference_graph.FLAGS.input_type = input_type
            export_inference_graph.FLAGS.output_directory = os.path.join(
                self._output_directory, input_type
            ).replace("\\", "/")

            # <CLEAR OUTPUT DIRECTORY>
            rmtree(export_inference_graph.FLAGS.output_directory, ignore_errors=True)
            # </CLEAR OUTPUT DIRECTORY>

            # argv = flags.FLAGS(sys.argv, known_only=True)
            export_inference_graph.main(None)
            # copy label map
            # add information (which model for what...)

            for filename in ["labels.pbtxt", "groups.json"]:
                src = os.path.join(self._input_directory, filename)
                dst = os.path.join(
                    export_inference_graph.FLAGS.output_directory, filename
                )
                copyfile(src, dst)

        # </EXPORT>

        # <README>
        self._create_readme()
        # </README>

    # run

    ###################
    # PRIVATE METHODS #
    ###################

    def _create_readme(self):
        """ create a readme file in the output directory"""
        with open(os.path.join(self._output_directory, "README.txt"), "w") as f:
            f.write(
                "model for Portik or Kpture: ./image_tensor/frozen_inference_graph.pb\n"
                "model for Google ML Predict: ./encoded_image_string_tensor/saved_model/saved_model.pb\n"
            )

    # _create_readme

    def _find_trained_checkpoint_prefix(self):
        """ return path of latest model"""
        files = common.io_utilities.find_files(
            self._input_directory, "*model.ckpt-[0-9]*.index"
        )
        if len(files) == 0:
            raise ValueError(
                "Missing checkpoint file in {}".format(self._input_directory)
            )
        checkpoints = map(self._path_to_checkpoint, files)
        checkpoint = max(checkpoints)

        result = os.path.join(
            self._input_directory, "model.ckpt-{}".format(checkpoint)
        ).replace("\\", "/")
        return result

    # _find_trained_checkpoint_prefix

    ##################
    # PRIVATE STATIC #
    ##################

    @staticmethod
    def _path_to_checkpoint(path):
        """ :return int checkpoint from file path
        """
        x = re.sub(".*model[.]ckpt-", "", path)
        x = re.sub("[.]index$", "", x)
        return int(x)

    # _path_to_checkpoint


# ModelExporter
