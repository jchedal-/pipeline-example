from setuptools import setup, find_packages


setup(
    name='loveai',
    version='1.0.0',
    description='ML Pipeline',
    url='https://gitlab.com/jchedal-/pipeline-example/',
    author='VERI',
    keywords='ml',
    packages=find_packages(exclude=['docs', 'test']),
    python_requires='==3.7',
    project_urls={
        'Source': 'https://gitlab.com/jchedal-/pipeline-example/',
    },
)
