"""
    convert Steven's database to tf records file with StevenDTBConverter
"""
import datetime
import hashlib
import io
import json
import logging
import math
import os
import random
import re

# STL
from collections import OrderedDict

import cv2  # to fill mask from contours
import numpy as np
import PIL.Image

# TF
import tensorflow as tf

# Project
import common.utilities

# VeRI
from common import io_utilities, tf_research, veri_global
from common.database.gs2local_uri_mapper import GS2LocalURIMapper

# EXTERNAL
try:
    import contextlib2
except ModuleNotFoundError as e:
    logging.exception(e)


EMPTY_TAG = "no_label"
PBTXT_ITEM = """item {{
  id: {0[value]}
  name: {0[key]}
}}

"""


class StevenLabelMapper(object):
    """
        convert Steven's database to TF label meta
        labels are positive integer (label 0 is reserved for background)
        file are saved in a .pbtxt
        TODO ADD DISPLAY NAME
    """

    def __init__(self, **kwargs):
        """

        :param kwargs:
            + categories (default: None): tags field to consider for label (if None: all are considered)
            + filename (default: labels.pbtxt): output filename containing label mapper information
            +
        """
        self.labels = {}
        self.categories = kwargs.get("categories", None)
        self.filename = kwargs.get("filename", "labels.pbtxt")
        self.filename_json = kwargs.get("filename_json", "labels.json")
        self.label_field = kwargs.get("label_field", "tags")

    # __init__

    def create_from_dtb(self, object_dtb, output_directory=None):
        self.labels = {}
        label_value = int(1)
        for objects in object_dtb.values():
            for single_object in objects.values():
                label_key = self.object_data_to_label(single_object)
                if label_key not in self.labels:
                    self.labels[label_key] = label_value
                    label_value += 1
        if output_directory is not None:
            self._write(
                os.path.join(output_directory, self.filename),
                os.path.join(output_directory, self.filename_json),
            )

    def __call__(self, single_object):
        label_key = self.object_data_to_label(single_object)
        if label_key in self.labels:
            return self.labels[label_key], label_key
        return int(0), ""

    # __call__

    def object_data_to_label(self, single_object):
        tags = single_object.get(self.label_field, EMPTY_TAG)
        if tags != EMPTY_TAG and self.categories is not None:
            new_tag = {}
            for cat in self.categories:
                new_tag[cat] = tags.get(cat, None)
            tags = new_tag
        label_key = json.dumps(tags, sort_keys=True)
        return label_key

    # object_data_to_label

    def _write(self, filename, filename_json):
        # TODO sort label by label values
        with open(filename, "w") as f:
            for key, value in self.labels.items():
                key = json.dumps(key)
                f.write(PBTXT_ITEM.format({"key": key, "value": value}))

        with open(filename_json, "w") as f:
            json.dump(self.labels, f, indent=4)

    # _write


# StevenLabelMapper
# ======================================================================================================================


class StevenDTBConverter(object):
    """ convert Steven's database to train/test/eval TF records"""

    FILENAME_PATTERN = "*json"

    def __init__(self, **kwargs):
        """

        :param kwargs:
            + image_meta_directory
            + image_status_directory
            + keep_empty_tag: (default: True)
            + label_mapper: (default: StevenLabelMapper())
            + max_nb_images_per_shard: (default 50) maximum number of image per shard file
            + object_directory:
            + output_directory (default: [VERI_OUT_DIR]/[datetime] or [VERI_OUT_DIR]/[module_name]_[datetime])
            + use_uri_mapper: (default: False

        """
        # <PARAMETERS>
        self.max_nb_images_per_shard = kwargs.get("max_nb_images_per_shard ", 50)

        self.uri_mapper = None  # useful to get local file from Google Storage URI
        if kwargs.get("use_uri_mapper", False):
            original_prefix = kwargs.get("uri_mapper", {}).get("original_prefix")
            target_prefix = kwargs.get("uri_mapper", {}).get("target_prefix")
            self.uri_mapper = GS2LocalURIMapper(
                original_prefix=original_prefix, target_prefix=target_prefix
            )

        # <PARAMETERS>

        self.instance_recognition = kwargs.get("instance_recognition", False)

        # <OUTPUT DIRECTORY>
        self._output_directory = veri_global.VERI_OUT_DIR
        self._output_directory = os.path.join(
            self._output_directory, re.sub("[: .]", "_", str(datetime.datetime.now()))
        )
        if "module_name" in kwargs:
            self._output_directory = "{}_{}".format(
                kwargs["module_name"], self._output_directory
            )
        self._output_directory = kwargs.get("output_directory", self._output_directory)
        print(self._output_directory)
        if not os.path.isdir(self._output_directory):
            os.makedirs(self._output_directory)
        # </OUTPUT DIRECTORY>

        # <INPUT DTB>
        self._image_meta = self._load(kwargs.get("image_meta_directory", ""))
        self._image_status = self._load(kwargs.get("image_status_directory", ""))
        self._objects = self._load(kwargs.get("object_directory", ""))
        labels = self._load(kwargs.get("labels_directory", ""))
        common.utilities.recursive_dictionary_update(self._objects, labels)

        self.keep_empty_tag = kwargs.get("keep_empty_tag", True)
        # </INPUT DTB>

        self._label_mapper = kwargs.get("label_mapper", StevenLabelMapper(**kwargs))
        self._groups = {}

    # __init__

    def filter(self, *args):
        """
            keep only images with a given status
            e.g. self.filter("SegDone", "SegStarted") keeps only image with the "SegDone" or "SegStarted" status
        :param args: accepted status
        :return: self (to allow composition)
        """
        # TODO add a list of discarded images and causes
        accepted_status = set(args)
        key_to_delete = []
        for key, value in self._image_status.items():
            if value["status"] not in accepted_status:
                key_to_delete.append(key)
            elif not self.keep_empty_tag:
                objs = self._objects.get(key, {})
                for obj in objs.values():
                    if not obj.get(self._label_mapper.label_field, False):
                        key_to_delete.append(key)
                        continue

        for key in key_to_delete:
            if key in self._image_meta:
                del self._image_meta[key]
            if key in self._image_status:
                del self._image_status[key]
            if key in self._objects:
                del self._objects[key]
        return self

    # filter

    def split(self, **groups):
        """
        not so easy a problem if we want to do it properly....
        here purely random: may have some problem if we require images to be grouped by timestamps
        build self._groups:
            key are group names,
            value are list of image_keys
        :param groups: (default: train:80%, eval:10%, test: 10%): keys: group name, values: group proportion (size)
        :return:
        """

        # FIXME ADD IMAGES WITH NO OBJECTS

        # <RESET GROUPS>
        self._groups = {}

        if groups == {}:
            groups = {"train": 0.8, "eval": 0.1, "test": 0.1}

        group_sum = sum(groups.values())
        group_name = sorted(groups.keys())
        group_cum_sum = [0] * len(group_name)
        cum_sum = 0
        for i, name in enumerate(group_name):
            cum_sum += groups[name]
            group_cum_sum[i] = float(cum_sum) / float(group_sum)
        group_cum_sum[-1] = 2  # > 1

        for name in group_name:
            self._groups[name] = list()
        # </RESET GROUPS>

        # <BUILD OBJECT TO IMAGE MAP>
        object_to_image = {}
        for image_key, objects in self._objects.items():
            nb_objects = len(objects)
            object_to_image.update(dict(zip(objects.keys(), [image_key] * nb_objects)))
        # </BUILD OBJECT TO IMAGE MAP>

        object_keys = list(object_to_image.keys())
        random.shuffle(object_keys)

        nb_images = len(self._image_status)
        image_key_available = dict(zip(self._image_status.keys(), [True] * nb_images))
        while True:
            # <PICK RANDOM IMAGE FROM RANDOM OBJECT>
            try:
                key = object_keys.pop()
            except IndexError:
                break
            image_key = object_to_image[key]
            if image_key_available[image_key]:
                image_key_available[image_key] = False
                # </PICK RANDOM IMAGE FROM RANDOM OBJECT>

                # <PICK RANDOM GROUP>
                x = random.random()
                for i, p in enumerate(group_cum_sum):
                    if x < p:
                        picked_group = group_name[i]
                        break
                # </PICK RANDOM GROUP>

                self._groups[picked_group].append(image_key)

        # <ADD IMAGE WITH NO OBJECTS>
        for image_key, available in image_key_available.items():
            if available:
                # <PICK RANDOM GROUP>
                x = random.random()
                for i, p in enumerate(group_cum_sum):
                    if x < p:
                        picked_group = group_name[i]
                        break
                # </PICK RANDOM GROUP>
                self._groups[picked_group].append(image_key)
        # </ADD IMAGE WITH NO OBJECTS>

        with open(os.path.join(self._output_directory, "groups.json"), "w") as f:
            json.dump(self._groups, f, indent=2)

    # split

    def create_tf_records(self, max_examples=None):
        """

        :param max_examples: maximum number of examples (for debug
        :return:
        """
        for group, image_keys in self._groups.items():
            nb_images = len(image_keys)

            if max_examples is not None and nb_images > max_examples:
                nb_images = max_examples

            num_shards = math.ceil(
                float(nb_images) / float(self.max_nb_images_per_shard)
            )
            output_filename = os.path.join(self._output_directory, group)

            random.shuffle(image_keys)

            with contextlib2.ExitStack() as tf_record_close_stack:
                output_tf_records = tf_research.tf_record_creation_util.open_sharded_output_tfrecords(
                    tf_record_close_stack, output_filename, num_shards
                )
                for idx, example in enumerate(image_keys):
                    if max_examples is not None and idx > max_examples:
                        break
                    if idx % 100 == 0:
                        logging.info("On image %d of %d", idx, len(image_keys))
                    try:
                        for tf_example in self._steven_dtb_to_tf_example(example):
                            shard_idx = idx % num_shards
                            output_tf_records[shard_idx].write(
                                tf_example.SerializeToString()
                            )
                    except ValueError:
                        logging.warning("Invalid example: ignoring.")

    def build_mapper(self):
        """
        :return:
        """
        self._label_mapper.create_from_dtb(self._objects, self._output_directory)
        # TODO add a second step to merge label and/or filter image with wrong labels

    # build_mapper

    def output_directory(self):
        """ get ouput directory """
        return self._output_directory

    # output_directory

    #####################
    # PRIVATE METHOD(S) #
    #####################
    def _steven_dtb_to_tf_example(self, image_key):
        """
            Notice that this function normalizes the bounding box coordinates provided
            by the raw data.
            TODO add mask for instance segmentation
        :param image_key:
        :return: The converted tf.Example.
        Raises:
              ValueError: if the image pointed to by data['filename'] is not a valid image
        """
        uri = self._image_meta[image_key].get("uri", "")
        if self.uri_mapper is not None:
            uri = self.uri_mapper(uri)

        with io_utilities.read_file(uri) as f:
            image = PIL.Image.open(f)

        with io.BytesIO() as output:
            image.save(output, format="JPEG")
            # PIL.Image: quality The image quality, on a scale from 1 (worst) to 95 (best).
            # The default is 75. Values above 95 should be avoided;
            # 100 disables portions of the JPEG compression algorithm, and results in large files
            # with hardly any gain in image quality.
            # https://pillow.readthedocs.io/en/5.1.x/handbook/image-file-formats.html#jpeg

            encoded_jpg = output.getvalue()
        key = hashlib.sha256(encoded_jpg).hexdigest()
        image = np.asarray(image)
        height = image.shape[0]
        width = image.shape[1]

        xmins = []
        ymins = []
        xmaxs = []
        ymaxs = []
        classes = []
        classes_text = []
        truncated = []
        poses = []
        difficult_obj = []
        if self.instance_recognition:
            masks = []
        if image_key in self._objects:
            for object_key, object_data in self._objects[image_key].items():
                bounding_box = object_data.get(
                    "bounding_box", None
                )  # use contours instead?
                if bounding_box is not None:
                    x_min, y_min, x_max, y_max = bounding_box
                else:
                    contours = object_data.get(
                        "contours", None
                    )  # use contours instead?
                    if contours is None:
                        continue

                difficult = False
                difficult_obj.append(int(difficult))

                xmins.append(x_min / float(width))
                ymins.append(y_min / float(height))
                xmaxs.append(x_max / float(width))
                ymaxs.append(y_max / float(height))

                class_value, class_name = self._label_mapper(object_data)

                classes_text.append(class_name.encode("utf8"))
                classes.append(int(class_value))

                truncated.append(int(False))

                if self.instance_recognition:
                    mask = np.zeros(image.shape[:2], dtype=np.uint8)
                    contours = object_data.get("contours", None)
                    if contours is not None:
                        contours = [np.array(cnt, dtype=int) for cnt in contours]
                        if len(contours) > 0:
                            cv2.drawContours(
                                mask, contours, -1, color=1, thickness=cv2.FILLED
                            )
                    masks.append(mask)

            feature_dict = {
                "image/height": tf_research.dataset_util.int64_feature(height),
                "image/width": tf_research.dataset_util.int64_feature(width),
                "image/filename": tf_research.dataset_util.bytes_feature(
                    uri.encode("utf8")
                ),
                "image/source_id": tf_research.dataset_util.bytes_feature(
                    uri.encode("utf8")
                ),
                "image/key/sha256": tf_research.dataset_util.bytes_feature(
                    key.encode("utf8")
                ),
                "image/encoded": tf_research.dataset_util.bytes_feature(encoded_jpg),
                "image/format": tf_research.dataset_util.bytes_feature(
                    "jpeg".encode("utf8")
                ),
                "image/object/bbox/xmin": tf_research.dataset_util.float_list_feature(
                    xmins
                ),
                "image/object/bbox/xmax": tf_research.dataset_util.float_list_feature(
                    xmaxs
                ),
                "image/object/bbox/ymin": tf_research.dataset_util.float_list_feature(
                    ymins
                ),
                "image/object/bbox/ymax": tf_research.dataset_util.float_list_feature(
                    ymaxs
                ),
                "image/object/class/text": tf_research.dataset_util.bytes_list_feature(
                    classes_text
                ),
                "image/object/class/label": tf_research.dataset_util.int64_list_feature(
                    classes
                ),
                "image/object/difficult": tf_research.dataset_util.int64_list_feature(
                    difficult_obj
                ),
                "image/object/truncated": tf_research.dataset_util.int64_list_feature(
                    truncated
                ),
                "image/object/view": tf_research.dataset_util.bytes_list_feature(poses),
            }
            print(feature_dict)
            # USE MASK
            if self.instance_recognition:
                # if mask_type == 'numerical':
                #     mask_stack = np.stack(masks).astype(np.float32)
                #     masks_flattened = np.reshape(mask_stack, [-1])
                #     feature_dict['image/object/mask'] = (
                #         tf_research.dataset_util.float_list_feature(masks_flattened.tolist()))
                # elif mask_type == 'png':

                encoded_mask_png_list = []
                for mask in masks:
                    img = PIL.Image.fromarray(mask)
                    output = io.BytesIO()
                    img.save(output, format="PNG")
                    encoded_mask_png_list.append(output.getvalue())
                feature_dict[
                    "image/object/mask"
                ] = tf_research.dataset_util.bytes_list_feature(encoded_mask_png_list)

            example = tf.train.Example(features=tf.train.Features(feature=feature_dict))
            yield example

    ############################
    # PRIVATE STATIC METHOD(S) #
    ############################
    @staticmethod
    def _load(directory=""):
        """
            load dictionary from a directory containing FILENAME_PATTERN files
        """
        # if not io_utilities.isdir(directory):
        #     return {}

        if directory == "":
            return {}
        directory = directory.replace("\\", "/")
        file_names = io_utilities.find_files(
            directory, StevenDTBConverter.FILENAME_PATTERN
        )
        file_names = sorted(file_names)

        result = {}
        for file_name in file_names:
            try:
                with io_utilities.read_file(os.path.join(directory, file_name)) as f:
                    result.update(json.load(f, object_pairs_hook=OrderedDict))
            except Exception as e:
                logging.exception(e)
        return result

    # _load


# ======================================================================================================================
