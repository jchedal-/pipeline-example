"""
    change path prefix: eg. gs:// (original prefix) to c:/ (target prefix)
"""

# STL
import os


class GS2LocalURIMapper(object):
    def __init__(self, original_prefix, target_prefix):
        self._original_prefix = original_prefix
        self._target_prefix = target_prefix.rstrip("\\/")

    # __init__

    def __call__(self, uri):
        if str(uri).find(self._original_prefix) == 0:
            uri = uri[len(self._original_prefix) :]
            uri = uri.lstrip("\\/")
            uri = os.path.join(self._target_prefix, uri)
        return uri

    # __call


# GS2LocalURIMapper
