"""
    Module containing pseudo database code
    objects are stored in a dictionary which is saved in json files
    each instance create a new json file to avoid writing over existing files
"""
# STL
import copy
import datetime
import json
import logging
import os
import random
import re
from collections import OrderedDict

import pytz
from dateutil import parser

# VeRI
from common import io_utilities, rwlock


def timestamp():
    """
        return timestamp in string format (use time zone)
    """
    return "%s" % datetime.datetime.now(tz=pytz.UTC)


def str_to_date(x):
    """
        parse string and return datetime
    """
    try:
        res = parser.parse(str(x))
        # FIXME Warning to correct
        # "UnicodeWarning: Unicode equal comparison failed to convert both arguments to Unicode
        # - interpreting them as being unequal "
    except ValueError as e:
        res = None
        logging.error(e)
    return res


# str_to_date
# ======================================================================================================================


class ObjectSet(object):
    """
        objects are stored in a dictionary which is saved in json files
        each instance create a new json file to avoid writing over existing files
    """

    TIMESTAMP_KEY = "__timestamp__"
    FILENAME_KEY = "__filename__"
    CONTENT_DIRECTORY = "__content__"

    def __init__(self, **kwargs):
        self.annotation_directory = kwargs.get("annotation_directory", None)
        self.filename_pattern = kwargs.get("filename_pattern", "*json")

        delta_time = kwargs.get("delta_time", {"hours": 0, "minutes": 10, "seconds": 0})
        self.delta_time = datetime.timedelta(
            hours=delta_time["hours"],
            minutes=delta_time["minutes"],
            seconds=delta_time["seconds"],
        )

        self.timestamp = None

        self.annotated_objects = OrderedDict()
        self.new_annotated_objects = OrderedDict()

        self.out_filename = ""
        self._new_out_file()

        self._object_iterator = None
        self._rwlock = rwlock.RWLock()
        self.rwlock = rwlock.RWLock()  # Read Write Lock to be used from outside
        self.all_in_cache = kwargs.get("all_in_cache", True)

    # __init__

    def __len__(self):
        return len(self.annotated_objects)

    # __len__

    def __contains__(self, item):
        with self._rwlock.writing_lock:
            return self.annotated_objects.__contains__(item)

    # __contains__
    def items(self):
        """ warning this function is not threadsafe"""
        return self.annotated_objects.items()

    def keys(self):
        return self.annotated_objects.keys()

    def __iter__(self):
        """ warning this function is not threadsafe"""
        return self.annotated_objects.__iter__()

    def load_annotations(self, **kwargs):
        """
            load dictionary from a directory
        """
        if self.annotation_directory is None:
            return
        if not io_utilities.isdir(self.annotation_directory):
            io_utilities.makedirs(self.annotation_directory)
        filenames = io_utilities.find_files(
            self.annotation_directory,
            self.filename_pattern,
            ".*{}.*".format(ObjectSet.CONTENT_DIRECTORY),
        )

        # sort files to ensure that oldest files are updated with latest
        filenames = sorted(filenames)
        with self._rwlock.writing_lock:
            for filename in filenames:
                try:
                    with io_utilities.read_file(filename) as f:
                        self.annotated_objects.update(
                            json.load(f, object_pairs_hook=OrderedDict)
                        )
                except Exception as e:
                    logging.exception(e)
                    logging.exception("filename: {}".format(filename))

        if kwargs.get("clean_not_all_in_cache", True):
            self._clean_not_all_in_cache()

    # load_annotations

    def get(self, key, **kwargs):
        """
            get item with the key 'key'
        """
        default_value = kwargs.get("default_value", dict())
        if self.all_in_cache:
            with self._rwlock.reading_lock:
                return self.annotated_objects.get(key, default_value)
        else:
            with self._rwlock.reading_lock:
                filename = self.annotated_objects.get(key, {}).get(
                    ObjectSet.FILENAME_KEY, None
                )
            if filename is not None:
                try:
                    path = os.path.join(self.annotation_directory, filename)
                    with io_utilities.read_file(path) as f:
                        result = json.load(f, object_pairs_hook=OrderedDict)
                    return result
                except Exception as e:
                    logging.exception(e)

            return default_value

    # get

    def get_first(self, filter_rule):
        """
            get first object of self.annotated_objects meeting the filter rule
            filter can modify the database
        """
        with self._rwlock.writing_lock:
            # for key, value in self.annotated_objects.items():
            keys = list(self.annotated_objects.keys())
            random.shuffle(keys)
            for key in keys:
                value = self.annotated_objects[key]
                ok, modified = filter_rule(value)
                if modified:
                    self.annotated_objects[
                        key
                    ] = value  # TODO check this is probably useless
                    self.new_annotated_objects[key] = value
                if ok:
                    return key
        return None

    # get_first

    def copy(self):
        with self._rwlock.reading_lock:
            return copy.deepcopy(self.annotated_objects)

    # copy

    def get_all(self, filter_rule=lambda x: True):
        """ warning this is a generator hence lock release once loop is over!!!"""
        # with self._rwlock.reading_lock:
        for key, value in self.annotated_objects.items():
            if filter_rule(value):
                yield key

    # get_all

    def get_next(self, filter_rule):
        """
            iteration over object set
        :param filter_rule:
        :return: key respecting filter otherwise None
        """
        with self._rwlock.writing_lock:
            if self._object_iterator is None:
                self._object_iterator = self.annotated_objects.items()
            result = None
            first_loop = True
            while result is None:
                try:
                    key, value = next(self._object_iterator)
                except StopIteration:
                    self._object_iterator = self.annotated_objects.items()
                    if first_loop:
                        first_loop = False
                        continue
                    else:
                        return ""
                if filter_rule(value):
                    return key

    # get_next

    def put(self, key, data, **kwargs):
        """add new item in self.annotated_objects and self.new_annotated_objects"""
        with self._rwlock.writing_lock:
            if self.all_in_cache:
                self.annotated_objects[key] = data
                self.new_annotated_objects[key] = data
            else:
                file_data = {
                    ObjectSet.TIMESTAMP_KEY: self.annotated_objects[key].get(
                        ObjectSet.TIMESTAMP_KEY
                    ),
                    ObjectSet.FILENAME_KEY: self.annotated_objects[key].get(
                        ObjectSet.FILENAME_KEY
                    ),
                }
                # <CHECK TIMESTAMP AND CREATE NEW FILE IF NECESSARY>
                new_file = file_data[ObjectSet.TIMESTAMP_KEY] is None
                new_file = new_file or file_data[ObjectSet.FILENAME_KEY] is None

                if new_file:
                    time_threshold = datetime.datetime.now() - self.delta_time
                    new_file = (
                        new_file
                        or str_to_date(file_data[ObjectSet.TIMESTAMP_KEY])
                        < time_threshold
                    )

                if new_file:
                    file_data[ObjectSet.TIMESTAMP_KEY] = timestamp()
                    file_data[ObjectSet.FILENAME_KEY] = self._get_filename(
                        key, file_data[ObjectSet.TIMESTAMP_KEY]
                    )
                # </CHECK TIMESTAMP AND CREATE NEW FILE IF NECESSARY>

                self.annotated_objects[key] = file_data
                self.new_annotated_objects[key] = file_data

                # <SAVE IN FILE>

                path = os.path.join(
                    self.annotation_directory, file_data[ObjectSet.FILENAME_KEY]
                )
                print("save in : {}".format(path))
                with io_utilities.write_file(path) as f:
                    json.dump(data, f, indent=0)
                    # </SAVE IN FILE>

        if kwargs.get("save", True):
            self.save()

    # put

    def save(self):
        """write self.new_annotated_objects in a file"""
        if self.annotation_directory is None:
            return

        with self._rwlock.reading_lock:
            new_file = (
                datetime.datetime.now(tz=pytz.UTC) > self.timestamp + self.delta_time
            )

            if not io_utilities.isdir(self.annotation_directory):
                io_utilities.makedirs(self.annotation_directory)

            if not new_file:
                with io_utilities.write_file(
                    os.path.join(self.annotation_directory, self.out_filename)
                ) as f:
                    json.dump(self.new_annotated_objects, f, indent=0)

        if new_file:
            with self._rwlock.writing_lock:
                with io_utilities.write_file(
                    os.path.join(self.annotation_directory, self.out_filename)
                ) as f:
                    json.dump(self.new_annotated_objects, f, indent=0)
                self.new_annotated_objects = dict()
                self._new_out_file()

    # save

    def _new_out_file(self):
        self.timestamp = datetime.datetime.now(tz=pytz.UTC)
        self.out_filename = ""
        self.out_filename = "%s" % self.timestamp + ".json"
        self.out_filename = self.out_filename.replace(":", "-")
        self.out_filename = self.out_filename.replace(" ", "_")

    # _new_out_file

    #####################
    # PRIVATE METHOD(S) #
    #####################

    def _clean_not_all_in_cache(self, silent=True):
        """ to change from former all in cache to not all in cache """
        # TODO: same thing the other way round
        if not silent:
            print("_clean_not_all_in_cache")

        if self.all_in_cache:
            return

        updated = False

        for key, val in self.annotated_objects.items():
            print("key: {}".format(key))
            if not (ObjectSet.TIMESTAMP_KEY in val and ObjectSet.FILENAME_KEY in val):
                print("put key: {}".format(key))
                self.put(key, val, save=False)
                updated = True

        if updated:
            self.save()

    # _clean_not_all_in_cache

    ####################
    # STATIC METHOD(S) #
    ####################

    @staticmethod
    def _get_filename(key, timestamp):
        """ return filename from key and timestamp for not all in cache """
        filename = "{}_{}".format(key, timestamp)
        # filename = re.sub("[:+]", '_', filename)
        filename = re.sub("[:]", "-", filename)
        filename = re.sub("[ ]", "_", filename)
        filename = "{}.json".format(filename)
        filename = os.path.join(ObjectSet.CONTENT_DIRECTORY, filename)

        return filename
        # _get_filename


# ObjectSet
# ======================================================================================================================
