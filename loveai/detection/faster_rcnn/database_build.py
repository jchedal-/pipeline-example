# STL
import os.path

# VeRI
from common.database.dtb_to_tf_records import StevenDTBConverter
from common.pipeline.worker import Worker


class DtbToTFRecordsWorker(Worker):
    """
        convert Steven of Stevie database to tensorflow records
    """

    NAME = "database"
    SUBDIR_NAME = "tf_records"

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def run(self):
        root_dir = self._get_config(["database", "root_dir"], "")
        image_meta_directory = self._get_config(["database", "image_metadata"], "")
        image_status_directory = self._get_config(["database", "image_status"], "")
        object_directory = self._get_config(["database", "objects"], "")
        labels_directory = self._get_config(["database", "labels"], "")

        converter = StevenDTBConverter(
            output_directory=self._output_directory,
            image_meta_directory=os.path.join(root_dir, image_meta_directory),
            image_status_directory=os.path.join(root_dir, image_status_directory),
            object_directory=os.path.join(root_dir, object_directory),
            labels_directory=os.path.join(root_dir, labels_directory),
            use_uri_mapper=self._get_config(["database", "use_uri_mapper"], False),
            uri_mapper=self._get_config(["database", "uri_mapper"], {}),
            keep_empty_tag=self._get_config(["labels", "keep_empty_tag"]),
            categories=self._get_config(["labels", "categories"]),
        )

        accepted_status = self._get_config(["database", "accepted_statuses"], [])
        converter.filter(*accepted_status)

        converter.build_mapper()

        converter.split(
            train=self._get_config(["database", "split", "train"], 0.8),
            test=self._get_config(["database", "split", "test"], 0.15),
            eval=self._get_config(["database", "split", "eval"], 0.05),
        )
        # TODO ADD TIME SORTING ... TO AVOID OVERLAPPING
        # TODO USE PERCENT ONLY IS NUMBER if < 1
        # TODO USE REMAINING IF NUMBER < 0

        converter.create_tf_records(
            max_examples=self._get_config(["database", "max_examples"])
        )
