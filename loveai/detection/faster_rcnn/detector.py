# STL
import logging
import time

# External
import numpy as np
import tensorflow as tf
from tensorflow.python.client import device_lib

# add paths
from common import tf_research

# import from tf research
from utils import label_map_util

# ======================================================================================================================


class Detector(object):
    def __init__(self, path_to_frozen_graph, path_to_labels=None):
        self._device_info = str(device_lib.list_local_devices())
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(path_to_frozen_graph, "rb") as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name="")

        self.category_index = None
        if path_to_labels:
            self.category_index = label_map_util.create_category_index_from_labelmap(
                path_to_labels, use_display_name=True
            )
            logging.info(str(self.category_index))

        with detection_graph.as_default():
            self.sess = tf.Session()

            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            tensor_dict = {}
            for key in [
                "num_detections",
                "detection_boxes",
                "detection_scores",
                "detection_classes",
            ]:
                tensor_name = key + ":0"
                if tensor_name in all_tensor_names:
                    tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                        tensor_name
                    )

            self.image_tensor = tf.get_default_graph().get_tensor_by_name(
                "image_tensor:0"
            )
            # self.image_tensor = tf.get_default_graph().get_tensor_by_name('encoded_image_string_tensor:0')
            self.tensor_dict = tensor_dict

    # __init__

    def __call__(self, image):
        # Get handles to input and output tensors

        # Run inference
        t = time.time()
        output_dict = self.sess.run(
            self.tensor_dict, feed_dict={self.image_tensor: np.expand_dims(image, 0)}
        )
        elapsed = time.time() - t

        # all outputs are float32 numpy arrays, so convert types as appropriate
        output_dict["num_detections"] = int(output_dict["num_detections"][0])
        output_dict["detection_classes"] = output_dict["detection_classes"][0].astype(
            np.uint8
        )
        output_dict["detection_boxes"] = output_dict["detection_boxes"][0]
        output_dict["detection_scores"] = output_dict["detection_scores"][0]
        output_dict["prediction_time"] = elapsed
        output_dict["device"] = self._device_info
        return output_dict

    # __call__


# Detector
