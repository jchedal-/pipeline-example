# STL
import copy
import json
import logging
import os.path
import warnings

# External
import numpy as np

import common.detection.configuration
import common.detection.detector
import common.utilities
import common.veri_global
from common.database.dtb_to_tf_records import StevenLabelMapper
from common.evaluation import detection_evaluation
from common.evaluation.confidence import BinomialConfidenceInterval
from common.pipeline.worker import Worker
from common.utilities import recursive_get

# ======================================================================================================================
LABEL_JSON = "labels.json"
METRICS_FILENAME = "metrics.json"
METRICS_SUMMARY_FILENAME = "summary.json"
# ======================================================================================================================


class MetricsWorker(Worker):
    NAME = "tf_metrics"
    SUBDIR_NAME = "tf_metrics"

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def run(self):
        confidence_thresholds = self._get_config(
            ["eval", "confidence_threshold"], [0.75]
        )
        matching_iou_thresholds = self._get_config(
            ["eval", "matching_iou_threshold"], [0.5]
        )
        ci_configuration = self._get_config(["eval", "ci"], {})

        gt_label_mapper = StevenLabelMapper(
            categories=self._get_config(["labels", "categories"])
        )

        label_filename = os.path.join(self._input_directory, LABEL_JSON)

        with open(label_filename, "r") as f:
            label_mapper = json.load(f)

        sub_directories = next(os.walk(self._input_directory))[1]
        summary = {}
        full_metrics = {}
        for sub_directory in sub_directories:
            evaluations = []
            for confidence_threshold in confidence_thresholds:
                for matching_iou_threshold in matching_iou_thresholds:

                    evaluation = detection_evaluation.DetectionEvaluation(
                        confidence_threshold=confidence_threshold,
                        matching_iou_threshold=matching_iou_threshold,
                    )
                    evaluations.append(evaluation)

            file_names = common.io_utilities.find_files(
                os.path.join(self._input_directory, sub_directory), "*.json"
            )
            file_names = sorted(file_names)
            for num, filename in enumerate(file_names):
                logging.info("{}/{} image {}".format(num, len(file_names), filename))
                try:
                    with open(filename, "r") as f:
                        metadata = json.load(f)
                except Exception as e:
                    logging.exception(e)
                    continue
                image_shape = metadata.get("image_shape")

                image_uid = os.path.split(filename)[-1]
                for evaluation in evaluations:
                    image_uid = evaluation.add_image(image_uid)

                ground_truth = metadata.get("ground_truth", {})
                for val in ground_truth.values():
                    bbox = val.get("bounding_box", None)
                    if bbox is None:
                        continue
                    label = gt_label_mapper.object_data_to_label(val)
                    # label = val.get("tags")
                    # if tag_field is not None:
                    #     label = label.get(tag_field)
                    # if label is None:
                    #     warnings.warn("Label is None", UserWarning)
                    xmin, ymin, xmax, ymax = bbox

                    for evaluation in evaluations:
                        bounding_box = detection_evaluation.BoundingBox(
                            ymin=ymin, xmin=xmin, ymax=ymax, xmax=xmax
                        )
                        evaluation.add_bounding_box(
                            bounding_box=bounding_box,
                            image_uid=image_uid,
                            ground_truth=True,
                            label=label,
                        )
                    # TODO ADD CHECK bbox vs contour

                predicted = metadata.get("predicted", {})

                num_detections = predicted.get("num_detections", 0)
                detection_boxes = predicted.get("detection_boxes", [])
                detection_scores = predicted.get("detection_scores", [])
                detection_classes = predicted.get("detection_classes", [])

                for i in range(num_detections):
                    ymin, xmin, ymax, xmax = detection_boxes[i]
                    for evaluation in evaluations:
                        bounding_box = detection_evaluation.BoundingBox(
                            ymin=ymin,
                            xmin=xmin,
                            ymax=ymax,
                            xmax=xmax,
                            absolute=False,
                            image_shape=image_shape,
                        )
                        label = detection_classes[i]
                        label = label_mapper.get(str(label), {}).get("name")
                        if label is None:
                            warnings.warn("Label is None", UserWarning)
                        confidence = detection_scores[i]

                        evaluation.add_bounding_box(
                            bounding_box=bounding_box,
                            image_uid=image_uid,
                            ground_truth=False,
                            label=label,
                            confidence=confidence,
                        )

            for index, evaluation in enumerate(evaluations):
                evaluation.run()

            # <COUNT COMPARISON>
            _display_count_comparison(sub_directory, evaluations)
            # </COUNT>

            summary[sub_directory], full_metrics[sub_directory] = _display(
                sub_directory, evaluations, **ci_configuration
            )
        with open(os.path.join(self._output_directory, METRICS_FILENAME), "w") as f:
            json.dump(full_metrics, f, cls=_MyEncoder, indent=2)
        # common.detection.configuration.mark_as_complete(output_directory, module_directory)

        with open(
            os.path.join(self._output_directory, METRICS_SUMMARY_FILENAME), "w"
        ) as f:
            json.dump(summary, f, cls=_MyEncoder, indent=2)

        if not self._get_config(["plot", "hide"], default=False):
            import matplotlib

            matplotlib.use("TkAgg")
            import matplotlib.pyplot as plt

            plt.show()


# run
# compute_metrics
# ======================================================================================================================


def _display(sub_directory, evaluations, **kwargs):

    import matplotlib

    matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt

    full_metrics = []
    for evaluation in evaluations:
        full_metrics.append(
            {
                "matching_iou_threshold": evaluation.matching_iou_threshold,
                "confidence_threshold": evaluation.confidence_threshold,
                "metrics": copy.deepcopy(evaluation.metrics),
            }
        )

    alphas = kwargs.get("ci_alphas", [0.05, 0.025, 0.0125])
    # alpha = kwargs.get("ci_alpha", [.05])

    # FIXME MAGIC NUMBER
    ci = [
        BinomialConfidenceInterval(BinomialConfidenceInterval.WILSON_SCORE),
        BinomialConfidenceInterval(BinomialConfidenceInterval.WILSON_SCORE_CC),
        BinomialConfidenceInterval(BinomialConfidenceInterval.WALD),
        BinomialConfidenceInterval(BinomialConfidenceInterval.JEFFREY),
    ]

    # main_ci = BinomialConfidenceInterval(BinomialConfidenceInterval.WILSON_SCORE_CC)

    full_summary = []
    plt.ioff()
    metrics = None
    for evaluation in evaluations:
        # for sub_directory, metrics in evaluation_metrics.items():
        metrics = evaluation.metrics
        summary = {}
        for key, val in metrics.items():
            print("")
            print("{}: {}".format(sub_directory, key))

            summary[key] = {
                "precision": val.precision(),
                "precision_ci": val.precision_ci(alphas, *ci),
                "recall": val.recall(),
                "recall_ci": val.recall_ci(alphas, *ci),
                "AP": val.average_precision(),
                "matching_iou_threshold": evaluation.matching_iou_threshold,
                "confidence_threshold": evaluation.confidence_threshold,
                "tp": val.tp_value,
                "fp": val.fp_value,
                "fn": val.fn_value,
                "miscellaneous": val.miscellaneous,
            }
            print("Precision: {}, Recall {} ".format(val.precision(), val.recall()))
            print("AP: {}".format(val.average_precision()))

            res = val.bootstrap(
                1000, precision=(detection_evaluation.DetectionMetric.precision,)
            )
            for key, vals in res.items():
                print("========================================", key, np.median(vals))

            plt.figure("{}: {}".format(sub_directory, key))

            plt.plot(val.recalls(), val.precisions())

        full_summary.append(summary)
    if metrics:
        for key, val in metrics.items():
            plt.figure("{}: {}".format(sub_directory, key))
            plt.grid(True)
            plt.xlabel("recall (sensitivity)")
            plt.ylabel("precision")
            plt.legend(
                [
                    "iou: {}, confidence: {}".format(
                        evaluation.matching_iou_threshold,
                        evaluation.confidence_threshold,
                    )
                    for evaluation in evaluations
                ]
            )

    return full_summary, full_metrics


def _display_count_comparison(sub_directory, evaluations):
    # <COUNT>
    # should be done prior to "run"
    graphs = set()

    for index, evaluation in enumerate(evaluations):
        counts = evaluation.count_comparisons
        for key in counts:
            graph_key = (
                sub_directory + "_" + key + "_" + str(evaluation.confidence_threshold)
            )
            if graph_key in graphs:
                continue
            graphs.add(graph_key)
            nb_gt, nb, c = zip(*counts[key])
            nb = np.array(nb)
            nb_gt = np.array(nb_gt)
            c = np.array(c)
            print(np.unique(c))
            s = 100 * c / np.max(c)
            import matplotlib

            matplotlib.use("TkAgg")
            import matplotlib.pyplot as plt

            plt.figure(graph_key)
            plt.clf()
            n = np.maximum(np.max(nb_gt), np.max(nb))
            plt.plot([0, n], [0, n], color="gray", linestyle="--", linewidth=1)
            plt.scatter(nb, nb_gt, s, c, cmap="cool")
            plt.xlabel("detected")
            plt.ylabel("ground truth")
            plt.colorbar()
            plt.grid()
            plt.xlim([0, None])
            plt.ylim([0, None])
            plt.title(key)
            plt.figure(graph_key + "_hist")
            plt.hist(nb - nb_gt)
    # plt.show()


# </COUNT>


class _MyEncoder(json.JSONEncoder):
    def default(self, o):
        try:
            return o.__dict__
        except AttributeError:
            o.tolist()
