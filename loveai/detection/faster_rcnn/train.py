# STL
import logging
import os.path
import sys
import tarfile
from shutil import copyfile

import jinja2
from six.moves.urllib.request import urlretrieve
from tensorflow.python.framework import errors

# TF
from tensorflow.python.platform import flags

import common.detection.configuration

# VeRI
import common.veri_global
from common.pipeline import Pipeline
from common.pipeline.worker import Worker

# to add paths
from utils import label_map_util


# ======================================================================================================================
FINE_TUNE_CHECKPOINT_DIR = "detection_fine_tune_checkpoints"
# ======================================================================================================================


class TrainWorker(Worker):
    NAME = "train"
    SUBDIR_NAME = "tf_train"

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def run(self):
        """ perform training """
        # <CONFIGURATION>
        pipeline_config_path = self._load_and_fill_configuration()
        model_main = self._import_module_and_set_main_flags(pipeline_config_path)
        # </CONFIGURATION>

        # <RUN MODEL>
        # Parse known flags.
        argv = flags.FLAGS(sys.argv, known_only=True)
        try:
            model_main.main(argv)
        except errors.NotFoundError as e:
            # to catch deprecated call to model export
            logging.exception(e)

        # </RUN MODEL>

        # <COPY SOME FILES>
        for filename in ["labels.pbtxt", "groups.json"]:
            src = os.path.join(self._input_directory, filename)
            dst = os.path.join(self._output_directory, filename)
            copyfile(src, dst)
        # </COPY SOME FILES>

    # run

    #####################
    # PRIVATE METHOD(S) #
    #####################

    def _import_module_and_set_main_flags(self, pipeline_config_path):
        # <IMPORT REQUIRED MODULE>
        common.detection.configuration.clear_all_tf_flags(flags.FLAGS)
        import model_main

        # </IMPORT REQUIRED MODULE>

        # <SET MAIN FLAGS>
        model_main.FLAGS.model_dir = self._output_directory
        model_main.FLAGS.num_train_steps = self._get_config(
            ["train", "num_train_steps"], 50000
        )
        model_main.FLAGS.sample_1_of_n_eval_examples = self._get_config(
            ["train", "sample_1_of_n_eval_examples"], 1
        )
        model_main.FLAGS.pipeline_config_path = pipeline_config_path
        # </SET MAIN FLAGS>
        return model_main

    # _import_module

    def _load_and_fill_configuration(self):

        # <LOAD AND FILL TF CONFIG FILE>
        configuration_file = common.detection.configuration.find_configuration_file(
            self._module_directory
        )

        train_dictionary = self._get_config(["train"], {})

        fine_tune_checkpoint_path = train_dictionary.get("fine_tune_checkpoint")
        fine_tune_checkpoint_path = _download_fine_tune_checkpoint(
            fine_tune_checkpoint_path
        )
        train_dictionary["fine_tune_checkpoint"] = fine_tune_checkpoint_path

        # FIXME CHECK NUMBER OF '?'
        train_dictionary["train_input_path"] = "train-?????-of-?????"
        train_dictionary["eval_input_path"] = "eval-?????-of-?????"
        train_dictionary["label_map_path"] = "labels.pbtxt"

        for key in ["train_input_path", "eval_input_path", "label_map_path"]:
            val = train_dictionary[key]
            val = os.path.join(self._input_directory, val)
            val = val.replace("\\", "/")
            train_dictionary[key] = val

        category_index = label_map_util.create_category_index_from_labelmap(
            train_dictionary["label_map_path"]
        )
        train_dictionary["num_classes"] = len(category_index)

        #
        with open(configuration_file, "r") as f:
            content = f.read(-1)
            template = jinja2.Template(content)

        pipeline_config_path = os.path.join(
            self._output_directory, os.path.split(configuration_file)[-1]
        )
        pipeline_config = template.render(**train_dictionary)
        with open(pipeline_config_path, "w") as f:
            f.write(pipeline_config)
        # </LOAD AND FILL TF CONFIG FILE>
        return pipeline_config_path

    # _load_and_fill_configuration


# Trainer


def _download_fine_tune_checkpoint(fine_tune_checkpoint):
    model_name = os.path.split(fine_tune_checkpoint)[-1]
    output_directory = os.path.join(
        common.veri_global.VERI_OUT_DIR, FINE_TUNE_CHECKPOINT_DIR
    )

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    zipped_model_path = os.path.join(output_directory, model_name)
    model_path = os.path.join(output_directory, ".".join(model_name.split(".")[:-2]))

    if not os.path.isfile(zipped_model_path):
        logging.info("download model checkpoint from {}".format(fine_tune_checkpoint))
        urlretrieve(fine_tune_checkpoint, zipped_model_path)
    else:
        logging.info(
            "model checkpoint from {} already downloaded".format(fine_tune_checkpoint)
        )
        # if not common.detection.configuration.is_complete(output_directory):

    if not Pipeline.is_complete(model_path):
        logging.info("extract tar file")
        with tarfile.open(zipped_model_path) as f:
            f.extractall(path=output_directory)
        Pipeline.mark_as_complete(model_path)
    else:
        logging.info("tar file already extracted")

    model_files = common.io_utilities.find_files(model_path, "*.ckpt.*")
    if len(model_files) == 0:
        raise ValueError("missing .ckpt model file in {}".format(model_path))

    model_file = model_files[0]
    model_file = model_file.split(".ckpt")[0] + ".ckpt"

    return model_file.replace("\\", "/")


# _download_fine_tune_checkpoint
