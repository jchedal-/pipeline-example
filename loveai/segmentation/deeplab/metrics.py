import glob
import json
import logging
import os
from shutil import copyfile

import numpy as np
from PIL import Image

from common.pipeline.worker import Worker


def area(im):
    """Computes the area taken by objects in an image

    :param im: np.ndarray[H, W], array representing an image in grayscale format
    :return: the number of pixel occupied by non-null classes in an image
    :rtype: int

    """
    return (im != 0).sum()


def confusion_matrix(gt, pred, labels):
    """Build confusion matrix from 2 images

    Accessing the number of pixels of label A predicted when a label B
    is present in the ground-truth is done with `matrix[A, B]`.
    May be RAM-heavy, will cache 2 * `labels` arrays of the same shape as
    gt.

    :param gt: ground-truth image
    :param pred: prediction image
    :param labels: number of labels
    :return: confusion matrix
    :rtype: np.ndarray[labels, labels]

    """
    conf_matrix = np.zeros((labels, labels), dtype=np.uint32)
    cache = {}
    for label_gt in range(labels):
        gt_indices = gt == label_gt
        for label_pred in range(labels):
            if label_pred not in cache:
                cache[label_pred] = pred == label_pred
            pred_indices = cache[label_pred]
            conf_matrix[label_pred, label_gt] = np.count_nonzero(
                np.logical_and(gt_indices, pred_indices)
            )
    return conf_matrix


def iou(conf_mat):
    """Compute the raw "Intersection over Union" given a confusion matrix

    :param conf_mat: np.ndarray[labels, labels], confusion matrix
    :returns: iou
    :rtype: np.ndarray[labels]

    """
    labels = conf_mat.shape[0]
    iou = np.zeros(labels)
    for label in range(labels):
        intersection = confusion_matrix[label, label]
        union = (
            np.sum(confusion_matrix[label, :] + confusion_matrix[:, label])
            - intersection
        )
        if union:
            iou[label] = intersection / union
        else:
            iou[label] = 1.0
    return iou



def accuracy(conf_mat):
    labels = conf_mat.shape[0]
    acc = np.empty(labels, dtype=np.float)


class MetricsWorker(Worker):
    """Generate json files for each splits, computing the following metrics and logging them in the corresponding json file
    - Confusion matrices
    - IoU and mIoU
    - Object area
    """
    NAME = "tf_metrics"
    SUBDIR_NAME = "tf_metrics"

    def run(self):
        with open(os.path.join(self._input_directory, "cache.json"), "r") as f:
            cache = json.load(f)
        labels = cache["num_classes"]
        total_summary = {}
        for split in cache["dtb_splits"]:
            output_json = {}

            total_pixels = 0
            total_conf_matrix = np.zeros((labels, labels), dtype=np.uint32)
            total_gt_area = 0
            total_pred_area = 0

            for original, inferred in zip(
                glob.glob(
                    os.path.join(self._input_directory, split, "*-original-mask.png")
                ),
                glob.glob(
                    os.path.join(self._input_directory, split, "*-inferred-mask.png")
                ),
            ):
                filename = (
                    original.replace("\\", "/").rsplit("/", 1)[1].rsplit("-", 2)[0]
                )
                logging.info("Evaluating %s/%s...", (split, filename))

                gt = np.array(Image.open(original))
                pred = np.array(Image.open(inferred))
                pixels = gt.shape[0] * gt.shape[1]

                conf_matrix = confusion_matrix(gt, pred, labels)
                mean_conf_matrix = conf_matrix / (gt.shape[0] * gt.shape[1])
                iou_ = iou(conf_matrix)

                gt_area = area(gt)
                pred_area = area(pred)

                output_json[filename] = {
                    "raw_conf_matrix": conf_matrix.tolist(),
                    "conf_matrix": mean_conf_matrix.tolist(),
                    "iou": iou_.tolist(),
                    "miou": np.mean(iou_).tolist(),
                    "height": gt.shape[0],
                    "width": gt.shape[1],
                    "area": {
                        "gt": {
                            "raw": gt_area,
                            "normalized": gt_area / pixels
                        },
                        "pred": {
                            "raw": pred_area,
                            "normalized": pred_area / pixels
                        }
                    }
                }

                total_pixels += pixels
                total_conf_matrix += conf_matrix
                total_gt_area += gt_area
                total_pred_area += pred_area

            total_iou = iou(total_conf_matrix)
            total_mean_conf_matrix = (
                total_conf_matrix / np.sum(total_conf_matrix)
            )

            total_summary[split] = {
                "raw_confusion_matrix": total_conf_matrix.tolist(),
                "confusion_matrix": total_mean_conf_matrix.tolist(),
                "iou": total_iou.tolist(),
                "miou": np.mean(total_iou).tolist(),
                "area": {
                    "gt": {
                        "raw": total_gt_area,
                        "normalized": total_gt_area / total_pixels
                    },
                    "pred": {
                        "raw": total_pred_area,
                        "normalized": total_pred_area / total_pixels
                    }
                }
            }

            with open(
                os.path.join(self._output_directory, f"summary_{split}.json"), "w"
            ) as f:
                json.dump(output_json, f, indent=4)

        with open(os.path.join(self._output_directory, f"summary.json"), "w") as f:
            json.dump(total_summary, f, indent=4)

        for filename in ["labels.pbtxt", "groups.json", "cache.json"]:
            src = os.path.join(self._input_directory, filename)
            dst = os.path.join(self._output_directory, filename)
            copyfile(src, dst)
