import cv2  # to fill mask from contours
import numpy as np
import PIL.Image

from common import io_utilities, tf_research
from common.database import dtb_to_tf_records


class StevenDTBConverter(dtb_to_tf_records.StevenDTBConverter):
    def __init__(self, *args, single_class=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._single_class = single_class

    def _steven_dtb_to_tf_example(self, image_key):
        """Retrieve an image and return the corresponding tf example

        :param image_key: Key of the image
        :returns: Image in tensorflow example format, to be fed to a graph
        :rtype: tf.train.Example

        """

        if image_key not in self._objects:
            return None

        uri = self._image_meta[image_key].get("uri", "")
        if self.uri_mapper is not None:
            uri = self.uri_mapper(uri)

        with io_utilities.read_file(uri) as f:
            raw_image = PIL.Image.open(f)

        image = np.asarray(raw_image)
        height, width, *_ = image.shape
        image = image[:, :, ::-1]  # RGB to opencv's BGR
        mask = np.zeros((height, width), dtype=np.uint8)

        for object_data in self._objects[image_key].values():
            contours = object_data.get("contours")
            if contours is None:
                continue

            class_value, _ = self._label_mapper(object_data)
            np_contours = [np.array(cnt, dtype=int) for cnt in contours]
            cv2.drawContours(
                mask, np_contours, -1, class_value if not self._single_class else 1, -1
            )

        _, png_image = cv2.imencode(".png", image)
        _, png_mask = cv2.imencode(".png", mask)

        yield tf_research.deeplab_build_data.image_seg_to_tfexample(
            png_image.tobytes(), uri, height, width, png_mask.tobytes()
        )

class StevenDTBConverterToThumbnail(StevenDTBConverter):
    def _steven_dtb_to_tf_example(self, image_key):
        """Retrieve an image and return the corresponding tf example

        :param image_key: Key of the image
        :returns: Image in tensorflow example format, to be fed to a graph
        :rtype: tf.train.Example

        """

        if image_key not in self._objects:
            return None

        uri = self._image_meta[image_key].get("uri", "")
        if self.uri_mapper is not None:
            uri = self.uri_mapper(uri)

        with io_utilities.read_file(uri) as f:
            raw_image = PIL.Image.open(f)

        image = np.asarray(raw_image)
        height, width, *_ = image.shape
        image = image[:, :, ::-1]

        for object_data in self._objects[image_key].values():
            contours = object_data.get("contours")
            bbox = object_data.get("bounding_box")
            if contours is None or bbox is None:
                continue

            mask = np.zeros((height, width), dtype=np.uint8)

            class_value, _ = self._label_mapper(object_data)
            np_contours = [np.array(cnt, dtype=int) for cnt in contours]
            cv2.drawContours(
                mask, np_contours, -1, class_value if not self._single_class else 1, -1
            )

            x_min, y_min, x_max, y_max = bbox
            y_margin = (y_max - y_min) * 0.05
            x_margin = (x_max - x_min) * 0.05

            x_min = int(max(0, x_min - x_margin))
            x_max = int(min(width, x_max + x_margin))
            y_min = int(max(0, y_min - y_margin))
            y_max = int(min(height, y_max + y_margin))

            thumbnail_image = image[y_min:y_max, x_min:x_max]
            thumbnail_mask = mask[y_min:y_max, x_min:x_max]

            _, png_image = cv2.imencode(".png", thumbnail_image)
            _, png_mask = cv2.imencode(".png", thumbnail_mask)

            thumbnail_height = y_max - y_min
            thumbnail_width = x_max - x_min

            yield tf_research.deeplab_build_data.image_seg_to_tfexample(
                png_image.tobytes(), uri, thumbnail_height, thumbnail_width, png_mask.tobytes()
            )
