import importlib
import json
import logging
import os
import tarfile
from shutil import copyfile

import numpy as np
import tensorflow as tf
from six.moves.urllib.request import urlretrieve
from tensorflow import errors

import common.detection.configuration
from common.pipeline import Pipeline
from common.pipeline.worker import Worker
from common.segmentation import evaluate
from deeplab import datasets

FINE_TUNE_CHECKPOINT_DIR = "segmentation_fine_tune_checkpoints"


class TrainWorker(Worker):
    NAME = "train"
    SUBDIR_NAME = "tf_train"

    def run(self):
        """Execute the worker and perform training steps

        Read the records containing the dataset from the input
        directory, then feed them to the model.  Accept preset
        configuration from the configuration json.

        Use polynomial learning rate decay and output logs for
        tensorboard.

        """

        with open(os.path.join(self._input_directory, "cache.json"), "r") as f:
            cache = json.load(f)
        cache["path_to_records"] = self._input_directory
        train = self._import_module_and_set_main_flags(cache)

        with open(os.path.join(self._output_directory, "cache.json"), "w") as f:
            json.dump(cache, f, indent=4)

        try:
            total_num_steps = self._get_config(["train", "num_train_steps"], 100000)
            log_train_steps = self._get_config(["train", "log_train_steps"], 1000)
            try:
                init_train_steps = int(
                    tf.train.latest_checkpoint(self._output_directory).rsplit("-", 1)[1]
                )
            except (AttributeError, ValueError, IndexError):
                init_train_steps = 0
            evaluation_logger = logging.getLogger("Evaluation")
            evaluation_logger.setLevel(logging.ERROR)
            os.makedirs(tf.flags.FLAGS.eval_logdir, exist_ok=True)
            file_handler = logging.FileHandler(
                os.path.join(tf.flags.FLAGS.eval_logdir, "errors.log")
            )
            formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
            file_handler.setFormatter(formatter)
            evaluation_logger.addHandler(file_handler)
            base_lr = tf.flags.FLAGS.base_learning_rate
            for it in range(init_train_steps, total_num_steps, log_train_steps):
                logging.info("Starting training %d/%d...", it, total_num_steps)

                # Polynomial learning rate decay
                decay = (
                    1 - (it - log_train_steps) / total_num_steps
                ) ** self._get_config(["train", "learning_power"], 0.9)
                tf.flags.FLAGS.base_learning_rate = base_lr * decay

                tf.flags.FLAGS.training_number_of_steps = it
                train(None)
                try:
                    evaluate.eval(
                        self._module_directory,
                        cache["dtb_splits"],
                        cache["num_classes"],
                        tf.flags.FLAGS.flag_values_dict(),
                    )
                except Exception as e:
                    evaluation_logger.error(str(e))

        except errors.NotFoundError as e:
            logging.exception(e)

        for filename in ["labels.pbtxt", "groups.json"]:
            src = os.path.join(self._input_directory, filename)
            dst = os.path.join(self._output_directory, filename)
            copyfile(src, dst)

    def _import_module_and_set_main_flags(self, cache):
        """Clear flags, import the model's module, set the required flags and return the training function

        :param cache: Dictionary containing parameters to be used and filled
        :returns: The function used to start training
        :rtype: Callable[Any, None]

        """

        common.detection.configuration.clear_all_tf_flags(tf.flags.FLAGS)
        import deeplab.train

        importlib.reload(deeplab.common)

        with open(os.path.join(self._input_directory, "groups.json")) as f:
            content = json.load(f)
            splits = {
                k: min(self._get_config(["database", "max_examples"]) or np.inf, len(v))
                for k, v in content.items()
            }
            cache["dtb_splits"] = splits

        if not self._get_config(["database", "use_single_class"]):
            with open(os.path.join(self._input_directory, "labels.json")) as f:
                content = json.load(f)
                num_classes = 1 + len(list(content.values()))
                cache["num_classes"] = num_classes
        else:
            cache["num_classes"] = 2  # bg + objects

        config = self._get_config(["train"], {})
        checkpoint = config.get("checkpoint")

        descriptor = datasets.data_generator.DatasetDescriptor(
            splits_to_sizes=splits, num_classes=cache["num_classes"], ignore_label=255
        )
        datasets.data_generator._DATASETS_INFORMATION[
            self._module_directory
        ] = descriptor

        # manual polynomial learning rate decay
        tf.flags.FLAGS.learning_policy = "step"
        tf.flags.FLAGS.learning_rate_decay_step = np.inf

        # dataset-related flags
        tf.flags.FLAGS.dataset = self._module_directory
        tf.flags.FLAGS.train_split = "train"
        tf.flags.FLAGS.train_logdir = self._output_directory
        tf.flags.FLAGS.dataset_dir = self._input_directory
        tf.flags.FLAGS.max_scale_factor = config.get("max_scale_factor") or 1
        tf.flags.FLAGS.min_scale_factor = config.get("min_scale_factor") or 1

        # save images from training
        tf.flags.FLAGS.save_summaries_images = True

        # retraining
        tf.flags.FLAGS.initialize_last_layer = False
        tf.flags.FLAGS.last_layers_contain_logits_only = True
        tf.flags.FLAGS.fine_tune_batch_norm = True

        # flags from config, only used if preset == "custom"
        tf.flags.FLAGS.model_variant = config.get("model_variant", "mobilenet_v2")
        tf.flags.FLAGS.output_stride = config.get("output_stride", 16)
        tf.flags.FLAGS.atrous_rates = config.get("atrous_rates", None)
        tf.flags.FLAGS.decoder_output_stride = config.get("decoder_output_stride", None)
        tf.flags.FLAGS.train_crop_size = config.get("train_crop_size", [513, 513])
        tf.flags.FLAGS.base_learning_rate = config.get("learning_rate", 0.0001)
        tf.flags.FLAGS.train_batch_size = config.get("batch_size", 16)
        checkpoint = config.get("checkpoint")

        # presets
        if config.get("preset") == "mobilenet_v2":
            tf.flags.FLAGS.model_variant = "mobilenet_v2"
            tf.flags.FLAGS.atrous_rates = None
            tf.flags.FLAGS.output_stride = 16
            tf.flags.FLAGS.image_pyramid = None
            tf.flags.FLAGS.add_image_level_feature = True
            tf.flags.FLAGS.aspp_with_batch_norm = True
            tf.flags.FLAGS.aspp_with_separable_conv = True
            tf.flags.FLAGS.multi_grid = None
            tf.flags.FLAGS.decoder_output_stride = None
            tf.flags.FLAGS.decoder_use_separable_conv = True
            tf.flags.FLAGS.merge_method = "max"
            tf.flags.FLAGS.prediction_with_upsampled_logits = True
            tf.flags.FLAGS.train_crop_size = [513, 513]
            tf.flags.FLAGS.train_batch_size = 16
            checkpoint = "http://download.tensorflow.org/models/deeplabv3_mnv2_cityscapes_train_2018_02_05.tar.gz"
        elif config.get("preset") == "xception_65":
            tf.flags.FLAGS.model_variant = "xception_65"
            tf.flags.FLAGS.atrous_rates = [6, 12, 18]
            tf.flags.FLAGS.output_stride = 16
            tf.flags.FLAGS.image_pyramid = None
            tf.flags.FLAGS.add_image_level_feature = True
            tf.flags.FLAGS.aspp_with_batch_norm = True
            tf.flags.FLAGS.aspp_with_separable_conv = True
            tf.flags.FLAGS.multi_grid = None
            tf.flags.FLAGS.decoder_output_stride = [4]
            tf.flags.FLAGS.decoder_use_separable_conv = True
            tf.flags.FLAGS.merge_method = "max"
            tf.flags.FLAGS.prediction_with_upsampled_logits = True
            tf.flags.FLAGS.train_crop_size = [321, 321]
            tf.flags.FLAGS.train_batch_size = 12
            checkpoint = "http://download.tensorflow.org/models/deeplabv3_pascal_train_aug_2018_01_04.tar.gz"

        # eval
        tf.app.flags.DEFINE_string(
            "eval_logdir",
            os.path.join(self._output_directory, "log_eval"),
            "Where to write the event logs.",
        )

        tf.app.flags.DEFINE_string(
            "checkpoint_dir", tf.flags.FLAGS.train_logdir, "Path to model checkpoints."
        )

        tf.app.flags.DEFINE_integer(
            "eval_batch_size",
            4,
            "The number of images in each batch during evaluation.",
        )

        tf.app.flags.DEFINE_list(
            "eval_crop_size", "", "Image crop size [height, width] for evaluation."
        )
        tf.flags.FLAGS.eval_crop_size = tf.flags.FLAGS.train_crop_size

        tf.app.flags.DEFINE_multi_float(
            "eval_scales", [1.0], "The scales to resize images for evaluation."
        )

        tf.app.flags.DEFINE_bool(
            "add_flipped_images", False, "Add flipped images for evaluation or not."
        )

        tf.flags.DEFINE_string(
            "eval_split", "eval", "Which split of the dataset used for evaluation"
        )

        if checkpoint:
            tf.flags.FLAGS.tf_initial_checkpoint = _download_fine_tune_checkpoint(
                checkpoint
            )

        # deeplab.common flags, subset because not every flag is useful to keep in cache
        for flag in [
            "atrous_rates",
            "output_stride",
            "model_variant",
            "image_pyramid",
            "add_image_level_feature",
            "aspp_with_batch_norm",
            "aspp_with_separable_conv",
            "multi_grid",
            "decoder_output_stride",
            "decoder_use_separable_conv",
            "merge_method",
            "prediction_with_upsampled_logits",
        ]:
            cache[flag] = tf.flags.FLAGS[flag].value

        return deeplab.train.main


def _download_fine_tune_checkpoint(fine_tune_checkpoint):
    """Download a model and return its name

    :param fine_tune_checkpoint: URL to the model
    :returns: Name of the model once it is downloaded
    :rtype: str

    """

    model_name = os.path.split(fine_tune_checkpoint)[-1]
    output_directory = os.path.join(
        common.veri_global.VERI_OUT_DIR, FINE_TUNE_CHECKPOINT_DIR
    )

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    zipped_model_path = os.path.join(output_directory, model_name)
    unzipped_model_path = os.path.join(output_directory, model_name[:-7])
    if not os.path.isfile(zipped_model_path):
        logging.info("download model checkpoint from {}".format(fine_tune_checkpoint))
        urlretrieve(fine_tune_checkpoint, zipped_model_path)
    else:
        logging.info(
            "model checkpoint from {} already downloaded".format(fine_tune_checkpoint)
        )

    if not Pipeline.is_complete(unzipped_model_path):
        logging.info("extract tar file")
        with tarfile.open(zipped_model_path) as f:
            if not os.path.exists(unzipped_model_path):
                os.makedirs(unzipped_model_path)
            if not os.path.isdir(unzipped_model_path):
                raise FileExistsError(f"{unzipped_model_path} should be a directory")
            for member in f.getmembers():
                if member.isdir():
                    continue
                filename = member.name.rsplit("/", 1)[1]
                if "model.ckpt" in filename or "frozen_inference_graph.pb" in filename:
                    f.makefile(member, os.path.join(unzipped_model_path, filename))
        Pipeline.mark_as_complete(unzipped_model_path)
    else:
        logging.info("tar file already extracted")

    return tf.train.latest_checkpoint(unzipped_model_path)
