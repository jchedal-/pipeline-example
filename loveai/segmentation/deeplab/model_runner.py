import itertools

import numpy as np
import tensorflow as tf
from deeplab import export_model


class SegmentationRunner:
    def __init__(self, frozen_graph_path):
        with tf.gfile.GFile(frozen_graph_path, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        with tf.Graph().as_default() as self.graph:
            self._input, self._output = tf.import_graph_def(
                graph_def,
                input_map=None,
                return_elements=[
                    export_model._INPUT_NAME + ":0",
                    export_model._OUTPUT_NAME + ":0",
                ],
                name="",
            )

    def run_inference(self, sess, image):
        return sess.run(self._output, feed_dict={self._input: image})

    def run_from_record(self, sess, decoder, record):
        dataset = tf.data.TFRecordDataset(record).map(decoder)
        iterator = dataset.make_one_shot_iterator()
        el = iterator.get_next()
        try:
            for i in itertools.count(0):
                filename, image, mask = sess.run(el)
                inferred = self.run_inference(sess, image)
                yield {
                    "filename": filename,
                    "image": image,
                    "mask": mask,
                    "inferred": inferred.astype(np.uint8),
                }
        except tf.errors.OutOfRangeError:
            return
