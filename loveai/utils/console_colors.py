"""
    Class to add colors when printing to Python Console
    run as main file to get an example

    Veolia Research & Innovation
    contact: remi.cuingnet@veolia.com
"""

# STL
from functools import partialmethod


class ConsoleColors:
    """
        use static function or
        print(ConsoleColors.bold(ConsoleColors.cyan("Howdy!")))
        print(ConsoleColors.green(ConsoleColors.underline("What's up?")))

        use members/methods
        print(ConsoleColors().red.bold.underline("Cheers"))

        cf https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
        cf http://ascii-table.com/ansi-escape-sequences.php
    """
    _FORMAT = '\033[{}m'
    _DELIMITER = ';'
    _END = '0'
    _STYLES = {
        # Text attributes
        "bold": '1',
        "underline": '4',
        # "blink": '5',  # does not work?
        "reverse": '7',
        # "concealed": '8',  # ???

        # Foreground colors
        "white": '30',
        "red": '31',
        "green": '32',
        "gold": '33',
        "blue": '34',
        "magenta": '35',
        "darkcyan": '36',
        "gray": '37',
        "darkgray": '90',
        "red2": '91',
        "green2": '92',
        "yellow": '93',
        "lightblue": '94',
        "purple": '95',
        "cyan": '96',
        "black": '97',

        # Background colors
        "bg_white": '40',
        "bg_red": '41',
        "bg_green": '42',
        "bg_yellow": '43',
        "bg_blue": '44',
        "bg_magenta": '45',
        "bg_cyan": '46',
        "bg_gray": '47',
    }

    def __init__(self):
        self._begin = []
        # <PLACEHOLDERS FOR AUTO-COMPLETION>
        # Text attributes
        self.bold = None
        self.underline = None
        # self.blink = None
        self.reverse = None
        # self.concealed = None

        # Foreground colors
        self.white = None
        self.red = None
        self.green = None
        self.gold = None
        self.blue = None
        self.magenta = None
        self.darkcyan = None
        self.gray = None
        self.darkgray = None
        self.red2 = None
        self.green2 = None
        self.yellow = None
        self.lightblue = None
        self.purple = None
        self.cyan = None
        self.black = None

        # Background colors
        self.bg_white = None
        self.bg_red = None
        self.bg_green = None
        self.bg_yellow = None
        self.bg_blue = None
        self.bg_magenta = None
        self.bg_cyan = None
        self.bg_gray = None
        # </PLACEHOLDERS FOR AUTO-COMPLETION>

    def __getattribute__(self, item):
        # <OVERRIDE STATIC METHODS>
        if item in ConsoleColors._STYLES:
            style_str = ConsoleColors._STYLES[item]
            self._begin.append(style_str)
            return self
        # </OVERRIDE STATIC METHODS>
        else:
            return object.__getattribute__(self, item)

    def __call__(self, text=None):
        if text is None:
            return self
        else:
            return self._format(text, self._begin)

    @staticmethod
    def _format(text, begin):
        begin = ConsoleColors._DELIMITER.join(begin)
        begin = ConsoleColors._FORMAT.format(begin)
        end = ConsoleColors._FORMAT.format(ConsoleColors._END)
        return begin + text + end

    # <PLACEHOLDERS FOR AUTO-COMPLETION>
    @staticmethod
    def bold(text): pass

    @staticmethod
    def underline(text): pass

    # @staticmethod
    # def blink(text): pass

    @staticmethod
    def reverse(text): pass

    # @staticmethod
    # def concealed(text): pass

    # Foreground colors
    @staticmethod
    def black(text): pass

    @staticmethod
    def red(text): pass

    @staticmethod
    def green(text): pass

    @staticmethod
    def gold(text): pass

    @staticmethod
    def blue(text): pass

    @staticmethod
    def magenta(text): pass

    @staticmethod
    def gray(text): pass

    @staticmethod
    def darkcyan(text): pass

    @staticmethod
    def white(text): pass

    @staticmethod
    def darkgray(text): pass

    @staticmethod
    def red2(text): pass

    @staticmethod
    def green2(text): pass

    @staticmethod
    def yellow(text): pass

    @staticmethod
    def lightblue(text): pass

    @staticmethod
    def purple(text): pass

    @staticmethod
    def cyan(text): pass

    @staticmethod
    def black(text): pass

    # Background colors
    @staticmethod
    def bg_gray(text): pass

    @staticmethod
    def bg_red(text): pass

    @staticmethod
    def bg_green(text): pass

    @staticmethod
    def bg_yellow(text): pass

    @staticmethod
    def bg_blue(text): pass

    @staticmethod
    def bg_magenta(text): pass

    @staticmethod
    def bg_cyan(text): pass

    @staticmethod
    def bg_white(text): pass
    # </PLACEHOLDERS FOR AUTO-COMPLETION>


# <ADD TRUE STATIC METHODS>
for style, value in ConsoleColors._STYLES.items():
    setattr(ConsoleColors, style, partialmethod(ConsoleColors._format, [value]))
# </ADD TRUE STATIC METHODS>
# ConsoleColors
# ======================================================================================================================


if __name__ == "__main__":
    import logging

    print(ConsoleColors.bold(ConsoleColors.cyan("Howdy!")))
    print(ConsoleColors.green(ConsoleColors.underline("What's up?")))
    print(ConsoleColors().bold.red.underline("Cheers!"))
    print("")

    for key in ConsoleColors._STYLES:
        try:
            max_length = max(map(len, ConsoleColors._STYLES))
            key_str = str(key).ljust(max_length + 1)
            print("{0}{1} (static)    {0}{2} (method)".format(key_str,
                                                              getattr(ConsoleColors, key)("text"),
                                                              ConsoleColors().__getattribute__(key)("text")))
        except AttributeError as e:
            logging.exception(e)
