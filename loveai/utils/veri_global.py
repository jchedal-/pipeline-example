"""
    define global variables
    all named VERI_* and saved in a user file
"""
import getpass
import json
import os
import re
import uuid

####################
# GLOBAL VARIABLES #
####################
VERI_CONFIG = None       # Directory containing configurations
VERI_OUT_DIR = None      # Directory for output / results
VERI_TF_RESEARCH = None  # TF research directory
VERI_ROOT = None         # Code root directory


# NOT SAVE in user file
RUNNING_TF_RESEARCH_EXISTS = False  # Is TF research directory properly imported

####################
# PUBLIC FUNCTIONS #
####################

def load():
    """ load """
    default = _get_default_config_file_path()
    user = _get_user_config_file_path()

    global_variables = globals()

    # warning order matters
    for filename in [default, user]:
        if os.path.isfile(filename):
            try:
                with open(filename, "r") as f:
                    configuration = json.load(f)
            except json.JSONDecodeError:
                configuration = {}
            global_variables.update(configuration)
# load


def save():
    """ load """
    filename = _get_user_config_file_path()
    global_variables = globals()
    configuration = {}
    for key, value in global_variables.items():
        if "VERI_" in key:
            configuration[key] = value

    with open(filename, "w") as f:
        json.dump(configuration, f, indent=2)
# save


#####################
# PRIVATE FUNCTIONS #
#####################

def _get_default_config_file_path():
    """ return default global variables """
    relative_filename = "../configuration/default/global.json"
    path = os.path.join(os.path.split(__file__)[0], relative_filename)
    return path
# _get_default_config_file_path


def _get_user_config_file_path():
    """ return the configuration filename"""
    user_id = _get_user_id()
    relative_filename = "../configuration/users/{}.json".format(user_id)
    path = os.path.join(os.path.split(__file__)[0], relative_filename)
    return path
# _get_user_config_file_path


def _get_user_id():
    """ :return a user id based on user name and mac address"""
    user_name = re.sub(" ", "_", getpass.getuser())
    mac_address = str(uuid.getnode())
    user_id = "_on_".join((user_name, mac_address))
    return user_id
# _get_user_id


load()
