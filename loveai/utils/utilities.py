
# STL
import collections
import colorsys
import copy
import fnmatch
from functools import reduce
from functools import wraps
from functools import partialmethod
import logging
import json
import operator
import os
import pathlib
import random
import sys
import glob
try:
    import psutil
except ModuleNotFoundError as e:
    print(e)
import tkinter as tk
from tkinter import simpledialog


# External
import cv2
import git
# pip install gitpython
# env variable:
# GIT_PYTHON_GIT_EXECUTABLE = C:/Program Files/Git/mingw64/bin/git.exe
# C:\Users\remi.cuingnet\AppData\Local\Programs\Git\mingw64\bin
import numpy as np

# Project
import common.string_operations
import common.veri_global
# ======================================================================================================================


def get_configuration(class_instance):
    """
    get recursively configuration
    :param class_instance:
    :return:
    """
    configuration = dict()
    for key, _object in class_instance.__dict__.items():
            if hasattr(_object, "get_configuration"):
                configuration[key] = _object.get_configuration()
            else:
                configuration[key] = _object
    return configuration
# get_configuration


def set_configuration(class_instance, configuration):
    """
        set recursively configuration (if a member has the method "set_configuration"
    :param class_instance: instance of an object
    :param configuration: dictionary of which keys are the object members
    :return: the modified objects
    """

    for _attr in configuration:
        if hasattr(class_instance, _attr):
            _object = getattr(class_instance, _attr)
            if hasattr(_object, "set_configuration"):
                _object.set_configuration(configuration[_attr])
                setattr(class_instance, _attr, _object)
            else:
                setattr(class_instance, _attr, configuration[_attr])
    return class_instance
# set_configuration
# ======================================================================================================================


def join(*args):
    return os.path.join(*args).replace("\\", "/")


def find_files(directory, pattern):
    """ Return list of files matching pattern in base folder. """
    names = [n.name for n in os.scandir(directory)]
    return [n for n in fnmatch.filter(names, pattern) if os.path.isfile(os.path.join(directory, n))]
# find_files


def find_dirs(directory, pattern):
    """ Return list of directories matching pattern in base folder. """
    names = [n.name for n in os.scandir(directory)]
    return [n for n in fnmatch.filter(names, pattern) if os.path.isdir(os.path.join(directory, n))]
# ======================================================================================================================


def recursive_find_files(directory, pattern):
    """ Return list of full paths filename matching pattern in base folder and all subfolders. """
    list_files = [y for x in os.walk(directory) for y in glob.glob(os.path.join(x[0], pattern))]
    return list_files
# recursive_find_files
# ======================================================================================================================


# deprecated (listdir is slower than os.scandir)
def find_files_old(directory, pattern):
    """ Return list of files matching pattern in base folder. """
    return [n for n in fnmatch.filter(os.listdir(directory), pattern) if
            os.path.isfile(os.path.join(directory, n))]
# find_files
# ======================================================================================================================


def find_file(pattern, path):
    result = []
    for search_file in os.scandir(path):
        if fnmatch.fnmatch(search_file.name, pattern):
            result.append(os.path.join(path, search_file.name))
    if len(result) > 1:
        logging.warning("multiple matches for %s, %s" % (path, pattern))
    elif len(result) == 0:
        logging.warning("empty match for %s, %s" % (path, pattern))
        return None
    return result[0]
# find_file
# ======================================================================================================================


# deprecated (listdir -- in os.walk -- is slower than scandir)
def find_file_old(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    if len(result) > 1:
        logging.warning("multiple matches for %s, %s" % (path, pattern))
    elif len(result) == 0:
        logging.warning("empty match for %s, %s" % (path, pattern))
        return None
    return result[0]
# find_file
# ======================================================================================================================


def find_file_in_sys_path(pathname):
    for dir_name in sys.path:
        candidate = os.path.join(dir_name, pathname)
        if os.path.isfile(candidate):
            return candidate
    return None
# find_file_in_sys_path
# ======================================================================================================================


def find_dir_in_sys_path(pathname):
    for dir_name in sys.path:
        candidate = os.path.join(dir_name, pathname)
        if os.path.isfile(candidate):
            return candidate
    return None
# find_dir_in_sys_path
# ======================================================================================================================


def recursive_dictionary_update(dictionary, update):
    """
        The update method does not work recursively for dictionaries, hence this method
    :param dictionary: dictionary to udpate
    :param update: update
    :return: dictionary
    """
    if not isinstance(dictionary, collections.Mapping):
        return copy.deepcopy(update)
    for k, v in update.items():
        if isinstance(v, collections.Mapping):
            r = recursive_dictionary_update(dictionary.get(k, {}), v)
            dictionary[k] = r
        else:
            dictionary[k] = update[k]
    return dictionary
# recursive_dictionary_update


def recursive_get(dictionary, fields, default=None):
    """

    :param dictionary:
    :param fields:
    :param default: default for default value
    :return:

        e.g. dico.get("field1, {})).get(field2,{}).get("field3", default_value)
        recursive_get(dico, ["field1", "field2", "field3"], default=default_value)

    """
    dictionary = reduce(lambda c, k: c.get(k, {}), fields[:-1], dictionary)
    return dictionary.get(fields[-1], default)

# ======================================================================================================================


class MultiTimer(object):
    """ Timer permettant de mesurer le temps d'execution de lignes de codes par blocs et par etapes """

    # TODO : prevoir methodes pour calcul de temps d'execution moyens

    def __init__(self):
        self.start_time = 0
        self.stop_time = 0
        self.perf_time = 0
        self.tick_times = list()
        self.tickperf_times = list()
        self.tick_freq = cv2.getTickFrequency()

    def start(self):
        self.start_time = cv2.getTickCount()

    def stop(self):
        self.stop_time = cv2.getTickCount()

    def perf(self, start, stop):
        self.perf_time = (stop - start) / self.tick_freq

    def stopAndPerf(self, text, num_iter=1):
        self.stop()
        self.perf(self.start_time, self.stop_time)
        print('Time perf', text, ' : ', int(round(10000*self.perf_time))/(10*num_iter), ' ms')

    def tick(self, label):
        self.tick_times.append((cv2.getTickCount(), str(len(self.tick_times)) + ' - ' + label))

    def tickAndPerfs(self, label, num_iter=1):
        if len(self.tick_times) == 0:
            return
        self.tick(label)

        print('-------------------------------------------------------------------')
        for i in range(1, len(self.tick_times)):
            self.tickperf_times.append(int(10000*(self.tick_times[i][0] - self.tick_times[i-1][0])/self.tick_freq)/(10*num_iter))
            sys.stdout.write("{:>11}  {:<36}  {:<2}  {:>6}  {:<3}\n".format('Time perf', self.tick_times[i][1], ':', self.tickperf_times[i-1], 'ms'))
        print('-------------------------------------------------------------------')
# multiTimer
# ======================================================================================================================


def debugImshow(image, num=0, time=1, label='DebugImshow ', window=cv2.WINDOW_NORMAL):
    """ YLA : affichage rapide d'une image """
    cv2.namedWindow(label + str(num), window)
    cv2.imshow(label + str(num), image)
    cv2.waitKey(time)
# debugImshow
# ======================================================================================================================


def adjustGamma(image, gamma=1.0, v_only=False):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")

    if v_only:
        img_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        img_hsv[:,:,2] = cv2.LUT(img_hsv[:,:,2], table)  #, dst = img_hsv[:,:,2])
        img = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR)
    else:
        img = cv2.LUT(image, table)

    return img
# adjustGamma
# ======================================================================================================================


def get_full_path(relative_path):
    """
        return full path based on root path defined in KPTURE_ROOT environment variable
    :param relative_path: relative path w.r.t. $KPTURE_ROOT
    :return: full path
    """
    kpture_root = os.getenv("KPTURE_ROOT", None)
    if kpture_root is None:
        logging.warning("Environment variable KPTURE_ROOT is not defined")
        return relative_path
    # The relative_path strings should not start with a slash.
    # If it starts with a slash, then it is considered as an "absolute path"
    # and everything before them would be discarded.
    relative_path = relative_path.lstrip("/")
    relative_path = relative_path.lstrip("\\")
    return os.path.join(kpture_root, relative_path)
# get_full_path
# ======================================================================================================================


def popup_error_message(func):
    """
        decorator that encapsulates the function in a try/except and shows a popup message with error and traceback

        cf. http://gael-varoquaux.info/programming/decoration-in-python-done-right-decorating-and-pickling.html
        for the use of @wraps
    :param func: function to be decorated
    :return: decorated function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            logging.exception(e)
            import sys
            import traceback
            import processors.display.popup
            import winsound

            exc_type, exc_value, exc_traceback = sys.exc_info()

            "*** format_exception:"
            message = traceback.format_exception(exc_type, exc_value, exc_traceback)
            winsound.PlaySound('SystemExclamation', winsound.SND_ASYNC)
            processors.display.popup.application_popup("\n".join(message))
    return wrapper
# popup_error_message
# ======================================================================================================================


def log_error_message(func):
    """
        decorator that encapsulates the function in a try/except and logs the error and traceback

        cf. http://gael-varoquaux.info/programming/decoration-in-python-done-right-decorating-and-pickling.html
        for the use of @wraps
    :param func: function to be decorated
    :return: decorated function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
         #   func(*args, **kwargs)
            result = func(*args, **kwargs)
            return result
        except Exception as e:
            logging.exception(e)
            return []
            # import sys
            # import traceback
            # exc_type, exc_value, exc_traceback = sys.exc_info()
            # "*** format_exception:"
            # message = traceback.format_exception(exc_type, exc_value, exc_traceback)
    return wrapper
# log_error_message
# ======================================================================================================================


def round_up_to_odd(f):
    return int(np.ceil(f) // 2 * 2 + 1)
# round_up_to_odd
# ======================================================================================================================


def TickToMsConverter(tick_begin, tick_end, tick_freq=0, prec=1, nb_iter=1):
    if tick_freq == 0:
        tick_freq = cv2.getTickFrequency()
    result = int(1000 * int(10**prec) * (tick_end - tick_begin) / tick_freq) / int(10**prec * nb_iter)
    return result
# TickCalculator
# ======================================================================================================================

def kill_by_process_name(name):
    for proc in psutil.process_iter():
        if proc.name() == name:
            kill_by_process_name_shell(name)
            print("Killing process: " + name)
            if (check_process_exist_by_name(name)):
                print("Killing process: " + name + " sucess")
            else:
                print("Killing process: " + name + " failed")
            return

    print("Not found process: " + name)
# kill_by_process_name
# ======================================================================================================================


def check_process_exist_by_name(name):
    for proc in psutil.process_iter():
        if proc.name() == name:
            return True
    return False
# check_process_exist_by_name
# ======================================================================================================================


def kill_by_process_name_shell(name):
    os.system("taskkill /f /im " + name)
# kill_by_process_name_shell
# ======================================================================================================================


def auto_generated_header(generating_file=__file__, save_diff=False):
    """
        list information about generating file
        :return: list of strings
    """
    generating_file.replace("\\", "/")
    root_dir = common.veri_global.VERI_ROOT.replace("\\", "/")
    root_dir = common.string_operations.longest_common_substring(root_dir, generating_file)

    repo = git.Repo(search_parent_directories=True)
    lines = ["Auto-generated file with {}".format(generating_file.split(root_dir)[-1]),
             "@ commit #{}".format(repo.head.object.hexsha),
             repo.git.status()]
    if save_diff:
        lines.append(repo.git.diff())

    lines = map("{}".format, lines)
    lines = map(lambda x: x.split("\n"), lines)
    lines = reduce(operator.add, lines)
    return lines
# ======================================================================================================================


def normalize_path(path):
    """Convert a path to an absolute path in POSIX format

    :param path: path to convert
    :return: the normalized path
    :rtype: str
    """
    return str(pathlib.Path(path).absolute().as_posix())


def random_colors(n, bright=True):
    """
    Generate pseudo 'random' colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    :param n: number of different colors to generate
    :param bright: True if generate bright colors
    :return: list of pseudo random colors in rgb
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / float(n), 1, brightness) for i in range(n)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors
