import threading

"""
  READ/WRITE LOCK for reader-write problem
  cf https://en.wikipedia.org/wiki/Readers%E2%80%93writers_problem

  USING IT:
    to make a class thread safe, use the decorator RWLocked

    @RWLocked
    class MyAwesomeClass(object):
      def __init__(self,...):
        ...
      def my_method(self,...):
        ...

    to use it:
      x = MyAwesomeClass(...)

      normal usage (not thread safe: 
      x.my_method(...)

      thread safe writing usage:
      my_writer = x.safe_write()
      my_writer.my_method(...)

      optional: to force the lock to be released, 
      delete the writer explicitly
      my_writer.__del__()

      thread safe reading usage:
      my_reader = x.safe_read()
      my_reader.my_method(...)

      optional: to force the lock to be released, 
      delete the writer explicitly
      my_writer.__del__()

      locks are release when readers and/or writers are delete
      (either explicitly using del myreader or implicitly
      at the end of a block)


    With the "with" statement:

    my_lock = RWLock()
    with my_lock.reading_lock:
        print("in reading lock")

    with my_lock.writing_lock:
        print("in writing lock")
"""
# ======================================================================================================================
"""
PSEUDO CODE from Wikipedia that explains the used implementation

resourceAccess  = threading.Semaphore() # controls access (read/write) to the resource
readCountAccess = threading.Semaphore() # for syncing changes to shared variable readCount
serviceQueue    = threading.Semaphore() # FAIRNESS: preserves ordering of requests (signaling must be FIFO)
readCount = 0

def writer():
  serviceQueue.acquire()           # wait in line to be serviced
  # <ENTER>
  resourceAccess.acquire()         # request exclusive access to resource
  # </ENTER>
  serviceQueue.release()           # let next in line be serviced

  # <WRITE>
  writeResource()                  # writing is performed
  # </WRITE>

  # <EXIT>
  resourceAccess.release()         # release resource access for next reader/writer
  # </EXIT>


def reader():
  serviceQueue.acquire()           # wait in line to be serviced
  readCountAccess.acquire()        # request exclusive access to readCount
  # <ENTER>
  if readCount == 0:               # if there are no readers already reading:
    resourceAccess.acquire()       # request resource access for readers (writers blocked)
  readCount+=1                     # update count of active readers
  # </ENTER>

  serviceQueue.release()           # let next in line be serviced
  readCountAccess.release()        # release access to readCount

  # <READ>
  readResource()                   # reading is performed
  # </READ>

  readCountAccess.acquire()        # request exclusive access to readCount
  # <EXIT>
  readCount-=1                     # update count of active readers
  if readCount == 0:               # if there are no readers left:
    resourceAccess.release()       # release resource access for all
  # </EXIT>
  readCountAccess.release()        # release access to readCount
"""


# ======================================================================================================================


class RWLock:
    def __init__(self):
        self.resourceAccess = threading.Lock()  # controls access (read/write) to the resource
        self.readCountAccess = threading.Lock()  # for syncing changes to shared variable readCount
        self.serviceQueue = threading.Lock()  # FAIRNESS: preserves ordering of requests (signaling must be FIFO)
        self.onServiceQueue = threading.Lock()
        self.readCount = 0
        self.readersCnt = dict()
        self.writersCnt = dict()  # to avoid deadlock with 2 writers being created by the same thread (not the best way)

        self.reading_lock = RWReader(self)
        self.writing_lock = RWWriter(self)

    # __init__

    def add_reader(self, reader_id):
        self.onServiceQueue.acquire()
        if reader_id not in self.readersCnt:
            self.readersCnt[reader_id] = 0
        res = self.readersCnt[reader_id] == 0
        self.readersCnt[reader_id] += 1
        self.onServiceQueue.release()
        return res

    # add_reader

    def remove_reader(self, reader_id):
        self.onServiceQueue.acquire()
        if reader_id not in self.readersCnt:
            self.readersCnt[reader_id] = 0
        self.readersCnt[reader_id] -= 1
        res = self.readersCnt[reader_id] == 0
        self.onServiceQueue.release()
        return res

    # remove_reader

    def add_writer(self, writer_id):
        self.onServiceQueue.acquire()
        if writer_id not in self.writersCnt:
            self.writersCnt[writer_id] = 0
        res = self.writersCnt[writer_id] == 0
        self.writersCnt[writer_id] += 1
        self.onServiceQueue.release()
        return res

    # add_writer

    def remove_writer(self, writer_id):
        self.onServiceQueue.acquire()
        if writer_id not in self.writersCnt:
            self.writersCnt[writer_id] = 0
        self.writersCnt[writer_id] -= 1
        res = self.writersCnt[writer_id] == 0
        self.onServiceQueue.release()
        return res
        # remove_writer


# RWLock
# ======================================================================================================================


class RWWriter(object):
    def __init__(self, lock):
        self._lock = lock

    # __init__

    def __enter__(self):
        if self._lock.add_writer(threading.get_ident()):
            self._lock.serviceQueue.acquire()  # wait in line to be serviced
            # <ENTER>
            self._lock.resourceAccess.acquire()  # request exclusive access to resource
            # </ENTER>
            # print("\n acquire writer\n")
            self._lock.serviceQueue.release()  # let next in line be serviced

    # __enter__

    def __exit__(self, *args):
        if self._lock.remove_writer(threading.get_ident()):
            # print("\n release writer\n")
            # <EXIT>
            self._lock.resourceAccess.release()  # release resource access for next reader/writer
            # </EXIT>
            # __exit__


# ======================================================================================================================


class RWReader(object):
    def __init__(self, lock):
        self._lock = lock

    # __init__

    def __enter__(self):
        if self._lock.add_reader(threading.get_ident()):
            self._lock.serviceQueue.acquire()  # wait in line to be serviced
            self._lock.readCountAccess.acquire()  # request exclusive access to readCount
            # <ENTER>
            if self._lock.readCount == 0:  # if there are no readers already reading:
                self._lock.resourceAccess.acquire()  # request resource access for readers (writers blocked)
            self._lock.readCount += 1  # update count of active readers
            # </ENTER>
            # print("\n acquire reader %d\n" % self._lock.readCount)
            self._lock.readCountAccess.release()  # release access to readCount
            self._lock.serviceQueue.release()  # let next in line be serviced

    # __init__

    def __exit__(self, *args):
        if self._lock.remove_reader(threading.get_ident()):

            self._lock.readCountAccess.acquire()  # request exclusive access to readCount
            # print("\n release reader %d" % self._lock.readCount)
            # <EXIT>
            self._lock.readCount -= 1  # update count of active readers
            if self._lock.readCount == 0:  # if there are no readers left:
                self._lock.resourceAccess.release()  # release resource access for all
            # </EXIT>
            self._lock.readCountAccess.release()  # release access to readCount
            # __exit__


# ======================================================================================================================


def build_writer(decorated_class):
    # inheritance is only for the hasattr(className,methodName)
    # to work (to use "in", "[key]", ...)
    # otherwise this works by composition (important to avoid copy)
    # as a results the decorated_class.__init__ is not explicitly called
    # this might cause some problems
    class Writer(decorated_class):
        def __init__(self, lock, object_instance):
            self._object_instance = object_instance
            self._lock = lock
            if self._lock.add_writer(threading.get_ident()):
                self._lock.serviceQueue.acquire()  # wait in line to be serviced
                # <ENTER>
                self._lock.resourceAccess.acquire()  # request exclusive access to resource
                # </ENTER>
                # print("\n acquire writer\n")
                self._lock.serviceQueue.release()  # let next in line be serviced

        # __init__

        def __del__(self):
            if self._lock.remove_writer(threading.get_ident()):
                # print("\n release writer\n")
                # <EXIT>
                self._lock.resourceAccess.release()  # release resource access for next reader/writer
                # </EXIT>

        # __del__

        def __getattribute__(self, *args):
            # try/except: may be not the most efficient way to proceed
            try:
                return object.__getattribute__(self, *args)
            except AttributeError:
                return decorated_class.__getattribute__(self._object_instance, *args)

        # __getattribute__

        def __setattr__(self, *args):
            # try/except: may be not the most efficient way to proceed
            try:
                return object.__setattr__(self, *args)
            except AttributeError:
                return decorated_class.__setattribute__(self._object_instance, *args)
                # __setattr__

    # Writer
    return Writer


# build_writer


def build_reader(decorated_class):
    class Reader(decorated_class):
        def __init__(self, lock, object_instance):
            self._object_instance = object_instance
            self._lock = lock
            if self._lock.add_reader(threading.get_ident()):
                self._lock.serviceQueue.acquire()  # wait in line to be serviced
                self._lock.readCountAccess.acquire()  # request exclusive access to readCount
                # <ENTER>
                if self._lock.readCount == 0:  # if there are no readers already reading:
                    self._lock.resourceAccess.acquire()  # request resource access for readers (writers blocked)
                self._lock.readCount += 1  # update count of active readers
                # </ENTER>
                # print("\n acquire reader %d\n"%self.__lock__.readCount)
                self._lock.readCountAccess.release()  # release access to readCount
                self._lock.serviceQueue.release()  # let next in line be serviced

        # __init__

        def __del__(self):
            if self._lock.remove_reader(threading.get_ident()):

                self._lock.readCountAccess.acquire()  # request exclusive access to readCount
                # print("\n release reader %d"%(self.__lock__.readCount))
                # <EXIT>
                self._lock.readCount -= 1  # update count of active readers
                if self._lock.readCount == 0:  # if there are no readers left:
                    self._lock.resourceAccess.release()  # release resource access for all
                # </EXIT>
                self._lock.readCountAccess.release()  # release access to readCount

        # __del__

        def __getattr__(self, name):
            # try/except: may be not the most efficient way to proceed
            try:
                return object.__getattribute__(self, name)
            except AttributeError:
                return decorated_class.__getattribute__(self._object_instance, name)
                # __getattribute__

    # Reader

    return Reader


# build_reader
# ======================================================================================================================


class RWLocked:
    def __init__(self, decorated):
        self._decorated = decorated
        self.Reader = build_reader(decorated)
        self.Writer = build_writer(decorated)

    # __init__

    def __call__(self):
        object_instance = self._decorated()
        lock = RWLock()

        def safe_reader():
            return self.Reader(lock, object_instance)

        def safe_writer():
            return self.Writer(lock, object_instance)

        # object_instance.safe_reader = safe_reader
        # object_instance.safe_writer = safe_writer
        setattr(object_instance, 'safe_reader', safe_reader)
        setattr(object_instance, 'safe_writer', safe_writer)
        return object_instance
        # __call__


# RWLocked
# ======================================================================================================================


class UserDict(object):
    """
        UserDictionary that inherits from objects (from Python2 only otherwise use collections.UserDict)
    """
    def __init__(*args, **kwargs):
        if not args:
            raise TypeError("descriptor '__init__' of 'UserDict' object "
                            "needs an argument")
        self = args[0]
        args = args[1:]
        if len(args) > 1:
            raise TypeError('expected at most 1 arguments, got %d' % len(args))
        if args:
            dict = args[0]
        elif 'dict' in kwargs:
            dict = kwargs.pop('dict')
            import warnings
            warnings.warn("Passing 'dict' as keyword argument is "
                          "deprecated", PendingDeprecationWarning,
                          stacklevel=2)
        else:
            dict = None
        self.data = {}
        if dict is not None:
            self.update(dict)
        if len(kwargs):
            self.update(kwargs)
    def __repr__(self):
        return repr(self.data)

    def __cmp__(self, dict):
        if isinstance(dict, UserDict):
            return cmp(self.data, dict.data)
        else:
            return cmp(self.data, dict)
    __hash__ = None # Avoid Py3k warning

    def __len__(self): return len(self.data)

    def __getitem__(self, key):
        if key in self.data:
            return self.data[key]
        if hasattr(self.__class__, "__missing__"):
            return self.__class__.__missing__(self, key)
        raise KeyError(key)

    def __setitem__(self, key, item): self.data[key] = item

    def __delitem__(self, key): del self.data[key]

    def clear(self):
        self.data.clear()

    def copy(self):
        if self.__class__ is UserDict:
            return UserDict(self.data.copy())
        import copy
        data = self.data
        try:
            self.data = {}
            c = copy.copy(self)
        finally:
            self.data = data
        c.update(self)
        return c

    def keys(self): return self.data.keys()

    def items(self): return self.data.items()

    def iteritems(self): return self.data.iteritems()

    def iterkeys(self): return self.data.iterkeys()

    def itervalues(self): return self.data.itervalues()

    def values(self): return self.data.values()

    def has_key(self, key): return key in self.data

    def update(*args, **kwargs):
        if not args:
            raise TypeError("descriptor 'update' of 'UserDict' object "
                            "needs an argument")
        self = args[0]
        args = args[1:]
        if len(args) > 1:
            raise TypeError('expected at most 1 arguments, got %d' % len(args))
        if args:
            dict = args[0]
        elif 'dict' in kwargs:
            dict = kwargs.pop('dict')
            import warnings
            warnings.warn("Passing 'dict' as keyword argument is deprecated",
                          PendingDeprecationWarning, stacklevel=2)
        else:
            dict = None
        if dict is None:
            pass
        elif isinstance(dict, UserDict):
            self.data.update(dict.data)
        elif isinstance(dict, type({})) or not hasattr(dict, 'items'):
            self.data.update(dict)
        else:
            for k, v in dict.items():
                self[k] = v
        if len(kwargs):
            self.data.update(kwargs)

    def get(self, key, failobj=None):
        if key not in self:
            return failobj
        return self[key]

    def setdefault(self, key, failobj=None):
        if key not in self:
            self[key] = failobj
        return self[key]

    def pop(self, key, *args):
        return self.data.pop(key, *args)

    def popitem(self):
        return self.data.popitem()

    def __contains__(self, key):
        return key in self.data

    @classmethod
    def fromkeys(cls, iterable, value=None):
        d = cls()
        for key in iterable:
            d[key] = value
        return d


if __name__ == "__main__":
    import sys
    if sys.version_info[0] >= 3:
        import collections

        x = RWLocked(collections.UserDict)()
    else:

        x = RWLocked(UserDict)()


    x.b = 2
    reader = x.safe_reader()
    reader[1] = 1
    reader.a = 1
    print(reader.b)
    del reader
    print(x[1])

    y = object

    my_lock = RWLock()
    with my_lock.reading_lock:
        print("in reading lock")
        with my_lock.reading_lock:
            print("in reading lock")
    with my_lock.writing_lock:
        print("in writing lock")
