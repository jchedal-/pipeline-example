# STL
import logging

# EXTERNAL
import scipy.optimize
import scipy.special
import scipy.stats

import numpy as np

# Project
from common import search_sorted


class Quantile(object):
    EXACT = "Binomial"
    CHERNOFF = "Chernoff"
    _METHOD_SET = {EXACT, CHERNOFF}

    def __init__(self, alpha=0.05):
        self.log_alpha = np.log(2./alpha)
        self.alpha = alpha

    # Public Method(s)

    def confidence_interval(self, x, p, method=EXACT):
        """
           Compute 1-\alpha confidence interval of p^th quantile
           there is not interpolation (unlike for instance np.quantile which uses linear interpolation)

        :param x: vector of values (list or numpy.array)
        :param p: quantile in (0,1)
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :return:  tuple: (lower value, mid value, upper value)
        """

        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if not (0. < p < 1.):
            raise ValueError("quantile p ({}) should be in (0,1)".format(p))

        x = np.array(x)
        # </CHECK & FORMAT ARGUMENTS>

        # <COMPUTE CI>
        result = None
        if method == self.EXACT:
            result = self._confidence_interval_exact(x, p)
        elif method == self.CHERNOFF:
            result = self._confidence_interval_chernoff(x, p)
        # </COMPUTE CI>
        return result

    def confidence_interval_quantile(self, k, n, method=EXACT):
        """
            Compute low_p, upp_p such as the k^th (smallest) element of x
            is in [Q(low_p), Q(upp_p)] with probability at least 1-alpha.
            where Q(p) is the p^th quantile of x

        :param k: index in [0, n-1]
        :param n: number of samples
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :return: low_p, upp_p so that x.partition(np.round(p*n)) in [Q(low_p), Q(upp_p)] with proba 1-alpha
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if not (0 <= k < n):
            raise ValueError("index k ({}) should be in [0,n)".format(k))
        # </CHECK & FORMAT ARGUMENTS>

        # <COMPUTE CI>
        result = None
        if method == self.EXACT:
            result = self._confidence_interval_quantile_exact(k, n)
        elif method == self.CHERNOFF:
            result = self._confidence_interval_quantile_chernoff(k, n)
        # </COMPUTE CI>
        return result

    def minimum_n_for_quantile_ci(self, p, err_p, method=EXACT, dichotomy=True, **kwargs):
        """
            minimum number n0 of samples so that k, low_p given by
                low_p, upp_p= confidence_interval_quantile(p, n0, method)
            verify:
                upp_p - low_p < err_p

        :param p: quantile in (0,1)
        :param err_p: maximum size of the "quantile confidence interval
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :param dichotomy: if true use a "logsearch + dichotomy" otherwise a brute force approach is used.
        :return: minimum number of samples
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if err_p <= 0:
            return np.inf

        max_iter_brute_force = kwargs.get("max_iter_brute_force", 1e6)
        # </CHECK & FORMAT ARGUMENTS>

        # <DIRECT METHOD>
        if method == self.EXACT:
            direct_method = self._confidence_interval_quantile_exact
        elif method == self.CHERNOFF:
            direct_method = self._confidence_interval_quantile_chernoff
        else:
            # should never happen
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))
        # </DIRECT METHOD>

        def _test(_n):
            if _n > 0:
                k = _n*p
                k_1 = np.ceil(k)
                k_2 = np.floor(k)
                low_p1, upp_p1 = direct_method(k_1, _n)
                low_p2, upp_p2 = direct_method(k_2, _n)
                low_p = np.minimum(low_p1, low_p2)
                upp_p = np.minimum(upp_p1, upp_p2)
                return (upp_p - low_p) > err_p
            return True

        if dichotomy:
            n = search_sorted.Dichotomy(_test)(lower_value=False)
        else:
            n = search_sorted.brute_force(_test, n0=1, max_n=max_iter_brute_force, lower_value=False)
        return n

    def minimum_n_for_quantile_lower_ci(self, p, err_p, method=EXACT, dichotomy=True, **kwargs):
        """
            minimum number n0 of samples so that low_p, upp_p given by
                k, low_p = quantile_lower_bound(p, n0, method)
            verify:
                p - low_p < err_p
                Q(low_p) <= x_k <= Q(p) with probability at least 1-\alpha

        :param p: quantile in (0,1)
        :param err_p: maximum size of the "quantile confidence interval
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :param dichotomy: if true use a "logsearch + dichotomy" otherwise a brute force approach is used.
        :return: tuple (n0, k) where n0 is minimum number of samples and k the element to select
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if err_p <= 0:
            return np.inf

        max_iter_brute_force = kwargs.get("max_iter_brute_force", 1e6)
        # </CHECK & FORMAT ARGUMENTS>

        # <DIRECT METHOD>
        if method == self.EXACT:
            direct_method = self._quantile_lower_bound_exact
        elif method == self.CHERNOFF:
            logging.warning("method '{}'  is not implemented yet for quantile_lower_bound. Use Exact instead".format(
                self.CHERNOFF))
            direct_method = self._quantile_lower_bound_exact
        else:
            # should never happen
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))
        # </DIRECT METHOD>

        def _test(_n):
            if _n > 0:
                _, low_p = direct_method(p, _n)
                return (p - low_p) > err_p
            return True

        if dichotomy:
            n = search_sorted.Dichotomy(_test)(lower_value=False)
        else:
            n = search_sorted.brute_force(_test, n0=1, max_n=max_iter_brute_force, lower_value=False)

        if n:
            k, _ = direct_method(p, n)
        else:
            k = None
        return n, k

    def minimum_n_for_quantile_upper_ci(self, p, err_p, method=EXACT, dichotomy=True, **kwargs):
        """
            minimum number n0 of samples so that low_p, upp_p given by
                k, upp_p = quantile_upper_bound(p, n0, method)
            verify:
                upp_p - p < err_p
                Q(p) <= x_k <= Q(upp_p) with probability at least 1-\alpha

        :param p: quantile in (0,1)
        :param err_p: maximum size of the "quantile confidence interval
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :param dichotomy: if true use a "logsearch + dichotomy" otherwise a brute force approach is used.
        :return: tuple (n0, k) where n0 is minimum number of samples and k the element to select
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if err_p <= 0:
            return np.inf

        max_iter_brute_force = kwargs.get("max_iter_brute_force", 1e6)
        # </CHECK & FORMAT ARGUMENTS>

        # <DIRECT METHOD>
        if method == self.EXACT:
            direct_method = self._quantile_upper_bound_exact
        elif method == self.CHERNOFF:
            direct_method = self._quantile_upper_bound_chernoff
        else:
            # should never happen
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        # </DIRECT METHOD>

        def _test(_n):
            if _n > 0:
                _, upp_p = direct_method(p, _n)
                return (upp_p - p) > err_p
            return True

        if dichotomy:
            n = search_sorted.Dichotomy(_test)(lower_value=False)
        else:
            n = search_sorted.brute_force(_test, n0=1, max_n=max_iter_brute_force, lower_value=False)

        k, _ = direct_method(p, n)
        return n, k

    def quantile_lower_bound(self, p, n, method=EXACT):
        """
           Given a quantile p and a number of sample n,
           it computes k and low_p such that the k^th (smallest) sample x_k verifies with probability
           at least 1-\alpha
           Q(low_p) <= x_k <= Q(p)

        :param p: quantile in (0,1)
        :param n: number of samples
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :return tuple (k, low_p): k is the rank of the element to select to get x_k \in [p, upp_p]
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if not (0. < p < 1.):
            raise ValueError("quantile p ({}) should be in (0,1)".format(p))
        # </CHECK & FORMAT ARGUMENTS>

        result = None
        if method == self.EXACT:
            result = self._quantile_lower_bound_exact(p, n)
        elif method == self.CHERNOFF:
            logging.warning("method '{}'  is not implemented yet for quantile_lower_bound. Use Exact instead".format(
                self.CHERNOFF))
            result = self._quantile_lower_bound_exact(p, n)

        return result

    def quantile_upper_bound(self, p, n, method=EXACT):
        """
           Given a quantile p and a number of sample n,
           it computes k and upp_p such that the k^th (smallest) sample x_k verifies with probability
           at least 1-\alpha
           Q(p) <= x_k <= Q(upp_p)

        :param p: quantile in (0,1)
        :param n: number of samples
        :param method: Quantile.EXACT (default) -> use Binomial distribution
                       Quantile.CHERNOFF -> use CHERNOFF BOUNDS
        :return tuple (k, upp_p): k is the rank of the element to select to get x_k \in [p, upp_p]
        """
        # <CHECK & FORMAT ARGUMENTS>
        if method not in self._METHOD_SET:
            raise ValueError("method ({}) should be in {}".format(method, self._METHOD_SET))

        if not (0. < p < 1.):
            raise ValueError("quantile p ({}) should be in (0,1)".format(p))
        # </CHECK & FORMAT ARGUMENTS>

        result = None
        if method == self.EXACT:
            result = self._quantile_upper_bound_exact(p, n)
        elif method == self.CHERNOFF:
            result = self._quantile_upper_bound_chernoff(p, n)
        return result

    # Private Method(s)

    def _confidence_interval_chernoff(self, x, p):
        """
            Use Chernoff bound to compute the 1-\alpha confidence interval of p^th quantile
            there is not interpolation (np.quantile use linear interpolation)
        :param x: vector of values
        :param p: quantile in (0,1)
        :return:  (lower value, mid value, upper value)
        """
        n = len(x)

        d1 = self._delta_1(p, n)
        d2 = self._delta_2(p, n)
        n1 = int(np.floor((1-d1)*p*n))
        n0 = int(np.round(p*n))
        n2 = int(np.ceil((1+d2)*p*n))
        ns = []

        low = np.NAN
        upp = np.NAN

        if n1 >= 0:
            ns.append(n1)
        ns.append(n0)
        if n2 < n:
            ns.append(n2)

        x.partition(np.array(ns))
        if n1 >= 0:
            low = x[n1]
        mid = x[n0]
        if n2 < n:
            upp = x[n2]
        return low, mid, upp

    def _confidence_interval_exact(self, x, p):
        """
           Use the binomial distribution to compute the  1-\alpha confidence interval of p^th quantile
           there is not interpolation (np.quantile use linear interpolation)
           since I do not know exactly the ppf(q, n, p, loc=0) function, there is a "safety margin" for n1 and n2

        :param x: vector of values
        :param p: quantile in (0,1)
        :return:  (lower value, mid value, upper value)
        """
        n = len(x)
        n1 = int(scipy.stats.binom.ppf(self.alpha/2., int(n), float(p)))
        n1 -= 1
        n0 = int(np.round(p*n))
        n2 = int(scipy.stats.binom.ppf(1-self.alpha/2., int(n), float(p)))

        low = np.NAN
        upp = np.NAN

        ns = []
        if n1 > 0:
            ns.append(n1)
        ns.append(n0)
        if n2 < n-1:
            ns.append(n2)

        x.partition(np.array(ns))
        if n1 > 0:
            low = x[n1]
        mid = x[n0]
        if n2 < n-1:
            upp = x[n2]
        return low, mid, upp

    def _confidence_interval_quantile_chernoff(self, k, n):
        """
            Use Chernoff bounds to compute low_p, upp_p such as the np.round(p*n)th (smallest) element of x
            is in [Q(low_p), Q(upp_p)] with probability at least 1-alpha.
            where Q(p) is the p^th quantile of x

        :param p: quantile in (0,1)
        :param n: number of samples
        :param p: quantile in (0,1)
        :param n: number of samples
        :return: low_p, upp_p so that x.partition(np.round(p*n)) in [Q(low_p), Q(upp_p)] with proba 1-alpha
        """
        k0 = k + 1
        p = k/float(n)
        d1 = self._delta_1(p, n)
        d2 = self._delta_2(p, n)

        low_p = (p-1./n)/(1.+d2)
        if d1 < 1:
            upp_p = (p+1./n)/(1.-d1)
        else:
            upp_p = 1
        low_p = np.maximum(0, np.minimum(low_p, 1))
        upp_p = np.maximum(low_p, np.minimum(upp_p, 1))
        return low_p, upp_p

    def _confidence_interval_quantile_exact(self, k, n):
        """
            Use the binomial distribution to compute low_p, upp_p such as the np.round(p*n)th (smallest) element of x
            is in [Q(low_p), Q(upp_p)] with probability at least 1-alpha.
            where Q(p) is the p^th quantile of x
            since I do not know exactly the ppf(q, n, p, loc=0) function, there is a "safety margin" for n1 and n2

        :param k: index in [0,n)
        :param n: number of samples
        :param p: quantile in (0,1)
        :param n: number of samples
        :return: low_p, upp_p so that x.partition(np.round(p*n)) in [Q(low_p), Q(upp_p)] with proba 1-alpha
        """
        if n <= 2:
            return 0, 1

        k0 = k + 1
        k = k0
        if k <= 0:
            low_p = 0
        else:
            a = n - k
            b = k + 1
            low_p = scipy.special.betaincinv(a, b, 1 - self.alpha / 2.)
            low_p = 1. - low_p

        k = k0+1
        if k == n:
            upp_p = 1
        else:
            a = n - k
            b = k + 1
            upp_p = scipy.special.betaincinv(a, b,  self.alpha / 2.)
            upp_p = 1. - upp_p

        # low_p = (scipy.stats.binom.ppf(self.alpha/2., int(n), float(p)))/float(n)
        # upp_p = (scipy.stats.binom.ppf(1-self.alpha/2., int(n), float(p)))/float(n)
        if low_p >= upp_p:
            low_p = 0
            upp_p = 1
        return low_p, upp_p

    def _delta_1(self, p, n):
        return np.sqrt(2.*self.log_alpha/(p*n))

    def _delta_2(self, p, n):
        c = self.log_alpha / (p * n)
        return c/3 + np.sqrt(c*c/9 + 2*c)

    def _quantile_lower_bound_exact(self, p, n):
        """ cf. quantile_lower_bound """

        k0 = scipy.stats.binom.ppf(self.alpha/2., int(n), float(p))
        k = k0-1
        if k <= 0:
            return 0, 0
        a = n - k
        b = k + 1
        low_p = scipy.special.betaincinv(a, b, 1-self.alpha/2.)
        low_p = 1. - low_p
        index = k-1  # warning index start from 0
        return index, low_p

    def _quantile_upper_bound_chernoff(self, p, n):
        """ cf. quantile_upper_bound """
        d = self._delta_2(p, n)
        index = int(np.ceil((1+d)*p*n))
        e = (1.+d)*p + 1./n
        c = self.log_alpha/n
        upp_p = c + e + np.sqrt(c*(2*e+c))
        return index, upp_p

    def _quantile_upper_bound_exact(self, p, n):
        """ cf. quantile_upper_bound """
        k0 = scipy.stats.binom.ppf(1-self.alpha/2., int(n), float(p))
        k = k0+1
        if k >= n-1:
            return n-1, 1

        a = n - k
        b = k + 1
        upp_p = scipy.special.betaincinv(a, b, self.alpha/2.)
        upp_p = 1. - upp_p
        index = k0-1   # warning index start from 0
        return index, upp_p

    @staticmethod
    def minimum_n_for_lower_bound_best_alpha(p, err_p, nb_alpha_points=500, method=EXACT, **kwargs):
        return Quantile._minimum_n_for_bound_best_alpha(p, err_p,
                                                        nb_alpha_points=nb_alpha_points,
                                                        method=method,
                                                        fun=Quantile.minimum_n_for_quantile_lower_ci,
                                                        upp=False,
                                                        **kwargs)

    @staticmethod
    def minimum_n_for_upper_bound_best_alpha(p, err_p, nb_alpha_points=500, method=EXACT, **kwargs):

        return Quantile._minimum_n_for_bound_best_alpha(p, err_p,
                                                        nb_alpha_points=nb_alpha_points,
                                                        method=method,
                                                        fun=Quantile.minimum_n_for_quantile_upper_ci,
                                                        upp=True,
                                                        **kwargs)

    @staticmethod
    def _minimum_n_for_bound_best_alpha(p, err_p, nb_alpha_points, method, fun, upp, **kwargs):
        margin = kwargs.get("margin", .00001)
        heuristic = kwargs.get("heuristic", False)
        heuristic_iter = kwargs.get("heuristic_iter", 6)
        heuristic_ratio = kwargs.get("heuristic_ratio", .25)
        heuristic_nb_points = kwargs.get("heuristic_nb_points", 10)
        heuristic_alpha_margin = kwargs.get("heuristic_alpha_margin", 3)

        _s = 1 if upp else -1

        k_opt = None
        alpha_opt = None

        n_min = np.inf
        alpha_min = margin
        alpha_max = err_p/2. - margin

        if heuristic:
            new_alphas = np.linspace(alpha_min, alpha_max, heuristic_nb_points)
            alphas = np.zeros((0,))
            ns = np.zeros((0,))
            ks = np.zeros((0,))
            for _ in range(heuristic_iter):
                nb_points = len(new_alphas)
                new_ns = np.full((nb_points,), fill_value=np.inf)
                new_ks = np.full((nb_points,), fill_value=np.inf)
                for ind, alpha in enumerate(new_alphas):
                    n, k = fun(Quantile(alpha=alpha), p + _s*alpha, err_p - 2. * alpha, method=method, **kwargs)
                    if n and n < n_min:
                        n_min = n
                        k_opt = k
                        alpha_opt = alpha
                    new_ns[ind] = n
                    new_ks[ind] = k

                alphas = np.concatenate((alphas, new_alphas))
                ns = np.concatenate((ns, new_ns))
                ks = np.concatenate((ks, new_ks))

                indices = np.argsort(alphas)
                alphas = alphas[indices]
                ns = ns[indices]
                ks = ks[indices]
                split_index = int(heuristic_ratio * len(ns))
                indices = np.argpartition(ns, split_index)[:split_index]
                ind_min = np.maximum(0, np.min(indices)-heuristic_alpha_margin)
                ind_max = np.minimum(len(alphas)-1, np.max(indices)+heuristic_alpha_margin)

                new_alphas = .5*(alphas[ind_min+1:ind_max] + alphas[ind_min:ind_max-1])
        else:
            alphas = np.linspace(alpha_min, alpha_max, nb_alpha_points)
            ns = np.full((nb_alpha_points,), fill_value=np.inf)
            ks = np.full((nb_alpha_points,), fill_value=np.inf)
            for ind, alpha in enumerate(alphas):
                n, k = fun(Quantile(alpha=alpha), p + _s * alpha, err_p - 2. * alpha, method=method, **kwargs)
                if n is None:
                    n = np.inf
                if n < n_min:
                    n_min = n
                    k_opt = k
                    alpha_opt = alpha
                ns[ind] = n
                ks[ind] = k

        class OptimizationResult(object):
            def __init__(self):
                self.nb_samples = None
                self.quantile_index = None
                self.optimal_alpha = None
                self.list_alpha = None
                self.list_nb_samples = None
                self.list_k = None

        result = OptimizationResult()
        result.nb_samples = n_min
        result.quantile_index = int(k_opt)
        result.optimal_alpha = alpha_opt
        result.list_alpha = alphas
        result.list_nb_samples = ns
        result.list_k = ks
        return result


# if __name__ == "__main__":
#     import cv2
#
#     def _run(text, fun, *args, **kwargs):
#         start = cv2.getTickCount()
#         _x = fun(*args, **kwargs)
#         stop = cv2.getTickCount()
#         elapsed = (stop - start)/cv2.getTickFrequency()
#         text = text.ljust(30, " ")
#         print("{}res: {}, duration: {} ms".format(text, _x, elapsed*1000))
#
#     quantile = Quantile()
#
#     _run("minimum_n exact", quantile.minimum_n, .5, .05, True)
#     _run("minimum_n bound", quantile.minimum_n, .5, .05, False)
#     _run("upper bound n exact", Quantile(alpha=.025).minimum_n_for_upper_bound, .975, .0125, True)
#     n = Quantile(alpha=.025).minimum_n_for_upper_bound(.975, .0125, True)
#     k, _ = Quantile(alpha=.025).quantile_upper_bound_exact(.975, n)
#     _run("direct", Quantile(alpha=.025).quantile_ci_exact, *(float(k)/float(n), n))
#
#     #
#     delta = .05
#     epsilon = .0125
#     epsilon = .025
#     nb_points = 500
#     alphas = np.linspace(.00001, epsilon/2., nb_points)
#     ns = np.zeros((len(alphas),))
#     n_min = np.inf
#     alpha_min = 0
#     for ind, alpha in enumerate(alphas):
#         n = Quantile(alpha=alpha).minimum_n_for_upper_bound(1-(delta + alpha), epsilon - alpha, True)
#         if n < n_min:
#             n_min = n
#             alpha_min = alpha
#         ns[ind] = n
#
#     # _run("upper bound n exact alpha: {} ".format(alpha),
#     #          Quantile(alpha=alpha).minimum_n_for_upper_bound, delta+alpha, epsilon-alpha, True)
#
#     import matplotlib.pyplot as plt
#     plt.plot(alphas*100, ns, "-")
#     plt.grid(True)
#     plt.show()
#
#
#     _run("upper bound n", Quantile(alpha=.025).minimum_n_for_upper_bound, .975, .0125, False)
#     _run("minimum_n bound (BF)", quantile.minimum_n_brute_force, *(.95, .025))
#     _run("upper bound n (BF)", Quantile(alpha=.025).minimum_n_for_upper_bound_brute_force, *(.975, .0125))
#
#
#     exit(0)
#     import scipy.stats
#     import matplotlib.pyplot as plt
#
#     batch_size = 1
#     nb_loop = 1000
#     q = .025
#     min_samples = 1
#     max_samples = 4.5
#     steps = n = np.logspace(min_samples, max_samples, num=100)
#     truth = scipy.stats.norm.ppf(q)
#     quantile = Quantile()
#     v_list_from_list_low = []
#     v_list_from_list = []
#     v_list_from_list_upp = []
#
#     v_list_from_list_low_exact = []
#     v_list_from_list_exact = []
#     v_list_from_list_upp_exact = []
#
#     for _ in range(nb_loop):
#         v_from_list = []
#         v_from_list_upp = []
#         v_from_list_low = []
#         v_from_list_exact = []
#         v_from_list_upp_exact = []
#         v_from_list_low_exact = []
#         x = []
#         # optimizer_smoothed.reset()
#
#         old_step = 0
#         for step in steps:
#             while old_step < step:
#                 sample = np.random.randn(batch_size).tolist()
#                 x += sample[:]
#                 old_step += 1
#
#             # v_smooth.append(optimizer_smoothed.x)
#             low, mid, upp = quantile.confidence_interval(x, q)
#             v_from_list_low.append(low)
#             v_from_list.append(mid)
#             v_from_list_upp.append(upp)
#             low, mid, upp = quantile.confidence_interval_exact(x, q)
#             v_from_list_low_exact.append(low)
#             v_from_list_exact.append(mid)
#             v_from_list_upp_exact.append(upp)
#             # print(optimizer.x, optimizer.cesaro_x)
#
#         v_list_from_list_low.append(v_from_list_low[:])
#         v_list_from_list.append(v_from_list[:])
#         v_list_from_list_upp.append(v_from_list_upp[:])
#         v_list_from_list_low_exact.append(v_from_list_low_exact[:])
#         v_list_from_list_exact.append(v_from_list_exact[:])
#         v_list_from_list_upp_exact.append(v_from_list_upp_exact[:])
#
#     v_list_from_list_low = np.array(v_list_from_list_low)
#     v_list_from_list = np.array(v_list_from_list)
#     v_list_from_list_upp = np.array(v_list_from_list_upp)
#
#     v_list_from_list_low_exact = np.array(v_list_from_list_low_exact)
#     v_list_from_list_exact = np.array(v_list_from_list_exact)
#     v_list_from_list_upp_exact = np.array(v_list_from_list_upp_exact)
#
#     v_from_list_low = np.mean(v_list_from_list_low, axis=0)
#     v_from_list = np.mean(v_list_from_list, axis=0)
#     v_from_list_upp = np.mean(v_list_from_list_upp, axis=0)
#
#     v_from_list_low_exact = np.mean(v_list_from_list_low_exact, axis=0)
#     v_from_list_exact = np.mean(v_list_from_list_exact, axis=0)
#     v_from_list_upp_exact = np.mean(v_list_from_list_upp_exact, axis=0)
#
#     v_list_err = np.abs((v_list_from_list - truth) / truth)
#     v_err = np.mean(v_list_err, axis=0)
#     v_err_std = np.std(v_list_err, axis=0)
#     v_err_bound = np.mean(v_list_from_list_upp-v_list_from_list_low, axis=0)
#     v_err_bound_exact = np.mean(v_list_from_list_upp_exact-v_list_from_list_low_exact, axis=0)
#
#     fig = plt.figure("quantile_optim")
#     fig.clf()
#     ax = fig.add_subplot(111)
#
#     lines = []
#     titles = []
#     l, = ax.plot([steps[0], steps[-1]], [truth, truth], 'k')
#     # lines.append(l)
#     # titles.append("Truth")
#
#     l, = ax.plot(steps, v_from_list, 'r')
#     ax.plot(steps, v_from_list_upp, 'r--')
#     ax.plot(steps, v_from_list_low, 'r--')
#     ax.plot(steps, v_from_list_upp_exact, 'r:')
#     ax.plot(steps, v_from_list_low_exact, 'r:')
#     lines.append(l)
#     titles.append("List")
#     ax.legend(lines, titles)
#
#     ax.xaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
#     ax.yaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
#     ax.xaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
#     ax.yaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
#     ax.set_xlabel("# samples")
#     ax.set_xlim(left=steps[0], right=steps[-1])
#     ax.set_ylabel("relative error")
#     ax.semilogx()
#
#     fig = plt.figure("quantile_optim_relative")
#     fig.clf()
#     ax = fig.add_subplot(111)
#     lines = []
#     titles = []
#
#     #
#     l, = ax.plot(steps, v_err, 'g')
#     l, = ax.plot(steps, v_err + 1.96*v_err_std, 'g-.')
#     ax.plot(steps, v_err_bound, 'g--')
#     ax.plot(steps, v_err_bound_exact, 'g:')
#     ax.loglog()
#     # ax.semilogx()
#     ax.grid("on")
#
#     ax.xaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
#     ax.yaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
#     ax.xaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
#     ax.yaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
#     ax.set_xlabel("# samples")
#     ax.set_xlim(left=steps[0], right=steps[-1])
#     ax.set_ylabel("relative error")
#     ax.semilogx()
#
#     plt.show()
#
