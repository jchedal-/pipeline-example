import re


def clean_name(name, replacement_character="_", keep_slash=False, keep_bracket=False):
    """
        erzt;;:,rteA2.6 -> 'erzt_rteA2.6'
    """
    keep_characters = ""
    keep_characters += "a-z"
    keep_characters += "A-Z"
    keep_characters += "0-9"
    keep_characters += "\."
    if keep_bracket:
        keep_characters += "(){}\[\]"
    if keep_slash:
        keep_characters += "/"
    pattern = "[^{}]+".format(keep_characters)

    cleaned = re.sub(pattern, replacement_character, name)
    return cleaned


def longest_common_substring(s1, s2):
    """
    longest_common_substring
     in O(nm) time and O(nm) storage O(n) storage would be possible
    https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python_3
    :param s1:
    :param s2:
    :return: longest common substring
    """
    m = [[0] * (1 + len(s2)) for _ in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]

