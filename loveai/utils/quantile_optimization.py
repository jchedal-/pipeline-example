import numpy as np


class SGDOptimizer(object):

    def __init__(self, gradient, x_0=0, gamma_0=1, alpha=.75, n_min=0):
        self.gradient = gradient
        self.x_0 = x_0
        self.gamma_0 = gamma_0
        self.alpha = alpha

        self.n_min = n_min

        # state
        self.gamma = gamma_0
        self.x = x_0
        self.n = 0
        self.cesaro_x = x_0

    def reset(self, x_0=None):
        self.gamma = self.gamma_0
        self.x = self.x_0 if x_0 is None else x_0
        self.n = 0
        self.cesaro_x = self.x

    def __call__(self, *samples):
        g = self.gradient(self.x, *samples)
        self.n += 1
        self.gamma = self.gamma_0*self.n**(-self.alpha)
        self.x -= self.gamma*g

        n = np.maximum(1, self.n - self.n_min)
        self.cesaro_x = (1-1./n)*self.cesaro_x + self.x/n


class SGDOptimizerSmoothedGradient(object):
    def __init__(self, gradient, x_0=0, gamma_0=1, alpha=.75, rho=.05):
        self.gradient = gradient
        self.x_0 = x_0
        self.gamma_0 = gamma_0
        self.alpha = alpha
        self.rho = rho

        # state
        self.gamma = gamma_0
        self.x = x_0
        self.n = 0
        self.cesaro_x = x_0
        self.gradient_value = 0

    def reset(self, x_0=None):
        self.gamma = self.gamma_0
        self.x = self.x_0 if x_0 is None else x_0
        self.n = 0
        self.cesaro_x = self.x
        self.gradient_value = 0

    def __call__(self, *samples):
        g = self.gradient(self.x, *samples)
        self.gradient_value = g # self.rho*g + (1-self.rho)*self.gradient_value
        self.n += 1

        self.gamma = self.gamma_0*self.n**(-self.alpha)
        self.x -= self.gamma*self.gradient_value
        gamma = 1./float(self.n**.75)
        self.cesaro_x = self.x*gamma + (1-gamma)*self.cesaro_x #(1-1./self.n)*self.cesaro_x + self.x/self.n


class SAGLike(object):
    def __init__(self, gradient, x_0=0., gamma_0=10., alpha=.75, nb=100):
        self.gradient = gradient
        self.x_0 = x_0
        self.gamma_0 = gamma_0
        self.alpha = alpha
        self.nb = nb

        # state
        self.gamma = gamma_0
        self.x = x_0
        self.n = 0
        self.gradient_value = 0
        self.gradients = [0]*nb

    def reset(self, x_0=None):
        self.gamma = self.gamma_0
        self.x = self.x_0 if x_0 is None else x_0
        self.n = 0
        self.gradient_value = 0
        self.gradients = [0] * self.nb

    def __call__(self, *samples):
        g = self.gradient(self.x, *samples)
        self.gradient_value = g  # self.rho*g + (1-self.rho)*self.gradient_value
        self.n += 1
        i = np.random.randint(0, self.nb)
        self.gradient_value -= self.gradients[i]
        self.gradient_value += g
        self.gradients[i] = g

        self.gamma = (self.gamma_0*self.n**(-self.alpha))/float(self.nb)
        self.x -= self.gamma*self.gradient_value


class QuantileGradient(object):
    def __init__(self, q=.5):
        self.q = q

    def __call__(self, x, *samples):
        samples = np.array(samples)
        g = np.sign(x-samples)+1-2*self.q
        g = np.mean(g)
        return float(.5*g)


if __name__ == "__main__":

    import scipy.stats
    import matplotlib.pyplot as plt

    batch_size = 1
    nb_loop = 100
    q = .025
    min_samples = 1
    max_samples = 4.5
    steps = n = np.logspace(min_samples, max_samples, num=100)
    truth = scipy.stats.norm.ppf(q)
    gradient = QuantileGradient(q=q)
    optimizer = SGDOptimizer(gradient, alpha=.6, gamma_0=2, n_min=0)
    # optimizer_smoothed = SAGLike(gradient, alpha=0, gamma_0=5)

    v_list = []
    v_smooth_list = []
    v_list_from_list = []
    v_cesaro_list = []

    for _ in range(nb_loop):
        v = []
        # v_smooth = []
        v_cesaro = []
        v_from_list = []
        x = []
        optimizer.reset()
        # optimizer_smoothed.reset()

        old_step = 0
        for step in steps:
            while old_step < step:
                sample = np.random.randn(batch_size).tolist()
                optimizer(*sample)
                # optimizer_smoothed(*sample)
                # x.append(sample[0])
                x += sample[:]
                old_step += 1

            v.append(optimizer.x)
            # v_smooth.append(optimizer_smoothed.x)
            v_cesaro.append(optimizer.cesaro_x)
            v_from_list.append(np.quantile(x, q))
            # print(optimizer.x, optimizer.cesaro_x)

        v_list.append(v[:])
        # v_smooth_list.append(v_smooth[:])
        v_cesaro_list.append(v_cesaro[:])
        v_list_from_list.append(v_from_list[:])

    v_list = np.array(v_list)
    # v_smooth_list = np.array(v_smooth_list)
    v_cesaro_list = np.array(v_cesaro_list)
    v_list_from_list = np.array(v_list_from_list)

    v_list_err = np.abs((v_list-truth)/truth)
    v_smooth_list_err = np.abs((v_smooth_list-truth)/truth)
    v_cesaro_list_err = np.abs((v_cesaro_list-truth)/truth)
    v_list_from_list_err = np.abs((v_list_from_list-truth)/truth)

    v = np.mean(v_list, axis=0)
    # v_smooth = np.mean(v_smooth_list, axis=0)
    v_cesaro = np.mean(v_cesaro_list, axis=0)
    v_from_list = np.mean(v_list_from_list, axis=0)

    v_std = np.std(v_list, axis=0)
    # v_smooth_std = np.std(v_smooth_list, axis=0)
    v_cesaro_std = np.std(v_cesaro_list, axis=0)
    v_from_list_std = np.std(v_list_from_list, axis=0)

    v_err = np.mean(v_list_err, axis=0)
    # v_smooth_err = np.mean(v_smooth_list_err, axis=0)
    v_cesaro_err = np.mean(v_cesaro_list_err, axis=0)
    v_from_list_err = np.mean(v_list_from_list_err, axis=0)

    v_err_std = np.std(v_list_err, axis=0)
    # v_smooth_err_std = np.std(v_smooth_list_err, axis=0)
    v_cesaro_err_std = np.std(v_cesaro_list_err, axis=0)
    v_from_list_err_std = np.std(v_list_from_list_err, axis=0)

    fig = plt.figure("quantile_optim")
    fig.clf()
    ax = fig.add_subplot(111)

    lines = []
    titles = []
    l, = ax.plot([steps[0], steps[-1]], [truth, truth], 'k')
    # lines.append(l)
    # titles.append("Truth")
    l, = ax.plot(steps, v, 'g')
    ax.plot(steps, v+v_std, 'g--')
    ax.plot(steps, v-v_std, 'g--')
    lines.append(l)
    titles.append("SGD")
    # l, = ax.plot(steps, v_smooth, 'k')
    # ax.plot(steps, v_smooth+v_smooth_std, 'k--')
    # ax.plot(steps, v_smooth-v_smooth_std, 'k--')
    # lines.append(l)
    # titles.append("SAG Like")
    l, = ax.plot(steps, v_cesaro, 'b')
    ax.plot(steps, v_cesaro + v_cesaro_std, 'b--')
    ax.plot(steps, v_cesaro - v_cesaro_std, 'b--')
    lines.append(l)
    titles.append("SGD Polyak-Ruppert")
    l, = ax.plot(steps, v_from_list, 'r')
    ax.plot(steps, v_from_list + v_from_list_std, 'r--')
    ax.plot(steps, v_from_list - v_from_list_std, 'r--')
    lines.append(l)
    titles.append("List")
    ax.legend(lines, titles)



    fig = plt.figure("quantile_optim_relative")
    fig.clf()
    ax = fig.add_subplot(111)
    lines = []
    titles = []

    #
    l, = ax.plot(steps, v_err, 'g')
    ax.plot(steps, v_err+v_err_std, 'g--')
    ax.plot(steps, v_err-v_err_std, 'g--')
    lines.append(l)
    titles.append("SGD")
    # l, = ax.plot(steps, v_smooth_err, 'k')
    # ax.plot(steps, v_smooth_err+v_smooth_err_std, 'k--')
    # ax.plot(steps, v_smooth_err-v_smooth_err_std, 'k--')
    # lines.append(l)
    # titles.append("SAG Like")
    l, = ax.plot(steps, v_cesaro_err, 'b')
    ax.plot(steps, v_cesaro_err+v_cesaro_err_std, 'b--')
    ax.plot(steps, v_cesaro_err-v_cesaro_err_std, 'b--')
    lines.append(l)
    titles.append("SGD Polyak-Ruppert")
    l, = ax.plot(steps, v_from_list_err, 'r')
    ax.plot(steps, v_from_list_err+v_from_list_err_std, 'r--')
    ax.plot(steps, v_from_list_err-v_from_list_err_std, 'r--')
    lines.append(l)
    titles.append("List")
    ax.legend(lines, titles)
    ax.loglog()
    # ax.semilogx()
    ax.grid("on")

    ax.xaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
    ax.yaxis.grid(True, 'minor', lw=.5, color='gray', linestyle='-')
    ax.xaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
    ax.yaxis.grid(True, 'major', lw=1.5, color='gray', linestyle='-')
    ax.set_xlabel("# samples")
    ax.set_xlim(left=steps[0], right=steps[-1])
    ax.set_ylabel("relative error")
    ax.semilogx()

    print(truth, optimizer.x, optimizer.cesaro_x, v_list[-1])
    plt.show()
