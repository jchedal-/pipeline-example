import os.path
import sys
import warnings

from common import veri_global


sys.path.append(veri_global.VERI_TF_RESEARCH)

for sub_folder in [
    "research/object_detection",
    "research/deeplab",
    "research/",
    "research/slim",
]:
    path = os.path.join(veri_global.VERI_TF_RESEARCH, sub_folder).replace("\\", "/")
    if path not in sys.path:
        sys.path.append(path)
        print("add: {}".format(path))
try:
    from object_detection.dataset_tools import tf_record_creation_util
    from object_detection.utils import dataset_util
    from object_detection.utils import label_map_util
    from deeplab.datasets import build_data as deeplab_build_data

    veri_global.RUNNING_TF_RESEARCH_EXISTS = True

except ModuleNotFoundError as e:
    veri_global.RUNNING_TF_RESEARCH_EXISTS = False
    warnings.warn(str(e))
