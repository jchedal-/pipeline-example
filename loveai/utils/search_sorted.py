import numpy as np

class LogSearch(object):
    def __init__(self, test, k=2, step=1, neg=False):
        self._neg = neg
        self._test = test
        self._k = k
        self._step = step

    def __call__(self, n=0):
        if self._neg:
            return self._decreasing(n)
        else:
            return self._increasing(n)

    def _increasing(self, n, prev=None):
        res = prev, n
        if self._test(n):
            n1 = n * self._k + self._step
            res = self._increasing(n1, prev=n)
        return res

    def _decreasing(self, n, prev=None):
        res = n, prev
        if not self._test(n):
            n1 = int(n / self._k) - self._step
            if n1 >= 0:
                res = self._decreasing(n1, prev=n)
        return res


class Dichotomy(object):
    """
       return the maximum value index such that test(n) and not test(n+1)
       assuming monotonicity (if not lower_value: return n+1)
       dichotomy search
    """
    def __init__(self, test, k=2):
        self._test = test
        self._k = k

    def __call__(self, n_low=None, n_upp=None, lower_value=True):
        """
            return the maximum value index such that test(n) and not test(n+1)
            assuming monotonicity (if not lower_value: return n+1)

        :param n_low: starting lower value
        :param n_upp: starting upper value
        :param lower_value: n if lower value n+1 otherwise (default true)
        :return:  n if lower_value n+1 otherwise -  where n is such that test(n) and not test(n+1).
        """
        # <LOWER BOUND>
        n_low = self._get_bound(n_low, n_0=0, upp=False)
        if n_low is None:
            return None
        # </LOWER BOUND>

        # <UPPER_BOUND>
        n_upp = self._get_bound(n_upp, n_0=n_low, upp=True)
        if n_upp is None:
            return None
        # </UPPER_BOUND>

        # <DICHOTOMY>
        n_low = int(n_low)
        n_upp = int(n_upp)
        while n_low < n_upp:
            n_mid = int(n_low + n_upp) >> 1
            test_pass = self._test(n_mid)
            if test_pass:
                n_low = n_mid + 1
            else:
                n_upp = n_mid
        # </DICHOTOMY>
        if lower_value:
            n_low -= 1
        return n_low

    def _get_bound(self, n, n_0, upp):
        res = None
        if n is None:
            res = LogSearch(self._test, self._k)(n_0)
            res = res[int(upp)]
        elif upp ^ self._test(n):
            res = n
        return res


class DichotomyReal(Dichotomy):
    """
       return the maximum value index such that test(x) (and not test(x+e) with e <= epsilon)
       assuming monotonicity (return x)
       dichotomy search
    """
    def __init__(self, test, k=2, epsilon=1e-5):
        super(self.__class__, self).__init__(test, k)
        self._epsilon = epsilon

    def __call__(self, n_low=None, n_upp=None, lower_value=True):
        """
            return the maximum value index such that test(n) and not test(n+1)
            assuming monotonicity (if not lower_value: return n+1)

        :param n_low: starting lower value
        :param n_upp: starting upper value
        :return:  n
        """
        # <LOWER BOUND>
        n_low = self._get_bound(n_low, n_0=0, upp=False)
        if n_low is None:
            return None
        # </LOWER BOUND>

        # <UPPER_BOUND>
        n_upp = self._get_bound(n_upp, n_0=n_low, upp=True)
        if n_upp is None:
            return None
        # </UPPER_BOUND>

        # <DICHOTOMY>
        n_low = int(n_low)
        n_upp = int(n_upp)
        while (n_upp-n_low) > self._epsilon:
            n_mid = (n_low + n_upp)/2.
            test_pass = self._test(n_mid)
            if test_pass:
                n_low = n_mid
            else:
                n_upp = n_mid
        # </DICHOTOMY>

        return n_low if lower_value else n_upp


def brute_force(test, n0=0, max_n=1e6, lower_value=True):
    """
        return the maximum value index such that test(n) and not test(n+1)
        assuming monotonicity (if not lower_value: return n+1)

        return the smallest n such that test(n)
        using a brute force approach
    :param test: function, lambda, functor
    :param n0: starting value for n (default 0)
    :param max_n: maximum value for n (return np.inf if not test(n) for all n < max_n
    :param lower_value: n if lower value n+1 otherwise (default true)
    :return:  n if lower_value n+1 otherwise -  where n is such that test(n) and not test(n+1).
    """
    max_n = int(max_n)
    n = int(n0)
    while n < max_n and test(n0):
        n += 1
    if n == max_n:
        n = np.inf
    elif lower_value:
        n -= 1
    return n
