[Setuptools](https://setuptools.readthedocs.io/en/latest/setuptools.html) is the standard build tool for python, it can be used for distributing packages, running tests, handling dependencies, run arbitrary build commands, &#x2026;
(The code in this repository does not actually work, this is a proof of concept for a potential project layout)


<a id="org2079d0e"></a>

# Benefits

-   Being able to install via pip with all the dependencies
    -   <https://packaging.python.org/discussions/install-requires-vs-requirements/>
    -   Having to clone the project then follow the installation instructions then build the actual user project can be a pain, all this can be done in the regular way with pip:
        `python -m pip install git+https://gitlab.com/jchedal-/pipeline-example`
    -   Support for optional dependencies out of the box, useful to avoid the user to download GBs of data and libraries for an algorithm they will not use
    -   Conditional dependencies, not useful for now, but maybe in the future eg install a package that was added to the standard library for versions prior to its addition
-   Proper versioning with setuptools, such as enforcing python version
-   Being able to use testing and diagnostic tools in an easy way
    -   Run tests (eg with [unittest](https://docs.python.org/3/library/unittest.html), [pytest](https://docs.pytest.org/en/latest/) or [hypothesis](https://hypothesis.readthedocs.io/en/latest/)) with the builtin command `python -m setup test`, as well as linters and other code diagnostic tools as part of the process
-   Ease code sharing, usable like any other library, no clutter
-   Proper doc generation tools with [Sphinx](http://www.sphinx-doc.org/en/master/index.html) or [Doxygen](http://www.doxygen.nl/), to use with a static page or in the gitlab wiki etc
    -   Documentations are pulled from docstrings in the code, we can also write our own directly in [reST](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) (.rst)
    -   File format converter are very easy to find ([pandoc](https://pandoc.org/index.html) is very good), we can write the documentation in markdown or some other preferred format before converting to ReST when we push the documentation
-   Tokens, global variables, paths can be setup with setuptools, the content of `configuration_setup.py` will be moved to `setup.py`
-   Relatively easy to implement, doesn't need any change to the library code, simply pull out the `common` folder and write the `setup.py` file
-   Project will still be usable as a stand-alone if we want to, `__main__.py` can serve as the entry point to run as `python -m loveai` and have specific behaviour when run as a stand-alone vs imported in another program

# Issues

-   There may be better ways to initialize the configuration specific global variables
-   We will probably need to fork or duplicate the tensorflow/models repository, as cloning it is inherently incompatible with a library format
    -   Alternative: Make a python module that will setup the repo, which will be used with a command like `python -m loveai.build_models`
    -   Duplicating the repository gives us fine-grain control over what we use and what we depend on
    - Licensing issues ?
        - Update: Seems fine for tensorflow/models, allows commercial uses, but not trademark uses, not sure what the difference is

# Design changes to consider

-   Folder where files are outputed should be set up programmatically, for example with `pipeline = Pipeline(out_dir="./out_dir")`, over the global variable set up in `configuration_setup.py`
-   Same with token, written in the user config: `pipeline = Pipeline(out_dir="./out_dir", token="foo")`
    -   However, this could cause issues with interoperability if we want to support various cloud providers, maybe give a path to a config file (json, yaml, ...)
